<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Activitylog\Models\Activity;

class Cidade extends Model implements HasMedia
{
    use LogsActivity;
    use HasMediaTrait;
    use HasSlug;

    protected $guarded = [];
    protected static $logUnguarded = true;
    protected static $logOnlyDirty = true;    

    public function entidade()
    {
        return $this->belongsTo('App\Entidade');
    }    
    public function localEventos()
    {
        return $this->belongsToMany('App\LocalEvento');
    }    
    public function alojamentoTipos()
    {
        return $this->belongsToMany('App\AlojamentoTipo', 'cidade_alojamento_tipo');
    }
    public function alojamentoCategorias()
    {
        return $this->belongsToMany('App\AlojamentoCategoria', 'cidade_alojamento_categoria');
    }
    public function modalTipos()
    {
        return $this->belongsToMany('App\ModalTipo', 'cidade_modal_tipo');
    }
    public function localEventoTipos()
    {
        return $this->belongsToMany('App\LocalEventoTipo', 'cidade_local_evento_tipo');
    }

    public function getApprovalStatusAttribute($value)
    {
        return $value == 0 ? "Pendente" : ($value == 1 ? 'Aprovado' : ($value == 2 ? "Rejeitado.\n  ".$this->motivo_rejeicao : ''));
    }

    public static function findCidadeByLogId($id)
    {
        $cidadeID = Activity::where('id',$id)->where('subject_type','App\Cidade')->select('subject_id')->get()->first()->subject_id;
        return Cidade::find($cidadeID);
    }

    public static function pendingAndRejected($cidadeID = 0)
    {        
        $pendingAndRejected = Activity::join('cidades', 'activity_log.subject_id','cidades.id')
                        ->join('users','activity_log.causer_id','users.id')
                        ->whereRaw("(description = 'edited' OR description like 'rejected%')")
                        ->select('cidades.*', 'activity_log.id as alid', 'activity_log.created_at as log_created_at', 'activity_log.properties','activity_log.description','users.name','users.id as uid');

        if (!auth()->user()->hasRole('admin')) {        
            $pendingAndRejected->where("activity_log.causer_id",auth()->user()->id);
        }                        
        if ($cidadeID > 0) {
            $pendingAndRejected->where("cidades.id",$cidadeID);
        }
        return $pendingAndRejected->get();
    }
    public static function pending($cidadeID = 0)
    {        
        $pending = Activity::join('cidades', 'activity_log.subject_id','cidades.id')
                        ->join('users','activity_log.causer_id','users.id')
                        ->where("description","edited")
                        ->select('cidades.*', 'activity_log.id as alid', 'activity_log.created_at as log_created_at', 'activity_log.properties','users.name','users.id as uid');
                        
        if (!auth()->user()->hasRole('admin')) {        
            $pending->where("activity_log.causer_id",auth()->user()->id);
        } 
        if ($cidadeID > 0) {
            $pending->where("cidades.id",$cidadeID);
        }
        return $pending->get();
    }
    public static function rejected($cidadeID = 0)
    {        
        $rejected = Activity::join('cidades', 'activity_log.subject_id','cidades.id')
                        ->join('users','activity_log.causer_id','users.id')
                        ->where("description", "like", "rejected%")
                        ->select('cidades.*', 'activity_log.id as alid', 'activity_log.created_at as log_created_at', 'activity_log.properties','users.name','users.id as uid');
                        
        if (!auth()->user()->hasRole('admin')) {        
            $rejected->where("activity_log.causer_id",auth()->user()->id);
        } 
        if ($cidadeID > 0) {
            $rejected->where("cidades.id",$cidadeID);
        }
        return $rejected->get();
    }
    public function approve($logID)
    {
        $log = Activity::find($logID);
        $log->description = 'approved';
        $log->save();
    }
    public function reject($logID, $motivo)
    {
        $log = Activity::find($logID);
        $log->description = "rejected|$motivo";
        $log->save();
    }

    public static function search(Request $filters)
    {
        $query = Cidade::query();
        $query->select('cidades.*');
        
        if ($filters->has('distancia_capital')) {
            if ($filters->distancia_capital == 241) {
                $query->where('distancia_capital', '>', 240);
            } elseif ($filters->distancia_capital > 0) {
                $query->where('distancia_capital', '<=', $filters->distancia_capital);                
            }
        }
        if ($filters->has('acesso')) {
            $query->whereHas('modalTipos', function($q) use ($filters) {
                $q->whereIn('modal_tipo_id', $filters->acesso);
            });
        }
        if ($filters->has('tipo_local')) {
            $query->whereHas('localEventoTipos', function($q) use ($filters) {
                $q->whereIn('local_evento_tipo_id', $filters->tipo_local);
            });
        }        
        if ($filters->has('capacidade_auditorio') && $filters->has('capacidade_auditorio') > 0) {
            $query->where('capacidade_maior_auditorio', '>=',  $filters->capacidade_auditorio);
        }
        if ($filters->has('tamanho_espaco') && $filters->has('tamanho_espaco') > 0) {
            $query->where('capacidade_maior_espaco', '>=',  $filters->tamanho_espaco);
        }
        if ($filters->has('capacidade_banquete') && $filters->has('capacidade_banquete') > 0) {
            $query->where('capacidade_maior_banquete', '>=',  $filters->capacidade_banquete);
        }      
        return $query->where('is_active', 1)->orderBy('nome', 'asc')->distinct()->get();
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('nome')
            ->saveSlugsTo('slug');
    }
}
