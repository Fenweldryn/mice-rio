<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alojamento extends Model
{    
    public function cidade()
    {
        return $this->belongsTo('App\Cidade');
    }
    public function alojamentoTipo()
    {
        return $this->belongsTo('App\AlojamentoTipo');
    }
    public function alojamentoCategoria()
    {
        return $this->belongsTo("App\AlojamentoCategoria");
    }
}
