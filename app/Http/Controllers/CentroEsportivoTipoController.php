<?php

namespace App\Http\Controllers;

use App\CentroEsportivoTipo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CentroEsportivoTipoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:ver centros esportivos']);
        $this->middleware(['role:admin'],['except' => ['index','datatablesAjax']]);
    }

    public function list()
    {
        return CentroEsportivoTipo::select('id','nome')->get();
    }

    public function index()
    {
        return view("admin.centroesportivotipos.index");
    }
    
    public function create()
    {
        return view("admin.centroesportivotipos.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);        
        $centroesportivotipo = new CentroEsportivoTipo;
        $centroesportivotipo->nome = $request['nome'];
        $centroesportivotipo->save();
        return redirect('admin/centroesportivotipos')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(CentroEsportivoTipo $centroesportivotipo)
    {
        dd($centroesportivotipo);
    }

    public function edit(CentroEsportivoTipo $centroesportivotipo)
    {
        $cidades = \App\Cidade::all();
        return view('admin.centroesportivotipos.edit', compact('centroesportivotipo','cidades'));
    }

    public function update(Request $request, CentroEsportivoTipo $centroesportivotipo)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);        
        $centroesportivotipo->nome = $request['nome'];
        $centroesportivotipo->update();
        return redirect('admin/centroesportivotipos')->with('success',"Editado com sucesso.");   
    }

    public function destroy(CentroEsportivoTipo $centroesportivotipo)
    {
        $centroesportivotipo->delete();
        return redirect('admin/centroesportivotipos')->with('success',"Excluído com sucesso.");   
    }

    public function caracteristicas(CentroEsportivoTipo $centroesportivotipo)
    {
        return $centroesportivotipo->caracteristicas()->get();
    }

    public function datatablesAjax()
    {
        $centroesportivotipos = CentroEsportivoTipo::query();
        return DataTables::of($centroesportivotipos)         
            ->addColumn('action', function($row){
                $btn = "";
                if (auth()->user()->hasRole('admin')) {
                    $btn = '<a href=' . route("centroesportivotipos.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                        <form action=' . route("centroesportivotipos.destroy", $row->id) . ' method="POST"
                                style="display: inline"
                                onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                            <input type="hidden" name="_method" value="DELETE">
                            ' . csrf_field() . '
                            <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                        </form>';
                }
                return $btn;
            })
            ->rawColumns(['action'])
        ->make(true);
    }
}
