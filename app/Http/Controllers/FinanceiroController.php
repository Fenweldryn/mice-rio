<?php

namespace App\Http\Controllers;

use \App\Associado;
use \App\Pagamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FinanceiroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        // dd(auth()->user()->entidade->first()->exists());
        $emDia = Associado::emDia()->count();
        $totalAssociados = Associado::where('ativo', 1)->count();
        $totalAssociadosInativos = Associado::where('ativo', 0)->count();
        $vencidas = Pagamento::vencidos()->count();
        $dadosGraficoTodosOsPagamentos = $this->graficoPagamentosRecebidos();
        $dadosGraficoTodosOsPagamentosPendentes = $this->graficoPagamentosPendentes();
        $cvbs = \App\Entidade::all();
        
        return view("admin.financeiro.index", 
            compact(
                'emDia',
                'totalAssociados',
                'totalAssociadosInativos',
                'vencidas',
                'dadosGraficoTodosOsPagamentos',
                'dadosGraficoTodosOsPagamentosPendentes',
                'cvbs'
            )
        );
    }

    public function graficoPagamentosRecebidos($entidadeID = 0)
    {
        if (request()->has('entidade_id')) {
            $entidadeID = request()->post('entidade_id');
        }
        $pagamentos = Pagamento::recebidos($entidadeID);
        return [
            'grafico' => $this->estruturarProGrafico($pagamentos->toArray()), 
            'total' => number_format($pagamentos->sum('valor'), 2, ',', ' ')
        ];

    }

    public function graficoPagamentosPendentes($entidadeID = 0)
    {
        if (request()->has('entidade_id')) {
            $entidadeID = request()->post('entidade_id');
        }
        
        $pagamentos = Pagamento::pendentes($entidadeID);
        return [
            'grafico' => $this->estruturarProGrafico($pagamentos->toArray()), 
            'total' => number_format($pagamentos->sum('valor'), 2, ',', ' ')
        ];

    }

    public function estruturarProGrafico($dados)
    {
        $valores = [];
        $dadosEstruturados = [];

        foreach ($dados as $pagamento) {   
            if (!isset($valores[$pagamento['ano']])) {
                $valores[$pagamento['ano']] = [];
            }
            
            $valores[$pagamento['ano']][$pagamento['mes']] = (float)$pagamento['valor'];
        }

        foreach ($valores as $ano => $val) {
            for ($i=1; $i <= 12; $i++) { 
                if (!isset($valores[$ano][$i])) {
                    $valores[$ano][$i] = "null";
                }
            }            
        }

        foreach ($valores as $ano => $val) {
            \ksort($val);
            $dadosEstruturados[] = array(
                "name" => "$ano",
                "data" => array_values($val)
            );
        }

        return $dadosEstruturados;
    }
}
