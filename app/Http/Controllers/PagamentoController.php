<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cidade;
use App\Pagamento;
use App\Associado;
use Yajra\DataTables\DataTables;

class PagamentoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $pagamentos = Pagamento::all();
        return view("admin.pagamentos.index", compact('pagamentos'));
    }

    public function vencidos(Request $request)
    {
        $this->validate($request, [
            'data' => "required|date_format:m/Y",
        ]);  
        $data = \Carbon\Carbon::createFromFormat('d/m/Y', "01/" . $request['data'])->format('Y-m-d');        
        return Associado::whereHas('pagamentos', function ($q) use ($data){
            $q->whereNull('data_pagamento')->where('data_referencia', $data);
        })->get();
    }
    
    public function create()
    {
        return view("admin.pagamentos.create");
    }

    public function store(Request $request)
    {   
        $this->validate($request, [
            'pagamento_id' => "required|numeric"
        ]);  
        $pagamento = Pagamento::find($request->pagamento_id);

        if ($pagamento->data_pagamento == null) 
        {
            $pagamento->data_pagamento = date('Y-m-d');
            $pagamento->update();
            return back()->with('success',"Pagamento cadastrado com sucesso");                           
        }
        else 
        {
            return back()->with('info',"Já existe pagamento para o associado e mês informados.")->withInput();          
        }
    }

    public function storeBatch(Request $request)
    {   
        $this->validate($request, [
            'associados' => 'required',
            'associados.*' => "numeric",
            'data' => "required|date_format:m/Y",
        ]);  
        
        $data = \Carbon\Carbon::createFromFormat('d/m/Y', "01/" . $request['data'])->format('Y-m-d');        

        $pagamentos = Pagamento::whereDate('data_referencia', $data)
            ->whereIn('associado_id', $request['associados'])
        ->get();
        
        foreach ($pagamentos as $key => $pagamento) {
            if ($pagamento->data_pagamento == null) {
                $pagamento->data_pagamento = date('Y-m-d');
                $pagamento->update();                
            }
        }
                
        return back()->with('success',"Pagamentos cadastrados com sucesso.");                           
    }

    public function show(Pagamento $pagamento)
    {
        dd($pagamento);
    }

    public function edit(Pagamento $pagamento)
    {
        return view('admin.pagamentos.edit', compact('pagamento'));
    }

    public function update(Request $request, Pagamento $pagamento)
    {
        $this->validate($request, [
            'associado_id' => "required|numeric",
        ]);   
        $pagamento->associado_id = $request['associado_id'];         
        $pagamento->update();            
        
        return redirect('admin/pagamentos')->with('success',"Editado com sucesso."); 
    }

    public function destroy(Pagamento $pagamento)
    {
        $pagamento->delete();
        return redirect('admin/pagamentos')->with('success',"Excluído com sucesso.");   
    }

    public function datatablesAjax()
    {
        $pagamentos = Pagamento::query();

        return DataTables::of($pagamentos)                    
        ->addColumn('action', function($row){
            $informarPagamento = '
                <form action=' . route("pagamentos.store") . ' method="POST"                        
                        onsubmit=\'return confirm("Tem certeza que deseja informar pagamento?");\'>
                    <input type="hidden" name="pagamento_id" value="'.$row->id.'">
                    ' . csrf_field() . '
                    <button type="submit" class="btn btn-primary" >
                        <i class="fas fa-hand-holding-usd"></i> 
                        Informar Pagamento
                    </button>
                </form>
            ';

            $deletar = ' 
                <form action=' . route("pagamentos.destroy", $row->id) . ' method="POST"                        
                        onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                    <input type="hidden" name="_method" value="DELETE">
                    ' . csrf_field() . '
                    <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                </form>
            ';

            $botoes = $row->data_pagamento ? $deletar : $informarPagamento.$deletar;

            return '
                <div class="d-flex" style="justify-content: space-evenly">
                    '.$botoes.'
                </div>
            ';
        })                
        ->addColumn('entidade', function($row){
            return $row->associado->entidade->nome_fantasia;
        })                
        ->addColumn('associado', function($row){
            return $row->associado->nome;
        })                
        ->addColumn('status', function($row){   
            return $row->status();
        })          
        ->editColumn('valor', function($row){   
            return 'R$ '.number_format($row->valor, 2, ',', '');
        })          
        ->rawColumns(['status', 'action'])   
        ->make(true);
    }
}
