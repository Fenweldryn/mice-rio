<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cidade;
use App\Associado;
use App\AssociadoTipoModel;
use App\Http\Requests\AssociadoCreateRequest;
use App\Http\Requests\AssociadoUpdateRequest;
use App\Telefone;
use phpDocumentor\Reflection\Types\Integer;
use Yajra\DataTables\DataTables;

class AssociadoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $totalAssociados = Associado::where('ativo',1)->count();
        $totalAssociadosInativos = Associado::where('ativo',0)->count();

        return view("admin.associados.index",  
            compact(
                'totalAssociados',
                'totalAssociadosInativos'
            )
        );
    }

    public function getSubtipo(Request $request, $tipo)
    {
        $model = AssociadoTipoModel::find($tipo)->model;
        return $model::all()->pluck('nome','id');
    }
    
    public function create()
    {
        $entidades = \App\Entidade::all();
        $tipos = AssociadoTipoModel::all();
        return view("admin.associados.create", compact('entidades','tipos'));
    }

    public function store(AssociadoCreateRequest $request)
    {        
        $associado = new Associado;
        $associado->nome = $request['nome'];
        $associado->mensalidade = number_format((float)str_replace(',', '.', str_replace('.', '', $request['mensalidade'])), 2, '.', '');      
        $associado->entidade_id = $request['entidade_id'];          
        $associado->dia_vencimento = $request['dia_vencimento'];
        $associado->cnpj = $request['cnpj'];
        $associado->tipo_type = AssociadoTipoModel::find($request->tipo)->model;
        $associado->tipo_id = $request->subtipo;
        $associado->email = $request->email;
        $associado->site = $request->site;
        $associado->endereco = $request->endereco;        
        $associado->facebook = $request->facebook;
        $associado->instagram = $request->instagram;
        $associado->linkedin = $request->linkedin;
        $associado->pinterest = $request->pinterest;
        $associado->distancia_centro_km = number_format((float)str_replace(',', '.', str_replace('.', '', $request['distancia_centro_km'])), 2, '.', '');
        $associado->area_construida_total_m2 = $request->area_construida_total_m2;
        $associado->area_evento_total_m2 = $request->area_evento_total_m2;
        $associado->capacidade_lugares_auditorio = $request->capacidade_lugares_auditorio;
        $associado->capacidade_lugares_coquetel = $request->capacidade_lugares_coquetel;
        $associado->data_associacao = \Carbon\Carbon::createFromFormat('d/m/Y', $request->data_associacao)->format('Y-m-d');
        $associado->telefones = array_filter($request->telefones);
        $associado->save(); 
        return redirect('admin/associados')->with('success',"Cadastrado com sucesso.");               
    }

    public function show(Associado $associado)
    {
        return view('admin.associados.show', compact('associado'));
    }

    public function edit(Associado $associado)
    {
        $entidades = \App\Entidade::all();
        $tipos = AssociadoTipoModel::all();
        $model = AssociadoTipoModel::where('model', $associado->tipo_type)->get()->first()->model;
        $subtipos = $model::all();
        return view('admin.associados.edit', compact('associado','entidades','tipos','subtipos'));
    }

    public function update(AssociadoUpdateRequest $request, Associado $associado)
    {
        $associado->nome = $request['nome'];      
        $associado->mensalidade = number_format((float)str_replace(',', '.', str_replace('.', '',$request['mensalidade'])),2,'.','');      
        $associado->entidade_id = $request['entidade_id'];      
        $associado->dia_vencimento = $request['dia_vencimento'];
        $associado->cnpj = $request['cnpj'];
        $associado->tipo_type = AssociadoTipoModel::find($request->tipo)->model;
        $associado->tipo_id = $request->subtipo;
        $associado->email = $request->email;
        $associado->site = $request->site;
        $associado->endereco = $request->endereco;
        $associado->facebook = $request->facebook;
        $associado->instagram = $request->instagram;
        $associado->linkedin = $request->linkedin;
        $associado->pinterest = $request->pinterest;
        $associado->distancia_centro_km = number_format((float)str_replace(',', '.', str_replace('.', '', $request['distancia_centro_km'])), 2, '.', '');        
        $associado->area_construida_total_m2 = $request->area_construida_total_m2;
        $associado->area_evento_total_m2 = $request->area_evento_total_m2;
        $associado->capacidade_lugares_auditorio = $request->capacidade_lugares_auditorio;
        $associado->capacidade_lugares_coquetel = $request->capacidade_lugares_coquetel;   
        $associado->data_associacao = \Carbon\Carbon::createFromFormat('d/m/Y', $request->data_associacao)->format('Y-m-d');
        $associado->telefones = array_filter($request->telefones);       
        $associado->update();

        return redirect('admin/associados')->with('success',"Editado com sucesso."); 
    }

    public function destroy(Associado $associado)
    {
        $associado->delete();
        return redirect('admin/associados')->with('success',"Excluído com sucesso.");   
    }

    public function datatablesAjax()
    {
        $associados = Associado::query();

        return DataTables::of($associados)                    
        ->addColumn('action', function($row){
            return '
                <a href=' . route("associados.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                <form action=' . route("associados.destroy", $row->id) . ' method="POST"                        
                        onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                    <input type="hidden" name="_method" value="DELETE">
                    ' . csrf_field() . '
                    <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                </form>';
        })                
        ->addColumn('entidade', function($row){
            return $row->entidade->nome_fantasia;
        })                
        ->editColumn('created_at', function($row){
            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('d/m/Y H:i:s');
        })                
        ->addColumn('pagamento', function($row){   
            $pendentes = '<span class="badge badge-light">'.$row->pagamentosPendentes()->count().'</span>';
            $color = starts_with($row->status(),"Pendente") ? 'danger' : (starts_with($row->status(),'Vence') ? "warning" : "success");
            if ($row->status() == "Pendente") 
            {
                return "<a class='btn btn-$color' href=" . route('associados.show', $row->id) . ">" . $row->status() . " $pendentes</a>";    
            }
            else 
            {
                return "<a class='btn btn-$color' href=" . route('associados.show', $row->id) . ">" . $row->status() . "</a>";                
            }
        })     
        ->editColumn('mensalidade', function($row){   
            return number_format($row->mensalidade, 2, ',', '');
        })             
        ->rawColumns(['pagamento', 'action'])   
        ->make(true);
    }
}
