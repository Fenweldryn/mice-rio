<?php

namespace App\Http\Controllers;

use App\AssociadoTipoModel;
use Illuminate\Http\Request;

class AssociadoTipoModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssociadoTipoModel  $associadoTipoModel
     * @return \Illuminate\Http\Response
     */
    public function show(AssociadoTipoModel $associadoTipoModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssociadoTipoModel  $associadoTipoModel
     * @return \Illuminate\Http\Response
     */
    public function edit(AssociadoTipoModel $associadoTipoModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssociadoTipoModel  $associadoTipoModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssociadoTipoModel $associadoTipoModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssociadoTipoModel  $associadoTipoModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssociadoTipoModel $associadoTipoModel)
    {
        //
    }
}
