<?php

namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    public function index()
    {
        return view("admin.roles.index");
    }

    public function list()
    {
        return Role::all();
    }
    
    public function create()
    {
        return view("admin.roles.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|min:3|max:191",
        ]);                
        $role = new Role;
        $role->name = $request['name'];        
        $role->save();
        return redirect('admin/roles')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(Role $role)
    {
        dd($role);
    }

    public function destroy(Role $role)
    {
        if ($role->id > 2) {
            $role->delete();            
        }
        return redirect('admin/roles')->with('success',"Excluído com sucesso.");   
    }

    public function permissions(Role $role)
    {
        return $role->permissions;
    }

    public function addPermissionPage(Role $role)
    {
        return view('admin/roles/addPermission', compact('role'));
    }

    public function addPermissions(Role $role, Request $request)
    {           
        $role->givePermissionTo(Permission::find($request->id)->name);
    }

    public function destroyPermission(Role $role, Request $request)
    {
        $role->revokePermissionTo(Permission::find($request->id)->name);
    }
    
    public function datatablesAjax()
    {
        $role = Role::query();
        return DataTables::of($role)      
            ->addColumn('permissions', function($row){
                $btn = "";
                if($row->id > 1) 
                {
                    $btn = '<button onclick="ajaxThenCallModal('.$row->id.',\''.$row->name.'\')" class="btn btn-default"><i class="fas fa-eye"></i> Ver</button>';
                }
                return $btn;
            })      
            ->addColumn('action', function($row){
                $btn = "";
                if($row->id > 2) 
                {

                    $btn = '<form action=' . route("roles.destroy", $row->id) . ' method="POST"
                                    style="display: inline"
                                    onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                                <input type="hidden" name="_method" value="DELETE">
                                ' . csrf_field() . '
                                <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                            </form>';
                }
                return $btn;
            })
            ->rawColumns(['action','permissions'])
        ->make(true);
    }
}
