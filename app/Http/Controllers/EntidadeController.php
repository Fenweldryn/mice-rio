<?php

namespace App\Http\Controllers;

use App\Entidade;
use App\User;
use App\Cidade;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class EntidadeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:gerir entidades']);
    }

    public function index()
    {
        return view("admin.entidades.index");
    }
    
    public function create()
    {
        return view("admin.entidades.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "nome_fantasia" => "required|min:3|max:191",            
            "endereco" => "required|min:3|max:191",
            "cnpj" => "required|cnpj",
            "email" => "required|email",
            "telefone" => "required|numeric",
            "whatsapp" => "required|numeric",            
            "ddd_telefone" => "required|numeric|max:999",
            "ddd_whatsapp" => "required|numeric|max:999",            
            "site" => "url",            
            "facebook" => "url",            
            "instagram" => "url",            
            "twitter" => "url",            
            "linkedin" => "url",  
            "nome_presidente" => "required|min:3|max:191",
            "email_presidente" => "required|email|min:3|max:191",
            "telefone_fixo_presidente" => "required|numeric",
            "celular_presidente" => "required|numeric",
            "ddd_telefone_fixo_presidente" => "required|numeric|max:999",
            "ddd_celular_presidente" => "required|numeric|max:999",
            "nome_financeiro" => "required|min:3|max:191",
            "email_financeiro" => "required|email|min:3|max:191",
            "telefone_fixo_financeiro" => "required|numeric",
            "celular_financeiro" => "required|numeric",
            "ddd_telefone_fixo_financeiro" => "required|numeric|max:999",
            "ddd_celular_financeiro" => "required|numeric|max:999",
            "nome_gerente" => "required|min:3|max:191",
            "email_gerente" => "required|email|min:3|max:191",
            "telefone_fixo_gerente" => "required|numeric",
            "celular_gerente" => "required|numeric",                        
            "ddd_telefone_fixo_gerente" => "required|numeric|max:999",
            "ddd_celular_gerente" => "required|numeric|max:999",   
        ]);        
        $entidade = new Entidade;
        $entidade->nome = $request['nome'];
        $entidade->nome_fantasia = $request['nome_fantasia'];
        $entidade->endereco = $request['endereco'];
        $entidade->cnpj = $request['cnpj'];
        $entidade->email = $request['email'];
        $entidade->telefone = $request['telefone'];
        $entidade->whatsapp = $request['whatsapp'];
        $entidade->ddd_telefone = $request['ddd_telefone'];
        $entidade->ddd_whatsapp = $request['ddd_whatsapp'];
        $entidade->site = $request['site'];
        $entidade->facebook = $request['facebook'];
        $entidade->instagram = $request['instagram'];
        $entidade->twitter = $request['twitter'];
        $entidade->linkedin = $request['linkedin'];
        $entidade->nome_presidente = $request['nome_presidente'];
        $entidade->email_presidente = $request['email_presidente'];
        $entidade->telefone_fixo_presidente = $request['telefone_fixo_presidente'];
        $entidade->celular_presidente = $request['celular_presidente'];
        $entidade->ddd_telefone_fixo_presidente = $request['ddd_telefone_fixo_presidente'];
        $entidade->ddd_celular_presidente = $request['ddd_celular_presidente'];
        $entidade->nome_financeiro = $request['nome_financeiro'];
        $entidade->email_financeiro = $request['email_financeiro'];
        $entidade->telefone_fixo_financeiro = $request['telefone_fixo_financeiro'];
        $entidade->celular_financeiro = $request['celular_financeiro'];
        $entidade->ddd_telefone_fixo_financeiro = $request['ddd_telefone_fixo_financeiro'];
        $entidade->ddd_celular_financeiro = $request['ddd_celular_financeiro'];
        $entidade->nome_gerente = $request['nome_gerente'];
        $entidade->email_gerente = $request['email_gerente'];
        $entidade->telefone_fixo_gerente = $request['telefone_fixo_gerente'];
        $entidade->celular_gerente = $request['celular_gerente'];
        $entidade->ddd_telefone_fixo_gerente = $request['ddd_telefone_fixo_gerente'];
        $entidade->ddd_celular_gerente = $request['ddd_celular_gerente'];
        $entidade->save();
        if($request->hasfile('foto'))
        {
            $entidade->addMedia($request->file('foto'))->toMediaCollection(); 
        }
        return redirect('admin/entidades')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(Entidade $entidade)
    {
        dd($entidade);
    }

    public function edit(Entidade $entidade)
    {
        return view('admin.entidades.edit', compact('entidade'));
    }

    public function update(Request $request, Entidade $entidade)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "nome_fantasia" => "required|min:3|max:191",            
            "endereco" => "required|min:3|max:191",
            "cnpj" => "required|cnpj",
            "email" => "required|email",
            "telefone" => "required|numeric",
            "whatsapp" => "required|numeric",            
            "ddd_telefone" => "required|numeric|max:999",
            "ddd_whatsapp" => "required|numeric|max:999",            
            "site" => "url",            
            "facebook" => "url",            
            "instagram" => "url",            
            "twitter" => "url",            
            "linkedin" => "url",  
            "nome_presidente" => "required|min:3|max:191",
            "email_presidente" => "required|email|min:3|max:191",
            "telefone_fixo_presidente" => "required|numeric",
            "celular_presidente" => "required|numeric",
            "ddd_telefone_fixo_presidente" => "required|numeric|max:999",
            "ddd_celular_presidente" => "required|numeric|max:999",
            "nome_financeiro" => "required|min:3|max:191",
            "email_financeiro" => "required|email|min:3|max:191",
            "telefone_fixo_financeiro" => "required|numeric",
            "celular_financeiro" => "required|numeric",
            "ddd_telefone_fixo_financeiro" => "required|numeric|max:999",
            "ddd_celular_financeiro" => "required|numeric|max:999",
            "nome_gerente" => "required|min:3|max:191",
            "email_gerente" => "required|email|min:3|max:191",
            "telefone_fixo_gerente" => "required|numeric",
            "celular_gerente" => "required|numeric",                        
            "ddd_telefone_fixo_gerente" => "required|numeric|max:999",
            "ddd_celular_gerente" => "required|numeric|max:999",                           
        ]);        
        $entidade->nome = $request['nome'];
        $entidade->nome_fantasia = $request['nome_fantasia'];
        $entidade->endereco = $request['endereco'];
        $entidade->cnpj = $request['cnpj'];
        $entidade->email = $request['email'];
        $entidade->telefone = $request['telefone'];
        $entidade->whatsapp = $request['whatsapp'];
        $entidade->ddd_telefone = $request['ddd_telefone'];
        $entidade->ddd_whatsapp = $request['ddd_whatsapp'];
        $entidade->site = $request['site'];
        $entidade->facebook = $request['facebook'];
        $entidade->instagram = $request['instagram'];
        $entidade->twitter = $request['twitter'];
        $entidade->linkedin = $request['linkedin'];
        $entidade->nome_presidente = $request['nome_presidente'];
        $entidade->email_presidente = $request['email_presidente'];
        $entidade->telefone_fixo_presidente = $request['telefone_fixo_presidente'];
        $entidade->celular_presidente = $request['celular_presidente'];
        $entidade->ddd_telefone_fixo_presidente = $request['ddd_telefone_fixo_presidente'];
        $entidade->ddd_celular_presidente = $request['ddd_celular_presidente'];
        $entidade->nome_financeiro = $request['nome_financeiro'];
        $entidade->email_financeiro = $request['email_financeiro'];
        $entidade->telefone_fixo_financeiro = $request['telefone_fixo_financeiro'];
        $entidade->celular_financeiro = $request['celular_financeiro'];
        $entidade->ddd_telefone_fixo_financeiro = $request['ddd_telefone_fixo_financeiro'];
        $entidade->ddd_celular_financeiro = $request['ddd_celular_financeiro'];
        $entidade->nome_gerente = $request['nome_gerente'];
        $entidade->email_gerente = $request['email_gerente'];
        $entidade->telefone_fixo_gerente = $request['telefone_fixo_gerente'];
        $entidade->celular_gerente = $request['celular_gerente'];
        $entidade->ddd_telefone_fixo_gerente = $request['ddd_telefone_fixo_gerente'];
        $entidade->ddd_celular_gerente = $request['ddd_celular_gerente'];
        $entidade->regiao_cidade_principal_id = $request['regiao_cidade_principal_id'];
        $entidade->update();
        
        if($request->hasFile('foto'))
        {
            if($entidade->getFirstMedia()) 
            {
                $entidade->getFirstMedia()->delete();
            }            
            $entidade->addMedia($request->file('foto'))->toMediaCollection();                
        }
        return redirect('admin/entidades')->with('success',"Editado com sucesso.");   
    }

    public function destroy(Entidade $entidade)
    {
        $entidade->delete();
        return redirect('admin/entidades')->with('success',"Excluído com sucesso.");   
    }

    public function usuarios(Entidade $entidade)
    {
        return $entidade->users()->get();
    }
    public function addUserPage(Entidade $entidade)
    {
        return view('admin/entidades/addUser', compact('entidade'));
    }
    public function addUsers(Entidade $entidade, Request $request)
    {   
        $entidade->users()->attach($request->users);
    }
    public function destroyUser(Entidade $entidade, Request $request)
    {
        $entidade->users()->detach($request->userID);
    }

    public function cidades(Entidade $entidade)
    {
        return $entidade->cidades()->get();
    }
    public function addCidades(Entidade $entidade, Request $request)
    {   
        foreach ($request->cidades as $key => $value) {
            $cidade = Cidade::findOrFail($value);
            if ($cidade) {
                $entidade->cidades()->save($cidade);
            }
        }
    }
    public function destroyCidade(Entidade $entidade, Request $request)
    {
        $cidade = Cidade::findOrFail($request->cidadeID);
        if ($cidade) {
            $cidade->entidade_id = null;
            $cidade->save();
        }
    }

    public function datatablesAjax()
    {
        $entidades = Entidade::query();
        if (!auth()->user()->hasRole('admin')) {        
            $entidades->has('users');
        }

        return DataTables::of($entidades)    
        ->editColumn('telefone', function($row){
            return "($row->ddd_telefone) $row->telefone";
        })
        ->editColumn('whatsapp', function($row){
            return "($row->ddd_whatsapp) $row->whatsapp";
        })
        ->editColumn('telefone_fixo_presidente', function($row){
            return "($row->ddd_telefone_fixo_presidente) $row->telefone_fixo_presidente";
        })
        ->editColumn('celular_presidente', function($row){
            return "($row->ddd_celular_presidente) $row->celular_presidente";
        })
        ->editColumn('telefone_fixo_financeiro', function($row){
            return "($row->ddd_telefone_fixo_financeiro) $row->telefone_fixo_financeiro";
        })
        ->editColumn('celular_financeiro', function($row){
            return "($row->ddd_celular_financeiro) $row->celular_financeiro";
        })
        ->editColumn('telefone_fixo_gerente', function($row){
            return "($row->ddd_telefone_fixo_gerente) $row->telefone_fixo_gerente";
        })
        ->editColumn('celular_gerente', function($row){
            return "($row->ddd_celular_gerente) $row->celular_gerente";
        })
        ->addColumn('usuarios', function($row){
            return '<button onclick="ajaxThenCallModal('.$row->id.',\''.$row->nome.'\')" class="btn btn-default"><i class="fas fa-eye"></i> Ver</button>';
        })
        ->addColumn('cidades', function($row){
            return '<button onclick="ajaxThenCallModalCidades('.$row->id.',\''.$row->nome.'\')" class="btn btn-default"><i class="fas fa-eye"></i> Ver</button>';
        })
        ->addColumn('action', function($row){
            $action = '<a href=' . route("entidades.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>';

            if (auth()->user()->hasRole('admin')) {        
                $action .= '<form action=' . route("entidades.destroy", $row->id) . ' method="POST"                        
                        onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                    <input type="hidden" name="_method" value="DELETE">
                    ' . csrf_field() . '
                    <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                </form>';
            }

            return $action;
        })
        ->rawColumns(['action','usuarios','cidades'])        
        ->make(true);
    }
}
