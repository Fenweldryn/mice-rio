<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index()
    {
        return view("admin.clientes.index");
    }
    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        
    }
    
    public function show(Cliente $cliente)
    {
        //
    }

    public function edit(Cliente $cliente)
    {
        return view('admin.clientes.edit', compact('cliente'));
    }

    public function update(Request $request, Cliente $cliente)
    {
        $this->validate($request, [
            "name" => "required|min:3|max:191"
        ]);        
        $cliente->name = $request['name'];
        $cliente->update();
        return redirect('admin/clientes')->with('success',"Usuário editado com sucesso.");   
    }

    public function destroy(Cliente $cliente)
    {
        $cliente->delete();
        return redirect('admin/clientes')->with('success',"Usuário excluído com sucesso.");   
    }

    public function datatablesAjax()
    {
        $clientes = cliente::query();
        return DataTables::of($clientes)            
            ->addColumn('action', function($row){
                $btn = '<a href=' . route("clientes.edit", $row->id) . ' class="btn btn-default">Edit</a>
                        <form action=' . route("clientes.destroy", $row->id) . ' method="POST"
                                style="display: inline"
                                onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                            <input type="hidden" name="_method" value="DELETE">
                            ' . csrf_field() . '
                            <button class="btn btn-danger">Delete</button>
                        </form>';
                return $btn;
            })
            ->rawColumns(['action'])
        ->make(true);
    }
}
