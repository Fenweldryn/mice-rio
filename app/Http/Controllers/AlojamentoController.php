<?php

namespace App\Http\Controllers;

use App\Alojamento;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AlojamentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:ver hospedagens');
        $this->middleware('permission:gerir hospedagens',['except' => ['datatablesAjax']]);
    }

    public function index()
    {
        return view("admin.alojamentos.index");
    }
    
    public function create()
    {
        $alojamento_tipos = \App\AlojamentoTipo::all();
        $alojamento_categorias = \App\AlojamentoCategoria::all();
        $cidades = \App\Cidade::has('entidade')->get();
        return view("admin.alojamentos.create", compact('alojamento_tipos','alojamento_categorias','cidades'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "descricao" => "required|min:3|max:191",
            "capacidade" => "required|numeric",
            "alojamento_tipo_id" => "required",
            "alojamento_categoria_id" => "required",
            "cidade_id" => "required"
        ]);        
        $alojamento = new Alojamento;
        $alojamento->nome = $request['nome'];
        $alojamento->descricao = $request['descricao'];
        $alojamento->capacidade = $request['capacidade'];
        $alojamento->alojamento_tipo_id = $request['alojamento_tipo_id'];                
        $alojamento->alojamento_categoria_id = $request['alojamento_categoria_id'];                
        $alojamento->cidade_id = $request['cidade_id'];        
        $alojamento->save();
        
        return redirect('admin/alojamentos')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(Alojamento $alojamento)
    {
        dd($alojamento->cidade);
    }
    
    public function edit(Alojamento $alojamento)
    {
        $alojamento_tipos = \App\AlojamentoTipo::all();
        $alojamento_categorias = \App\AlojamentoCategoria::all();
        $cidades = \App\Cidade::all();
        return view('admin.alojamentos.edit', compact('alojamento','alojamento_tipos','cidades'));
    }

    public function update(Request $request, Alojamento $alojamento)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "descricao" => "required|min:3|max:191",
            "capacidade" => "required|numeric",
            "alojamento_tipo_id" => "required",
            "alojamento_categoria_id" => "required",
            "cidade_id" => "required"
        ]);        
        $alojamento->nome = $request['nome'];
        $alojamento->descricao = $request['descricao'];
        $alojamento->capacidade = $request['capacidade'];
        $alojamento->alojamento_tipo_id = $request['alojamento_tipo_id'];                
        $alojamento->alojamento_categoria_id = $request['alojamento_categoria_id'];                
        $alojamento->cidade_id = $request['cidade_id'];    
        $alojamento->update();
        return redirect('admin/alojamentos')->with('success',"Editado com sucesso.");   
    }

    public function destroy(Alojamento $alojamento)
    {
        $alojamento->delete();
        return redirect('admin/alojamentos')->with('success',"Excluído com sucesso.");   
    }

    public function datatablesAjax()
    {
        $alojamentos = Alojamento::query();
        if (!auth()->user()->hasRole('admin')) {        
            $alojamentos = $alojamentos->has('cidade.entidade');
        }
        
        return DataTables::of($alojamentos)         
            ->addColumn("cidade", function($row){                
                return $row->cidade->nome;
            })         
            ->addColumn("tipo", function($row){
                return $row->alojamentoTipo->nome;
            })         
            ->addColumn("categoria", function($row){
                return $row->alojamentoCategoria->nome;
            })         
            ->addColumn('action', function($row){
                $btn = "";
                if (auth()->user()->can('gerir hospedagem'))
                {
                    $btn = '<a href=' . route("alojamentos.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                        <form action=' . route("alojamentos.destroy", $row->id) . ' method="POST"
                                style="display: inline"
                                onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                            <input type="hidden" name="_method" value="DELETE">
                            ' . csrf_field() . '
                            <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                        </form>';
                }
                return $btn;
            })
            ->rawColumns(['action','cidade','tipo','categoria'])
        ->make(true);
    }
}
