<?php

namespace App\Http\Controllers;

use App\Permuta;
use App\Associado;
use App\Entidade;
use Illuminate\Http\Request;

class PermutaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(auth()->user()->entidade);
        // if (auth()->user()->hasRole('admin')) {
        // } else {
        //     $permutas = Permuta::whereIn('entidade_id', auth()->user()->entidade->pluck('id')->toArray())->get();
        // }
        $permutas = Permuta::all();
        // dd($permutas);
        return view('admin.permutas.index', compact('permutas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $associados = Associado::all();
        $entidades = Entidade::all();
        return view('admin.permutas.create', compact('associados', 'entidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'associado_id' => 'required|numeric',
            'entidade_id' => 'required|numeric',
            'descricao' => 'required|min:3|max:255',
            'quantidade_por_mes' => 'required|numeric'
        ]);

        Permuta::create([
            'associado_id' => $request->associado_id,
            'entidade_id' => $request->entidade_id,
            'descricao' => $request->descricao,
            'quantidade_por_mes' => $request->quantidade_por_mes,
            'quantidade_disponivel' => $request->quantidade_por_mes
        ]);

        return redirect('admin/permutas')->with('success', 'Cadastrado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permuta  $permuta
     * @return \Illuminate\Http\Response
     */
    public function show(Permuta $permuta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permuta  $permuta
     * @return \Illuminate\Http\Response
     */
    public function edit(Permuta $permuta)
    {
        $associados = Associado::all();
        $entidades = Entidade::all();
        return view('admin.permutas.edit', compact('permuta', 'associados', 'entidades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permuta  $permuta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permuta $permuta)
    {
        $this->validate($request, [
            'associado_id' => 'required|numeric',
            'entidade_id' => 'required|numeric',
            'descricao' => 'required|min:3|max:255',
            'quantidade_por_mes' => 'required|numeric'
        ]);

        $permuta->associado_id = $request->associado_id;
        $permuta->entidade_id = $request->entidade_id;
        $permuta->descricao = $request->descricao;
        $permuta->quantidade_por_mes = $request->quantidade_por_mes;
        $permuta->update();

        return redirect('admin/permutas')->with('success', 'Atualizado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permuta  $permuta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permuta $permuta)
    {
        $permuta->delete();
        return redirect('admin/permutas')->with('success', 'Excluído com sucesso');
    }
}
