<?php

namespace App\Http\Controllers;

use App\LocalEvento;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class LocalEventoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:ver locais evento']);
        $this->middleware(['permission:gerir locais evento'],['except' => ['datatablesAjax']]);
    }

    public function index()
    {
        return view("admin.localeventos.index");
    }
    
    public function create()
    {
        $local_evento_tipos = \App\LocalEventoTipo::all();
        $cidades = \App\Cidade::has('entidade')->get();
        return view("admin.localeventos.create", compact('local_evento_tipos','cidades'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "descricao" => "required|min:3|max:191",
            "capacidade_auditorio" => "required|numeric",
            "local_evento_tipo_id" => "required",
            "cidade_id" => "required"
        ]);        
        $localevento = new LocalEvento;
        $localevento->nome = $request['nome'];
        $localevento->descricao = $request['descricao'];
        $localevento->capacidade_auditorio = $request['capacidade_auditorio'];
        $localevento->local_evento_tipo_id = $request['local_evento_tipo_id'];                
        $localevento->cidade_id = $request['cidade_id'];        
        $localevento->save();
        return redirect('admin/localeventos')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(LocalEvento $localevento)
    {
        dd($localevento->cidade);
    }

    public function edit(LocalEvento $localevento)
    {
        $local_evento_tipos = \App\LocalEventoTipo::all();
        $cidades = \App\Cidade::has('entidade')->get();
        return view('admin.localeventos.edit', compact('localevento','local_evento_tipos','cidades'));
    }

    public function update(Request $request, LocalEvento $localevento)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "descricao" => "required|min:3|max:191",
            "capacidade_auditorio" => "required|numeric",
            "local_evento_tipo_id" => "required",
            "cidade_id" => "required"
        ]);        
        $localevento->nome = $request['nome'];
        $localevento->descricao = $request['descricao'];
        $localevento->capacidade_auditorio = $request['capacidade_auditorio'];
        $localevento->local_evento_tipo_id = $request['local_evento_tipo_id'];                
        $localevento->cidade_id = $request['cidade_id'];    
        $localevento->update();
        return redirect('admin/localeventos')->with('success',"Editado com sucesso.");   
    }

    public function destroy(LocalEvento $localevento)
    {
        $localevento->delete();
        return redirect('admin/localeventos')->with('success',"Excluído com sucesso.");   
    }

    public function datatablesAjax()
    {
        $localeventos = LocalEvento::query();
        if (!auth()->user()->hasRole('admin')) {        
            $localeventos = $localeventos->has('cidade.entidade');
        }
        
        return DataTables::of($localeventos)         
            ->addColumn("cidade", function($row){                
                return $row->cidade->nome;
            })         
            ->addColumn("tipo", function($row){
                return $row->localEventoTipo->nome;
            })         
            ->addColumn('action', function($row){
                $btn = "";
                if (auth()->user()->can('gerir locais evento'))
                {
                    $btn = '<a href=' . route("localeventos.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                        <form action=' . route("localeventos.destroy", $row->id) . ' method="POST"
                                style="display: inline"
                                onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                            <input type="hidden" name="_method" value="DELETE">
                            ' . csrf_field() . '
                            <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                        </form>';
                }
                return $btn;
            })
            ->rawColumns(['action','cidade','tipo'])
        ->make(true);
    }
}
