<?php

namespace App\Http\Controllers;

use App\HistoricoPermuta;
use App\Permuta;
use App\Associado;
use App\Entidade;
use Illuminate\Http\Request;

class HistoricoPermutaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Permuta $permuta)
    {
        $historicos = $permuta->historico;
        return view('admin.historicopermutas.create', compact('historicos','permuta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'quantidade' => 'required|numeric',
            'permuta_id' => 'required|numeric',
            'associado_id' => 'required|numeric',
            'usado_em' => 'required|date_format:d/m/Y',
        ]);

        $historicoPermuta = new HistoricoPermuta;
        $historicoPermuta->quantidade = $request->quantidade;
        $historicoPermuta->permuta_id = $request->permuta_id;
        $historicoPermuta->user_id = auth()->user()->id;
        $historicoPermuta->usado_em = \Carbon\Carbon::createFromFormat('d/m/Y', $request->usado_em)->format('Y-m-d');
        $historicoPermuta->save();

        return back()->with('success', 'Baixa cadastrada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HistoricoPermuta  $historicoPermuta
     * @return \Illuminate\Http\Response
     */
    public function show(HistoricoPermuta $historicoPermuta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HistoricoPermuta  $historicoPermuta
     * @return \Illuminate\Http\Response
     */
    public function edit(HistoricoPermuta $historicoPermuta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HistoricoPermuta  $historicoPermuta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HistoricoPermuta $historicoPermuta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HistoricoPermuta  $historicoPermuta
     * @return \Illuminate\Http\Response
     */
    public function destroy($historicoPermuta)
    {
        HistoricoPermuta::find($historicoPermuta)->delete();        
        return back()->with('success', 'Excluído com sucesso');
    }
}
