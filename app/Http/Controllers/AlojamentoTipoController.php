<?php

namespace App\Http\Controllers;

use App\AlojamentoTipo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AlojamentoTipoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:ver hospedagens']);
        $this->middleware(['role:admin'],['except' => ['index','datatablesAjax','list']]);
    }

    public function index()
    {
        return view("admin.alojamentotipos.index");
    }
    
    public function list()
    {
        return AlojamentoTipo::select('id','nome')->get();
    }

    public function create()
    {
        return view("admin.alojamentotipos.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);        
        $alojamentotipo = new AlojamentoTipo;
        $alojamentotipo->nome = $request['nome'];
        $alojamentotipo->save();
        return redirect('admin/alojamentotipos')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(AlojamentoTipo $alojamentotipo)
    {
        dd($alojamentotipo);
    }

    public function edit(AlojamentoTipo $alojamentotipo)
    {
        $cidades = \App\Cidade::all();
        return view('admin.alojamentotipos.edit', compact('alojamentotipo','cidades'));
    }

    public function update(Request $request, AlojamentoTipo $alojamentotipo)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);        
        $alojamentotipo->nome = $request['nome'];
        $alojamentotipo->update();
        return redirect('admin/alojamentotipos')->with('success',"Editado com sucesso.");   
    }

    public function destroy(AlojamentoTipo $alojamentotipo)
    {
        $alojamentotipo->delete();
        return redirect('admin/alojamentotipos')->with('success',"Excluído com sucesso.");   
    }

    public function caracteristicas(AlojamentoTipo $alojamentotipo)
    {
        return $alojamentotipo->caracteristicas()->get();
    }

    public function datatablesAjax()
    {
        $alojamentotipotipos = AlojamentoTipo::query();
        return DataTables::of($alojamentotipotipos)         
            ->addColumn('action', function($row){
                $btn = "";
                if (auth()->user()->hasRole('admin')) {
                    $btn = '<a href=' . route("alojamentotipos.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                            <form action=' . route("alojamentotipos.destroy", $row->id) . ' method="POST"
                                    style="display: inline"
                                    onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                                <input type="hidden" name="_method" value="DELETE">
                                ' . csrf_field() . '
                                <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                            </form>';
                }
                return $btn;
            })
            ->rawColumns(['action'])
        ->make(true);
    }
}
