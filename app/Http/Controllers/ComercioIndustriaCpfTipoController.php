<?php

namespace App\Http\Controllers;

use App\ComercioIndustriaCpfTipo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ComercioIndustriaCpfTipoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:ver hospedagens']);
        $this->middleware(['role:admin'], ['except' => ['index', 'datatablesAjax', 'list']]);
    }

    public function index()
    {
        return view("admin.comercioindustriacpftipos.index");
    }

    public function list()
    {
        return ComercioIndustriaCpfTipo::select('id', 'nome')->get();
    }

    public function create()
    {
        return view("admin.comercioindustriacpftipos.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);
        $comercioindustriacpftipo = new ComercioIndustriaCpfTipo;
        $comercioindustriacpftipo->nome = $request['nome'];
        $comercioindustriacpftipo->save();
        return redirect('admin/comercioindustriacpftipos')->with('success', "Cadastrado com sucesso.");
    }

    public function show(ComercioIndustriaCpfTipo $comercioindustriacpftipo)
    {
        dd($comercioindustriacpftipo);
    }

    public function edit(ComercioIndustriaCpfTipo $comercioindustriacpftipo)
    {
        $cidades = \App\Cidade::all();
        return view('admin.comercioindustriacpftipos.edit', compact('comercioindustriacpftipo', 'cidades'));
    }

    public function update(Request $request, ComercioIndustriaCpfTipo $comercioindustriacpftipo)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);
        $comercioindustriacpftipo->nome = $request['nome'];
        $comercioindustriacpftipo->update();
        return redirect('admin/comercioindustriacpftipos')->with('success', "Editado com sucesso.");
    }

    public function destroy(ComercioIndustriaCpfTipo $comercioindustriacpftipo)
    {
        $comercioindustriacpftipo->delete();
        return redirect('admin/comercioindustriacpftipos')->with('success', "Excluído com sucesso.");
    }

    public function caracteristicas(ComercioIndustriaCpfTipo $comercioindustriacpftipo)
    {
        return $comercioindustriacpftipo->caracteristicas()->get();
    }

    public function datatablesAjax()
    {
        $comercioindustriacpftipotipos = ComercioIndustriaCpfTipo::query();
        return DataTables::of($comercioindustriacpftipotipos)
            ->addColumn('action', function ($row) {
                $btn = "";
                if (auth()->user()->hasRole('admin')) {
                    $btn = '<a href=' . route("comercioindustriacpftipos.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                            <form action=' . route("comercioindustriacpftipos.destroy", $row->id) . ' method="POST"
                                    style="display: inline"
                                    onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                                <input type="hidden" name="_method" value="DELETE">
                                ' . csrf_field() . '
                                <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                            </form>';
                }
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
