<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $modalTipos = \App\ModalTipo::where('tipo','=','acesso')->get();
        $localEventoTipos = \App\LocalEventoTipo::all();
        $alojamentoTipos = \App\AlojamentoTipo::all();
        $alojamentoCategorias = \App\AlojamentoCategoria::all();
        $cidades = \App\Cidade::orderBy('nome','asc')->where('is_active',1)->get();
        return view("index", compact('modalTipos','localEventoTipos', 'alojamentoTipos', 'alojamentoCategorias','cidades'));
    }
}
