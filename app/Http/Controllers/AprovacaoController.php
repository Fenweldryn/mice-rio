<?php

namespace App\Http\Controllers;

use App\Cidade;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Spatie\Activitylog\Models\Activity;

class AprovacaoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:gestor|admin'],['except' => ['datatablesAjax']]);
    }    

    public function index()
    {
        return view("admin.aprovacoes.index");
    }

    public function list()
    {
        return aprovacao::select('id','nome')->get();
    }

    public function show($logID)
    {        
              
    }

    public function edit($logID)
    {
        $cidade = Cidade::findCidadeByLogId($logID);
        $cidade = Cidade::find($cidade->id);
        $cidade_new = Activity::find($logID)->properties;
        return view('admin.cidades.edit', compact('cidade','cidade_new','logID'));
    }

    public function approve(Request $request)
    {
        $log = Activity::find($request->logID);
        $cidade = Cidade::find($log->subject_id);
        foreach ($log->properties as $key => $value) {
            $cidade->$key = $value;
        }
        $cidade->approve($request->logID);        
        $cidade->save();
        $request->session()->flash('success', "Aprovado com sucesso.");        
    }

    public function reject(Request $request)
    {        
        $log = Activity::find($request->logID);
        $cidade = Cidade::find($log->subject_id);
        $cidade->reject($request->logID, $request->motivo);        
        $request->session()->flash('success', "Rejeitado com sucesso.\nMotivo:" . $request->motivo );        
    }

    public function update(Request $request, $logID)
    {        
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "descricao" => "required",
            "estado" => "required|min:3|max:191",
            "estado_sigla" => "required|min:2|max:2",
            "distancia_capital" => "required|numeric",
            "tem_centro_atendimento" => "required|boolean",
            "tem_rodovia_estadual" => "required|boolean",
            "tem_rodovia_federal" => "required|boolean",                        
            "fotos" => "required|sometimes",                
            "fotos.*" => 'nullable|image'            
        ]);                
        $cidade->nome = $request['nome'];
        $cidade->descricao = $request['descricao'];
        $cidade->estado = $request['estado'];
        $cidade->estado_sigla = $request['estado_sigla'];
        $cidade->distancia_capital = $request['distancia_capital'];
        $cidade->tem_centro_atendimento = $request['tem_centro_atendimento'];
        $cidade->tem_rodovia_estadual = $request['tem_rodovia_estadual'];
        $cidade->tem_rodovia_federal = $request['tem_rodovia_federal'];
        $cidade->capacidade_maior_espaco = $request['capacidade_maior_espaco'];
        $cidade->capacidade_maior_auditorio = $request['capacidade_maior_auditorio'];
        $cidade->capacidade_maior_banquete = $request['capacidade_maior_banquete'];                
        $cidade->capacidade_total_quartos = $request['capacidade_total_quartos'];                
        $cidade->capacidade_total_leitos = $request['capacidade_total_leitos'];                
        $cidade->capacidade_quartos_maior_hotel = $request['capacidade_quartos_maior_hotel'];                
        $cidade->numero_instituicoes_ensino_superior = $request['numero_instituicoes_ensino_superior'];        
        $cidade->areas_expertise_referencia = $request['areas_expertise_referencia'];         
        $cidade->principais_eventos = $request['principais_eventos'];    

        if($request['approve'])
        {
            $log = Activity::find($request['approve']);
            $log->causer_id = auth()->user()->id;
            $log->description = 'approved';
            $log->save();
        }
        $cidade->update();

        if($request->hasfile('fotos'))
        {
            foreach($request->file('fotos') as $foto)
            {
                $cidade->addMedia($foto)->toMediaCollection();
            }    
        }
        
        return redirect('admin/cidades')->with('success',"Editado com sucesso.");               
    }

    public function datatablesAjax()
    {
        $aprovacoes = Cidade::pendingAndRejected();
        
        return DataTables::of($aprovacoes)                
            ->editColumn('log_created_at', '{{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$log_created_at)->format("d/m/Y H:i:s")}}')               
            ->addColumn('status', function($row){
                $status = "";
                if (strpos($row->description, "rejected") !== FALSE ) {
                    $status = "<button class='btn btn-flat btn-danger'>REJEITADO</button>";
                } else {
                    $texto = "AGUARDANDO APROVAÇÃO";
                    if(auth()->user()->hasRole('admin'))
                    {
                        $texto = "NOVO";
                    } 

                    $status = "<button class='btn btn-flat btn-warning'>$texto</button>";
                }
                return $status;
            })               
            ->addColumn('motivo_devolucao', function($row){
                $motivo = "";
                if (strpos($row->description, "rejected") !== FALSE ) {
                    $motivo = str_replace("rejected|","",$row->description);
                }
                return $motivo;
            })               
            ->addColumn('action', function($row){
                $btn = "";
            
                $btn = '<a href=' . url("admin/aprovacoes") . "/" . $row->alid . '/edit class="btn btn-primary"><i class="fas fa-eye"></i> Analisar</a>';                    
                
                return $btn;
            })
            ->rawColumns(['action','status'])
        ->make(true);
    }
}
