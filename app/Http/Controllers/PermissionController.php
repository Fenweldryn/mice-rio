<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PermissionController extends Controller
{
    public function index()
    {
        return view("admin.permissions.index");
    }

    public function list()
    {
        return Permission::all();
    }
    
    public function create()
    {
        return view("admin.permissions.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|min:3|max:191",
        ]);                
        $permission = new Permission;
        $permission->name = $request['name'];        
        $permission->save();
        return redirect('admin/permissions')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(Permission $permission)
    {
        dd($permission);
    }

    public function destroy(Permission $permission)
    {
        if($permission->id > 20) 
        {
            $permission->delete();
        }
        return redirect('admin/permissions')->with('success',"Excluído com sucesso.");   
    }
    
    public function datatablesAjax()
    {
        $permission = Permission::query();
        return DataTables::of($permission)                  
            ->addColumn('action', function($row){
                $btn = "";
                if($row->id > 20) 
                {
                    $btn = '<form action=' . route("permissions.destroy", $row->id) . ' method="POST"
                                    style="display: inline"
                                    onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                                <input type="hidden" name="_method" value="DELETE">
                                ' . csrf_field() . '
                                <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                            </form>';
                }
                return $btn;
            })
            ->rawColumns(['action'])
        ->make(true);
    }
}
