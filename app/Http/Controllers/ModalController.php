<?php

namespace App\Http\Controllers;

use App\Modal;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ModalController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:ver modais']);
        $this->middleware(['permission:gerir modais'],['except' => ['datatablesAjax']]);
    }

    public function index()
    {
        return view("admin.modais.index");
    }
    
    public function create()
    {
        $modal_tipos = \App\ModalTipo::all();
        $cidades = \App\Cidade::has('entidade')->get();
        return view("admin.modais.create", compact('modal_tipos','cidades'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "modal_tipo_id" => "required",
            "cidade_id" => "required"            
        ]);        
        $modal = new Modal;
        $modal->nome = $request['nome'];
        $modal->modal_tipo_id = $request['modal_tipo_id'];        
        $modal->cidade_id = $request['cidade_id'];        
        $modal->save();
        return redirect('admin/modais')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(Modal $modal)
    {
        dd($modal->cidade);
    }

    public function edit(Modal $modal)
    {
        $modal_tipos = \App\ModalTipo::all();
        $cidades = \App\Cidade::has('entidade')->get();
        return view('admin.modais.edit', compact('modal','modal_tipos','cidades'));
    }

    public function update(Request $request, Modal $modal)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "modal_tipo_id" => "required",
            "cidade_id" => "required"            
        ]);        
        $modal->nome = $request['nome'];
        $modal->modal_tipo_id = $request['modal_tipo_id'];       
        $modal->cidade_id = $request['cidade_id'];       
        $modal->update();
        return redirect('admin/modais')->with('success',"Editado com sucesso.");   
    }

    public function destroy(Modal $modal)
    {
        $modal->delete();
        return redirect('admin/modais')->with('success',"Excluído com sucesso.");   
    }

    public function datatablesAjax()
    {
        $modais = Modal::query();
        if (!auth()->user()->hasRole('admin')) {        
            $modais = $modais->has('cidade.entidade');
        }
        
        return DataTables::of($modais)         
            ->addColumn("cidade", function($row){                
                return $row->cidade->nome;
            })         
            ->addColumn("tipo", function($row){
                return $row->modalTipo->nome;
            })         
            ->addColumn('action', function($row){
                $btn = "";
                if (auth()->user()->can('gerir modais'))
                {
                    $btn = '<a href=' . route("modais.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                        <form action=' . route("modais.destroy", $row->id) . ' method="POST"
                                style="display: inline"
                                onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                            <input type="hidden" name="_method" value="DELETE">
                            ' . csrf_field() . '
                            <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                        </form>';
                }
                return $btn;
            })
            ->rawColumns(['action','cidade','tipo'])
        ->make(true);
    }
}
