<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Analytics;
use Spatie\Analytics\Period;

class AnalyticsController extends Controller
{
    public function index()
    {
       
    }

    public function visitorsAndPageViews(Request $request) 
    {
        $this->validate($request, [
            'inicio' => 'date_format:d/m/Y',
            'fim' => 'date_format:d/m/Y'
        ]);

        $dados = [];
        $dadosFormatados = [];

        $inicio = \DateTime::createFromFormat('d/m/Y', $request->inicio);
        $fim = \DateTime::createFromFormat('d/m/Y', $request->fim);
        $dados = Analytics::fetchTotalVisitorsAndPageViews(Period::create($inicio,$fim));

        foreach ($dados as $key => $value) {
            $dadosFormatados[$key]['date'] = $dados[$key]['date']->format('d/m/Y');            
            $dadosFormatados[$key]['visitors'] = $dados[$key]['visitors'];
            $dadosFormatados[$key]['pageViews'] = $dados[$key]['pageViews'];
        }
        
        return $dadosFormatados;
    } 
    public function mostVisitedPages(Request $request) 
    {
        $this->validate($request, [
            'inicio' => 'date_format:d/m/Y',
            'fim' => 'date_format:d/m/Y'
        ]);

        $dados = [];

        $inicio = \DateTime::createFromFormat('d/m/Y', $request->inicio);
        $fim = \DateTime::createFromFormat('d/m/Y', $request->fim);
        $dados = Analytics::fetchMostVisitedPages(Period::create($inicio,$fim), 10);
        foreach ($dados as $key => $value) {
            if (strpos($value['url'],'admin') !== false) {
                unset($dados[$key]);
            }
        }
        return $dados;
    } 

    public function visitorsCountry(Request $request)
    {
        $this->validate($request, [
            'inicio' => 'date_format:d/m/Y',
            'fim' => 'date_format:d/m/Y'
        ]);
        $inicio = \DateTime::createFromFormat('d/m/Y', $request->inicio);
        $fim = \DateTime::createFromFormat('d/m/Y', $request->fim);
        $country = Analytics::performQuery(Period::create($inicio,$fim),'ga:sessions',  ['dimensions'=>'ga:country','sort'=>'-ga:sessions']);
        $result = collect($country['rows'] ?? [])->map(function (array $dateRow) {
            return [
                'label' =>  $dateRow[0],
                'value' => (int) $dateRow[1],
            ];
        });
        return $result;
    }
}
