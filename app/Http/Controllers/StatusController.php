<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;


class StatusController extends Controller
{
    public function index()
    {
        return view("admin.status.index");
    }
    
    public function create()
    {
        return view("admin.status.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "status" => "required",            
        ]);        
        $status = new Status;
        $status->status = $request['status'];        
        $status->save();
        return redirect('admin/status')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(Status $status)
    {
        dd($status);
    }

    public function edit(Status $status)
    {
        return view('admin.status.edit', compact('status'));
    }

    public function update(Request $request, Status $status)
    {
        $this->validate($request, [
            "status" => "required",
        ]);        
        $status->status = $request['status'];
        $status->update();
        return redirect('admin/status')->with('success',"Editado com sucesso.");   
    }

    public function destroy(Status $status)
    {
        $status->delete();
        return redirect('admin/status')->with('success',"Excluído com sucesso.");   
    }

    public function datatablesAjax()
    {
        $status = Status::query();
        return DataTables::of($status)            
            ->addColumn('action', function($row){
                $btn = '<a href=' . route("status.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                        <form action=' . route("status.destroy", $row->id) . ' method="POST"
                                style="display: inline"
                                onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                            <input type="hidden" name="_method" value="DELETE">
                            ' . csrf_field() . '
                            <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                        </form>';
                return $btn;
            })
            ->rawColumns(['action'])
        ->make(true);
    }
}