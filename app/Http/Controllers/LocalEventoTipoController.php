<?php

namespace App\Http\Controllers;

use App\LocalEventoTipo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class LocalEventoTipoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:ver locais evento']);
        $this->middleware(['role:admin'],['except' => ['index','datatablesAjax','list']]);
    }

    public function index()
    {
        return view("admin.localeventotipos.index");
    }

    public function list()
    {
        return LocalEventoTipo::select('id','nome')->get();
    }
    
    public function create()
    {
        return view("admin.localeventotipos.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);        
        $localeventotipo = new LocalEventoTipo;
        $localeventotipo->nome = $request['nome'];
        $localeventotipo->save();
        return redirect('admin/localeventotipos')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(LocalEventoTipo $localeventotipo)
    {
        dd($localeventotipo);
    }

    public function edit(LocalEventoTipo $localeventotipo)
    {
        $cidades = \App\Cidade::all();
        return view('admin.localeventotipos.edit', compact('localeventotipo','cidades'));
    }

    public function update(Request $request, LocalEventoTipo $localeventotipo)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);        
        $localeventotipo->nome = $request['nome'];
        $localeventotipo->update();
        return redirect('admin/localeventotipos')->with('success',"Editado com sucesso.");   
    }

    public function destroy(LocalEventoTipo $localeventotipo)
    {
        $localeventotipo->delete();
        return redirect('admin/localeventotipos')->with('success',"Excluído com sucesso.");   
    }

    public function caracteristicas(LocalEventoTipo $localeventotipo)
    {
        return $localeventotipo->caracteristicas()->get();
    }

    public function datatablesAjax()
    {
        $localeventotipos = LocalEventoTipo::query();
        return DataTables::of($localeventotipos)         
            ->addColumn('action', function($row){
                $btn = "";
                if (auth()->user()->can('gerir locais evento'))
                {
                    $btn = '<a href=' . route("localeventotipos.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                        <form action=' . route("localeventotipos.destroy", $row->id) . ' method="POST"
                                style="display: inline"
                                onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                            <input type="hidden" name="_method" value="DELETE">
                            ' . csrf_field() . '
                            <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                        </form>';
                }
                return $btn;
            })
            ->rawColumns(['action'])
        ->make(true);
    }
}
