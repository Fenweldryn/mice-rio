<?php

namespace App\Http\Controllers;

use App\RestauranteTipo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RestauranteTipoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:ver hospedagens']);
        $this->middleware(['role:admin'], ['except' => ['index', 'datatablesAjax', 'list']]);
    }

    public function index()
    {
        return view("admin.restaurantetipos.index");
    }

    public function list()
    {
        return RestauranteTipo::select('id', 'nome')->get();
    }

    public function create()
    {
        return view("admin.restaurantetipos.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);
        $restaurantetipo = new RestauranteTipo;
        $restaurantetipo->nome = $request['nome'];
        $restaurantetipo->save();
        return redirect('admin/restaurantetipos')->with('success', "Cadastrado com sucesso.");
    }

    public function show(RestauranteTipo $restaurantetipo)
    {
        dd($restaurantetipo);
    }

    public function edit(RestauranteTipo $restaurantetipo)
    {
        $cidades = \App\Cidade::all();
        return view('admin.restaurantetipos.edit', compact('restaurantetipo', 'cidades'));
    }

    public function update(Request $request, RestauranteTipo $restaurantetipo)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);
        $restaurantetipo->nome = $request['nome'];
        $restaurantetipo->update();
        return redirect('admin/restaurantetipos')->with('success', "Editado com sucesso.");
    }

    public function destroy(RestauranteTipo $restaurantetipo)
    {
        $restaurantetipo->delete();
        return redirect('admin/restaurantetipos')->with('success', "Excluído com sucesso.");
    }

    public function caracteristicas(RestauranteTipo $restaurantetipo)
    {
        return $restaurantetipo->caracteristicas()->get();
    }

    public function datatablesAjax()
    {
        $restaurantetipotipos = RestauranteTipo::query();
        return DataTables::of($restaurantetipotipos)
            ->addColumn('action', function ($row) {
                $btn = "";
                if (auth()->user()->hasRole('admin')) {
                    $btn = '<a href=' . route("restaurantetipos.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                            <form action=' . route("restaurantetipos.destroy", $row->id) . ' method="POST"
                                    style="display: inline"
                                    onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                                <input type="hidden" name="_method" value="DELETE">
                                ' . csrf_field() . '
                                <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                            </form>';
                }
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
