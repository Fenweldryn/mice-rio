<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cidade;

class AdminController extends Controller
{
    public function index()
    {
        $usuariosCount = \App\User::count();
        $cidadesCount = \App\Cidade::count();
        $entidadesCount = \App\Entidade::count();
        $alojamentosCount = \App\Alojamento::count();
        $locaisEventoCount = \App\LocalEvento::count();
        $propostasCount = \App\Proposta::count();
        $infoCidades = $this->infoCidades();
        // dd($infoCidades);
        return view('admin/index', compact(
            'usuariosCount',
            'cidadesCount',
            'entidadesCount',
            'alojamentosCount',
            'locaisEventoCount',
            'propostasCount',
            'infoCidades'
        )); 
    }

    private function infoCidades()
    {
        $cidade = Cidade::select('capacidade_total_leitos','capacidade_maior_auditorio','capacidade_maior_banquete','capacidade_total_quartos','capacidade_quartos_maior_hotel','nome')->get();
        $cidade->capacidade_total_leitos = $cidade->sum("capacidade_total_leitos");
        $cidade->capacidade_maior_auditorio = $cidade->sum("capacidade_maior_auditorio");
        $cidade->capacidade_maior_banquete = $cidade->sum("capacidade_maior_banquete");
        $cidade->capacidade_total_quartos = $cidade->sum("capacidade_total_quartos");
        $cidade->capacidade_quartos_maior_hotel = $cidade->sum("capacidade_quartos_maior_hotel");
        return $cidade;
    }
}
