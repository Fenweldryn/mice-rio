<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cidade;
use App\Proposta;
use Yajra\DataTables\DataTables;

class PropostaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        return view("admin.propostas.index");
    }
    
    public function create()
    {
        return view("admin.propostas.create");
    }

    public function solicitarProposta($slug, Request $request)
    {
        $cidade = Cidade::where('slug',$slug)->get()->first();
        return view('novaproposta',compact('cidade'));
    }

    public function store(Request $request)
    {        
        $messages = [
            'inicio.after' => 'A data de início deve ser maior que a data de hoje',
            'fim.after' => 'A data de fim deve ser maior que a data de hoje',
        ];
        $this->validate($request, [
            'cidade_id' => "required|numeric",
            'nome_empresa' => "required|max:191",
            'cnpj' => "required|cnpj",
            'nome_contato' => "required|max:191",
            'telefone_contato' => "required|max:191",
            'nome_evento' => "required|max:191",
            'tipo_evento' => "required",
            'inicio' => "required|date_format:d/m/Y|after:today",
            'fim' => "required|date_format:d/m/Y|after:today",
            'pessoas' => "required|numeric|max:1000000",
            'formato' => "required",
            'salas_paralelas' => "required|numeric|max:1000000",
            'area_exposicao_m2' => "required|numeric|max:1000000",
            'necessita_hospedagem' => "required|boolean",
            'quartos' => "required|numeric|max:1000000",
            'passeios' => "required",
            'atividades_sociais' => "required"
        ], $messages);   
        $proposta = new Proposta;
        $proposta->status = 'nova';
        $proposta->user_id = auth()->user()->id;
        $proposta->cidade_id = $request['cidade_id'];
        $proposta->nome_empresa = $request['nome_empresa'];
        $proposta->cnpj = $request['cnpj'];
        $proposta->nome_contato = $request['nome_contato'];
        $proposta->telefone_contato = $request['telefone_contato'];
        $proposta->nome_evento = $request['nome_evento'];
        $proposta->tipo_evento = $request['tipo_evento'];
        $proposta->inicio = \Carbon\Carbon::CreateFromFormat('d/m/Y', $request['inicio'])->format('Y-m-d');
        $proposta->fim = \Carbon\Carbon::CreateFromFormat('d/m/Y', $request['fim'])->format('Y-m-d');
        $proposta->pessoas = $request['pessoas'];
        $proposta->formato = $request['formato'];
        $proposta->salas_paralelas = $request['salas_paralelas'];
        $proposta->area_exposicao_m2 = $request['area_exposicao_m2'];
        $proposta->necessita_hospedagem = $request['necessita_hospedagem'];
        $proposta->quartos = $request['quartos'];
        $proposta->passeios = $request['passeios'];
        $proposta->atividades_sociais = $request['atividades_sociais'];      
        $proposta->save();            
        
        $cidade = \App\Cidade::find($proposta->cidade_id);
        return redirect('proposta_enviada');
        // return back()->with(['success'=>"Solicitação enviada com sucesso.",'cidade' => $cidade]);       
    }

    public function show($proposta)
    {
        $proposta = Proposta::find($proposta);
        return view('admin.propostas.show', compact('proposta'));
    }

    public function colocarEmAnalise($proposta)
    {
        $proposta = Proposta::find($proposta);
        $proposta->status = 'em análise';
        $proposta->update();

        return back();
    }

    public function concluir($proposta)
    {
        $proposta = Proposta::find($proposta);
        $proposta->status = 'concluída';
        $proposta->update();

        return back();
    }

    public function edit(Proposta $proposta)
    {
        
    }

    public function update(Request $request, Proposta $proposta)
    {
        
    }

    public function destroy(Proposta $proposta)
    {
        $proposta->delete();
        return redirect('admin/propostas')->with('success',"Excluído com sucesso.");   
    }

    public function datatablesAjax()
    {
        $propostas = Proposta::query();

        if (!auth()->user()->hasRole('admin')) {        
            $propostas->has('cidade.entidade.users');
        }

        return DataTables::of($propostas)                    
        ->addColumn('action', function($row){
            return '<div class="d-flex" style="justify-content: space-evenly">
                <a href='.route("propostas.show", $row->id).' class="btn btn-primary mr-2"><i class="fas fa-eye"></i></a>
            </div>';
        })
        ->addColumn('cidade', function($row){
            return $row->cidade->nome;
        })
        ->addColumn('contato', function($row){
            return $row->nome_contato."\n".$row->telefone_contato;
        })
        ->addColumn('empresa', function($row){
            return $row->nome_empresa."\n".$row->cnpj;
        })
        ->addColumn('usuario', function($row){
            return $row->user->name."\n".$row->user->email;
        })
        ->rawColumns(['action','cidade','cliente'])        
        ->make(true);
    }
}
