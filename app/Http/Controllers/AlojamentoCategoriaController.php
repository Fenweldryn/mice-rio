<?php

namespace App\Http\Controllers;

use App\AlojamentoCategoria;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AlojamentoCategoriaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:ver hospedagens']);
        $this->middleware(['role:admin'],['except' => ['index','datatablesAjax','list']]);
    }

    public function index()
    {
        return view("admin.alojamentocategorias.index");
    }

    public function list()
    {
        return AlojamentoCategoria::select('id','nome')->get();
    }
    
    public function create()
    {
        return view("admin.alojamentocategorias.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);        
        $alojamentocategoria = new AlojamentoCategoria;
        $alojamentocategoria->nome = $request['nome'];
        $alojamentocategoria->save();
        return redirect('admin/alojamentocategorias')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(AlojamentoCategoria $alojamentocategoria)
    {
        dd($alojamentocategoria);
    }

    public function edit(AlojamentoCategoria $alojamentocategoria)
    {
        $cidades = \App\Cidade::all();
        return view('admin.alojamentocategorias.edit', compact('alojamentocategoria','cidades'));
    }

    public function update(Request $request, AlojamentoCategoria $alojamentocategoria)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
        ]);        
        $alojamentocategoria->nome = $request['nome'];
        $alojamentocategoria->update();
        return redirect('admin/alojamentocategorias')->with('success',"Editado com sucesso.");   
    }

    public function destroy(AlojamentoCategoria $alojamentocategoria)
    {
        $alojamentocategoria->delete();
        return redirect('admin/alojamentocategorias')->with('success',"Excluído com sucesso.");   
    }

    public function caracteristicas(AlojamentoCategoria $alojamentocategoria)
    {
        return $alojamentocategoria->caracteristicas()->get();
    }

    public function datatablesAjax()
    {
        $alojamentocategoriacategorias = AlojamentoCategoria::query();
        return DataTables::of($alojamentocategoriacategorias)         
            ->addColumn('action', function($row){
                $btn = "";
                if (auth()->user()->hasRole('admin')) {
                    $btn = '<a href=' . route("alojamentocategorias.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                            <form action=' . route("alojamentocategorias.destroy", $row->id) . ' method="POST"
                                    style="display: inline"
                                    onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                                <input type="hidden" name="_method" value="DELETE">
                                ' . csrf_field() . '
                                <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                            </form>';                    
                }
                return $btn;
            })
            ->rawColumns(['action'])
        ->make(true);
    }
}
