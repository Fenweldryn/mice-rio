<?php

namespace App\Http\Controllers;

use App\Aeroporto;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AeroportoController extends Controller
{
    public function index()
    {
        return view("admin.aeroportos.index");
    }
    
    public function create()
    {
        $cidades = \App\Cidade::all();
        return view("admin.aeroportos.create", compact('cidades'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "sigla" => "required|min:3|max:191",
            "tipo" => "required|min:3|max:191",
            "cidade_id" => "required"            
        ]);        
        $aeroporto = new Aeroporto;
        $aeroporto->nome = $request['nome'];
        $aeroporto->sigla = $request['sigla'];
        $aeroporto->tipo = $request['tipo'];        
        $aeroporto->cidade_id = $request['cidade_id'];        
        $aeroporto->save();
        return redirect('admin/aeroportos')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(Aeroporto $aeroporto)
    {
        dd($aeroporto);
    }

    public function edit(Aeroporto $aeroporto)
    {
        $cidades = \App\Cidade::all();
        return view('admin.aeroportos.edit', compact('aeroporto','cidades'));
    }

    public function update(Request $request, Aeroporto $aeroporto)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "sigla" => "required|min:3|max:191",
            "tipo" => "required|min:3|max:191",
            "cidade_id" => "required"            
        ]);        
        $aeroporto->nome = $request['nome'];
        $aeroporto->sigla = $request['sigla'];
        $aeroporto->tipo = $request['tipo'];       
        $aeroporto->cidade_id = $request['cidade_id'];       
        $aeroporto->update();
        return redirect('admin/aeroportos')->with('success',"Editado com sucesso.");   
    }

    public function destroy(Aeroporto $aeroporto)
    {
        $aeroporto->delete();
        return redirect('admin/aeroportos')->with('success',"Excluído com sucesso.");   
    }

    public function caracteristicas(Aeroporto $aeroporto)
    {
        return $aeroporto->caracteristicas()->get();
    }

    public function datatablesAjax()
    {
        $aeroportos = Aeroporto::query();
        return DataTables::of($aeroportos)         
            ->addColumn("cidade", function($row){
                return \App\Cidade::find($row->id)->nome;
            })         
            ->addColumn('action', function($row){
                $btn = '<a href=' . route("aeroportos.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                        <form action=' . route("aeroportos.destroy", $row->id) . ' method="POST"
                                style="display: inline"
                                onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                            <input type="hidden" name="_method" value="DELETE">
                            ' . csrf_field() . '
                            <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                        </form>';
                return $btn;
            })
            ->rawColumns(['action','cidade'])
        ->make(true);
    }
}
