<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:admin'],['except' => ['datatablesAjax','list']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.users.index");
    }

    public function list()
    {
        return User::select('id','name', 'email')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/users/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:8'],
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        return redirect('admin/users')->with('success',"Cadastrado com sucesso."); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:8']
        ]);        
        $user->name = $request['name'];
        $user->password = Hash::make($request->password);
        $user->update();
        return redirect('admin/users')->with('success',"Usuário editado com sucesso.");   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect('admin/users')->with('success',"Usuário excluído com sucesso.");   
    }

    public function roles(User $user)
    {
        return $user->roles()->get();                
    }

    public function addRoles(User $user, Request $request)
    {   
        $user->assignRole(Role::find($request->id)->name);
    }

    public function destroyRole(User $user, Request $request)
    {
        $user->removeRole(Role::find($request->id)->name);
    }

    public function datatablesAjax()
    {
        $this->middleware(['permission:ver usuários']);
        $users = User::query();
        return DataTables::of($users)    
            ->addColumn('roles', function($row){                
                $btn = '<button onclick="ajaxThenCallModal('.$row->id.',\''.$row->name.'\')" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></button>';
                return $btn;
            })              
            ->addColumn('action', function($row){
                $btn = '<a href=' . route("users.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        <form action=' . route("users.destroy", $row->id) . ' method="POST"
                                style="display: inline"
                                onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                            <input type="hidden" name="_method" value="DELETE">
                            ' . csrf_field() . '
                            <button class="btn btn-danger"><i class="fas fa-trash"></i></button>
                        </form>';
                return $btn;
            })
            ->rawColumns(['action','roles'])
        ->make(true);
    }

    private function formatToString($object, $fieldName)
    {
        $formattedString = "";
        foreach ($object as $key) {
            $formattedString .= $key->name . ', ';
        }        
        return rtrim($formattedString,', ');
    }
}
