<?php

namespace App\Http\Controllers;

use App\Cidade;
use App\AlojamentoTipo;
use App\Entidade;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CidadeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:gerir cidades'],['except' => ['show']]);
    }    

    public function index()
    {
        return view("admin.cidades.index");
    }

    public function list()
    {
        return Cidade::select('id','nome')->orderBy('nome')->get();
    }
    
    public function create()
    {
        $entidades = Entidade::all();
        return view("admin.cidades.create", compact('entidades'));
    }
    
    public function store(Request $request)
    {
        if ($request->user()->hasRole('admin'))
        {
            $this->validate($request, [
                "nome" => "required|min:3|max:191",
                "descricao" => "required|max:1500",
                "estado" => "required|min:3|max:191",
                "estado_sigla" => "required|min:2|max:2",
                "distancia_capital" => "required|numeric",
                "tem_centro_atendimento" => "required|boolean",
                "tem_rodovia_estadual" => "required|boolean",
                "tem_rodovia_federal" => "required|boolean",
                "capacidade_maior_espaco" => "required|numeric|min:0",
                "capacidade_maior_auditorio" => "required|numeric|min:0",
                "capacidade_maior_banquete" => "required|numeric|min:0",
                "capacidade_total_quartos" => "required|numeric|min:0",
                "capacidade_total_leitos" => "required|numeric|min:0",
                "capacidade_quartos_maior_hotel" => "required|numeric|min:0",
                "numero_instituicoes_ensino_superior" => "required|numeric|min:0",
                "areas_expertise_referencia" => "required",
                "principais_eventos" => "required",
                "entidade_id" => "required|numeric",                        
                "fotos" => "required",
                "fotos.*" => "image|max:7000",
            ]);   
            $cidade = new Cidade;
            $cidade->nome = $request['nome'];
            $cidade->descricao = $request['descricao'];
            $cidade->estado = $request['estado'];
            $cidade->estado_sigla = $request['estado_sigla'];
            $cidade->distancia_capital = $request['distancia_capital'];
            $cidade->tem_centro_atendimento = $request['tem_centro_atendimento'];
            $cidade->tem_rodovia_estadual = $request['tem_rodovia_estadual'];
            $cidade->tem_rodovia_federal = $request['tem_rodovia_federal'];
            $cidade->capacidade_maior_espaco = $request['capacidade_maior_espaco'];
            $cidade->capacidade_maior_auditorio = $request['capacidade_maior_auditorio'];
            $cidade->capacidade_maior_banquete = $request['capacidade_maior_banquete'];                
            $cidade->capacidade_total_quartos = $request['capacidade_total_quartos'];                
            $cidade->capacidade_total_leitos = $request['capacidade_total_leitos'];                
            $cidade->capacidade_quartos_maior_hotel = $request['capacidade_quartos_maior_hotel'];                
            $cidade->numero_instituicoes_ensino_superior = $request['numero_instituicoes_ensino_superior'];        
            $cidade->areas_expertise_referencia = $request['areas_expertise_referencia'];         
            $cidade->principais_eventos = $request['principais_eventos'];         
            $cidade->approval_status = 1;         
            $cidade->entidade_id = $request->entidade_id;         
            $cidade->approval_at = date('Y-m-d H:i:s');         
            $cidade->save();            
            if($request->hasfile('fotos'))
            {
                foreach($request->file('fotos') as $foto)
                {
                    $cidade->addMedia($foto)->toMediaCollection();
                }    
            }
            return redirect('admin/cidades')->with('success',"Cadastrado com sucesso."); 
        } else {
            return redirect('admin/cidades')->with('error',"Você não tem acesso para executar esta ação.");
        }
    }
    
    public function show($slug)
    {
        $cidade = Cidade::where('slug',$slug)->get()->first();
        session(['redirect_url' => str_replace(url(''),'', url()->full())]);                
        return view('cidade',compact('cidade'));        
    }

    public function edit(Cidade $cidade)
    {
        $entidades = Entidade::all();
        return view('admin.cidades.edit', compact('cidade','entidades'));
    }

    public function update(Request $request, Cidade $cidade)
    {        
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "descricao" => "required|max:1500",
            "estado" => "required|min:3|max:191",
            "estado_sigla" => "required|min:2|max:2",
            "distancia_capital" => "required|numeric",
            "tem_centro_atendimento" => "required|numeric",
            "tem_rodovia_estadual" => "required|numeric",
            "tem_rodovia_federal" => "required|numeric",                        
            "entidade_id" => "required|numeric",                        
            "fotos" => "required|sometimes",                
            "fotos.*" => 'nullable|image|max:7000'            
        ]);                
        $cidade->nome = $request['nome'];
        $cidade->descricao = $request['descricao'];
        $cidade->estado = $request['estado'];
        $cidade->estado_sigla = $request['estado_sigla'];
        $cidade->distancia_capital = $request['distancia_capital'];
        $cidade->tem_centro_atendimento = $request['tem_centro_atendimento'];
        $cidade->tem_rodovia_estadual = $request['tem_rodovia_estadual'];
        $cidade->tem_rodovia_federal = $request['tem_rodovia_federal'];
        $cidade->capacidade_maior_espaco = $request['capacidade_maior_espaco'];
        $cidade->capacidade_maior_auditorio = $request['capacidade_maior_auditorio'];
        $cidade->capacidade_maior_banquete = $request['capacidade_maior_banquete'];                
        $cidade->capacidade_total_quartos = $request['capacidade_total_quartos'];                
        $cidade->capacidade_total_leitos = $request['capacidade_total_leitos'];                
        $cidade->capacidade_quartos_maior_hotel = $request['capacidade_quartos_maior_hotel'];                
        $cidade->numero_instituicoes_ensino_superior = $request['numero_instituicoes_ensino_superior'];        
        $cidade->areas_expertise_referencia = $request['areas_expertise_referencia'];         
        $cidade->principais_eventos = $request['principais_eventos'];    
        $cidade->principais_eventos = $request['principais_eventos'];    
        $cidade->entidade_id = $request->entidade_id;    
        
        if ($request->user()->hasRole('admin'))
        {            
            $cidade->update();
        } else {
            $cidade_old = Cidade::find($cidade->id)->toArray();
            $cidade_new = array_udiff_assoc(
                $cidade->toArray(),
                $cidade_old,
                function ($new, $old) {
                    if ($old === null || $new === null) {
                        return $new === $old ? 0 : 1;
                    }

                    return $new <=> $old;
                }
            );
            $log = collect($cidade_new)
                ->only(array_keys($cidade_old))
                ->all();
            activity()->on($cidade)->withProperties($log)->log('edited');
        } 

        if($request->hasfile('fotos'))
        {
            foreach($request->file('fotos') as $foto)
            {
                $cidade->addMedia($foto)->toMediaCollection();
            }    
        }
        
        if ($request->user()->hasRole('admin')) 
        {
            return redirect('admin/cidades')->with('success',"Editado com sucesso.");               
        } else {
            return redirect('admin/cidades')->with('success',"Enviado para aprovação da administração.");               
        }
    }

    public function destroy(Cidade $cidade)
    {
        $cidade->delete();
        return redirect('admin/cidades')->with('success',"Excluído com sucesso.");   
    }

    public function destroyMedia(Cidade $cidade, Request $request)
    {        
        $cidade->getMedia()->where('id', $request->mediaID)->first()->delete();  
        $cidade->foto_principal = $cidade->getFirstMedia()->id;
        $cidade->update();      
    }

    public function alojamentoTipos(Cidade $cidade)
    {
        return $cidade->alojamentoTipos()->get();
    }
    public function addAlojamentoTipos(Cidade $cidade, Request $request)
    {   
        $cidade->alojamentoTipos()->attach($request->alojamentoTipos);
    }
    public function destroyAlojamentoTipos(Cidade $cidade, Request $request)
    {
        $cidade->alojamentoTipos()->detach($request->alojamentoTiposID);
    }
    
    public function modalTipos(Cidade $cidade)
    {
        return $cidade->modalTipos()->get();
    }
    public function addModalTipos(Cidade $cidade, Request $request)
    {   
        $cidade->modalTipos()->attach($request->modalTipos);
    }
    public function destroyModalTipos(Cidade $cidade, Request $request)
    {
        $cidade->modalTipos()->detach($request->modalTiposID);
    }

    public function localEventoTipos(Cidade $cidade)
    {
        return $cidade->localEventoTipos()->get();
    }
    public function addLocalEventoTipos(Cidade $cidade, Request $request)
    {   
        $cidade->localEventoTipos()->attach($request->localEventoTipos);
    }
    public function destroyLocalEventoTipos(Cidade $cidade, Request $request)
    {
        $cidade->localEventoTipos()->detach($request->localEventoTiposID);
    }

    public function alojamentoCategorias(Cidade $cidade)
    {
        return $cidade->alojamentoCategorias()->get();
    }
    public function addAlojamentoCategorias(Cidade $cidade, Request $request)
    {   
        $cidade->alojamentoCategorias()->attach($request->alojamentoCategorias);
    }
    public function destroyAlojamentoCategorias(Cidade $cidade, Request $request)
    {
        $cidade->alojamentoCategorias()->detach($request->alojamentoCategoriasID);
    }

    public function inativar(Cidade $cidade, Request $request)
    {
        $cidade->is_active = 0;
        $cidade->update();        
        $request->session()->flash('success', $cidade->nome . " foi inativada com sucesso.");        
    }
    public function ativar(Cidade $cidade, Request $request)
    {
        $cidade->is_active = 1;
        $cidade->update();
        $request->session()->flash('success', $cidade->nome . " foi ativada com sucesso.");        

    }
    public function definirFotoPrincipal(Cidade $cidade, Request $request)
    {
        $cidade->foto_principal = $request->mediaID;     
        $cidade->update();   
    }

    public function datatablesAjax()
    {
        $cidades = Cidade::query();

        if (!auth()->user()->hasRole('admin')) {        
            $cidades->has('entidade.users');
        }

        return DataTables::of($cidades) 
            ->editColumn('tem_centro_atendimento', '{{$tem_centro_atendimento ? "sim" : "não"}}')    
            ->editColumn('is_active', '{{$is_active ? "sim" : "não"}}')    
            ->editColumn('tem_rodovia_estadual', '{{$tem_rodovia_estadual ? "sim" : "não"}}')    
            ->editColumn('tem_rodovia_federal', '{{$tem_rodovia_federal ? "sim" : "não"}}')    
            ->editColumn('created_at', '{{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$created_at)->format("d/m/Y H:i:s")}}')               
            ->addColumn('alojamentoTipos', function($row){
                return '<button onclick="ajaxThenCallModalAlojamentoTipos('.$row->id.',\''.$row->nome.'\')" class="btn btn-default"><i class="fas fa-eye"></i> Ver</button>';
            })                 
            ->addColumn('modalTipos', function($row){
                return '<button onclick="ajaxThenCallModalModalTipos('.$row->id.',\''.$row->nome.'\')" class="btn btn-default"><i class="fas fa-eye"></i> Ver</button>';
            })          
            ->addColumn('localEventoTipos', function($row){
                return '<button onclick="ajaxThenCallModalLocalEventoTipos('.$row->id.',\''.$row->nome.'\')" class="btn btn-default"><i class="fas fa-eye"></i> Ver</button>';
            })                      
            ->addColumn('action', function($row){
                $btn = "";
                if (auth()->user()->can('gerir cidades'))
                {
                    $btn = '<a href=' . route("cidades.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>';                    
                }
                if (auth()->user()->hasRole('admin'))
                {
                    $btn .= ' <form action=' . route("cidades.destroy", $row->id) . ' method="POST"
                                    style="display: inline"
                                    onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                                <input type="hidden" name="_method" value="DELETE">
                                ' . csrf_field() . '
                                <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                            </form>';
                }

                
                return $btn;
            })
            ->rawColumns(['action','alojamentoTipos','alojamentoCategorias','modalTipos','localEventoTipos'])
        ->make(true);
    }
}
