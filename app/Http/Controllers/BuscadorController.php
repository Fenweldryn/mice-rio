<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Caracteristica;
use App\Cidade;
use App\ModalTipo;
use App\LocalEventoTipo;
use App\AlojamentoTipo;
use App\AlojamentoCategoria;

class BuscadorController extends Controller
{
    public function index()
    {
        $modalTipos = ModalTipo::where('tipo','=','acesso')->get();
        $localEventoTipos = LocalEventoTipo::all();
        $alojamentoTipos = AlojamentoTipo::all();
        $alojamentoCategorias = AlojamentoCategoria::all();
        return view("buscador", compact('modalTipos','localEventoTipos', 'alojamentoTipos', 'alojamentoCategorias'));
    }

    public function buscar(Request $request)
    {
        $this->validate($request, [
            "distancia_capital" => "sometimes|numeric",
            "acesso.item" => "sometimes|numeric",            
            "tipo_local.item" => "sometimes|numeric",            
            // "numero_pessoas" => "sometimes|numeric",
            // "tamanho_espaco" => "sometimes|numeric",
            // "tipos_hospedagem.item" => "sometimes|numeric",
            // "perfil_hoteleiro.item" => "sometimes|numeric",
        ]);    
        $parametros = [];
        $parametros['distancia_capital'] = $request->distancia_capital;
        $parametros['acesso'] = $request->acesso;
        $parametros['tipo_local'] = $request->tipo_local;
        $parametros['capacidade_auditorio'] = (int)substr($request->capacidade_auditorio,0,6);
        $parametros['tamanho_espaco'] = (int)substr($request->tamanho_espaco,0,6);
        $parametros['capacidade_banquete'] = (int)substr($request->capacidade_banquete,0,6);
        $request->capacidade_auditorio = $parametros['capacidade_auditorio'];
        $request->tamanho_espaco = $parametros['tamanho_espaco'];
        $request->capacidade_banquete = $parametros['capacidade_banquete'];
        // $parametros['tipos_hospedagem'] = $request->tipos_hospedagem;
        // $parametros['perfil_hoteleiro'] = $request->perfil_hoteleiro;
        $parametros = (object)$parametros;
        
        $resultado = Cidade::search($request);
        $modalTipos = ModalTipo::all();
        $localEventoTipos = LocalEventoTipo::all();
        $alojamentoTipos = AlojamentoTipo::all();
        $alojamentoCategorias = AlojamentoCategoria::all();
        
        session(['busca_url' => $request->fullUrl()]);

        return view("resultado", compact('resultado', 'parametros', 'modalTipos', 'localEventoTipos','alojamentoTipos','alojamentoCategorias'));
    }
}
