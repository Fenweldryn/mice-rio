<?php

namespace App\Http\Controllers;

use App\ModalTipo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ModalTipoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:ver modais']);
        $this->middleware(['role:admin'],['except' => ['index','datatablesAjax','list']]);
    }

    public function list()
    {
        return ModalTipo::select('id','nome','tipo')->get();
    }

    public function index()
    {
        return view("admin.modaltipos.index");
    }
    
    public function create()
    {
        return view("admin.modaltipos.create");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "tipo" => "required",
        ]);        
        $modal = new ModalTipo;
        $modal->nome = $request['nome'];
        $modal->tipo = $request['tipo'];
        $modal->save();
        return redirect('admin/modaltipos')->with('success',"Cadastrado com sucesso."); 
    }
    
    public function show(ModalTipo $modal)
    {
        dd($modal);
    }

    public function edit(ModalTipo $modaltipo)
    {
        $cidades = \App\Cidade::all();
        return view('admin.modaltipos.edit', compact('modaltipo','cidades'));
    }

    public function update(Request $request, ModalTipo $modaltipo)
    {
        $this->validate($request, [
            "nome" => "required|min:3|max:191",
            "tipo" => "required",
        ]);        
        $modaltipo->nome = $request['nome'];
        $modaltipo->tipo = $request['tipo'];
        $modaltipo->update();
        return redirect('admin/modaltipos')->with('success',"Editado com sucesso.");   
    }

    public function destroy(ModalTipo $modal)
    {
        $modal->delete();
        return redirect('admin/modaltipos')->with('success',"Excluído com sucesso.");   
    }

    public function caracteristicas(ModalTipo $modal)
    {
        return $modal->caracteristicas()->get();
    }

    public function datatablesAjax()
    {
        $modaltipos = ModalTipo::query();
        return DataTables::of($modaltipos)         
            ->addColumn('action', function($row){
                $btn = "";
                if (auth()->user()->hasRole('admin')) {
                    $btn = '<a href=' . route("modaltipos.edit", $row->id) . ' class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                        <form action=' . route("modaltipos.destroy", $row->id) . ' method="POST"
                                style="display: inline"
                                onsubmit=\'return confirm("Tem certeza que deseja excluir?");\'>
                            <input type="hidden" name="_method" value="DELETE">
                            ' . csrf_field() . '
                            <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                        </form>';
                }
                return $btn;
            })
            ->rawColumns(['action'])
        ->make(true);
    }
}
