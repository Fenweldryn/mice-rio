<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function enviar(Request $request) 
    {        
        if (!empty($request->mordor)) //honeypot
        {
            return response()->json(false);
        }
        $validator = Validator::make($request->all(), [
            "nome" => "required|min:3",
            "email" => "required|email",
            "assunto" => "required|min:3",
            "mensagem" => "required|min:3",  
        ]);
        if ($validator->passes()) {            
            $nome = $request->nome;
            $email = $request->email;
            $assunto = $request->assunto;
            $mensagem = $request->mensagem;
    
            try 
            {
                Mail::send(new \App\Mail\ContatoPrincipal($email, $nome, $assunto,$mensagem));                
                return response()->json(true);
            }
            catch(\Exception $e)
            {
                return response()->json(false);
            }

        } else {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
}
