<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssociadoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->has('telefones')) {
            foreach ($this->telefones as $key => $value) {
                $telefones[$key] = intval($value);
            }
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => "required|max:191",
            'mensalidade' => "required|regex:/^\d+(\,\d{1,2})?$/",            
            'entidade_id' => "required|numeric",
            'dia_vencimento' => "required|numeric",
            'site' => "nullable|string",
            'cnpj' => "required|cnpj",
            'tipo' => "required|numeric",
            'subtipo' => "required|numeric",
            'email' => "required|string",
            'endereco' => "required|string",
            'telefones' => "required|array",
            'telefones.*' => "required_with:telefones.*|min:0",
            'facebook' => "nullable|string",
            'instagram' => "nullable|string",
            'linkedin' => "nullable|string",
            'linkedin' => "nullable|string",
            'pinterest' => "nullable|string",
            'distancia_centro_km' => "nullable|regex:/^\d+(\,\d{1,2})?$/",
            'area_construida_total_m2' => "nullable|numeric|min:0",
            'area_evento_total_m2' => "nullable|numeric|min:0",
            'capacidade_lugares_auditorio' => "nullable|numeric|min:0",
            'capacidade_lugares_coquetel' => "nullable|numeric|min:0",
            'data_associacao' => "nullable|date:d/m/Y"
        ];
    }
}
