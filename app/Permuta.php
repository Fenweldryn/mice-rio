<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Permuta extends Model
{
    protected $with = ['associado', 'entidade'];
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('userid', function(Builder $builder) {
            if (!auth()->user()->hasRole('admin'))
            {
                $builder->whereHas('entidade.users', function($q) {
                    $q->where('user_id', auth()->user()->id);
                });
            }
        });
    }

    public function entidade()
    {
        return $this->belongsTo(Entidade::class);
    }

    public function associado()
    {
        return $this->belongsTo(Associado::class);
    }

    public function historico()
    {
        return $this->hasMany(HistoricoPermuta::class);
    }
}
