<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \App\Associado;
use \App\Pagamento;

class GerarDebitos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $associados = Associado::where('ativo', 1)->get();
        foreach ($associados as $associado) {
            $data_referencia = date('Y-m-01');
            
            if ($associado->pagamentos()->where('data_referencia', $data_referencia)->get()->count() == 0) {
                $debito = new Pagamento;
                $debito->associado_id = $associado->id;
                $debito->valor = $associado->mensalidade;
                $debito->data_referencia = $data_referencia;
                $debito->dia_vencimento = $dia_vencimento;
                $debito->save();                
            }

        }      
    }
}
