<?php

namespace App;

use App\Telefone;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Associado extends Model
{
    const DIAS_PARA_AVISO_VENCIMENTO = 7;

    protected $casts = [
        'data_associacao' => 'date:d/m/Y',
        'telefones' => 'json'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('userid', function(Builder $builder) {
            if (!auth()->user()->hasRole('admin'))
            {
                $builder->whereHas('entidade.users', function($q) {
                    $q->where('user_id', auth()->user()->id);
                });
            }
        });
    }

    public function tipo()
    {
        return $this->morphTo();
    }

    public function entidade()
    {
        return $this->belongsTo('App\Entidade');
    }

    public function pagamentos()
    {
        return $this->hasMany('App\Pagamento');
    }

    public function status()
    {
        $hoje = date('Y-m-d');                
        $pagamentosPendentes = $this->pagamentos()->whereNull('data_pagamento')->get();
        $status = "Em dia";
        
        if ($pagamentosPendentes->count() > 0) 
        {
            $status = "Pendente";
        }
        
        return $status;
    }

    public function pagamentosPendentes()
    {
        return $this->pagamentos()->whereNull('data_pagamento')->get();
    }

    public static function getVencido()
    {
        $hoje = date('Y-m');
        $dias_para_vencimento = \App\Associados::DIAS_PARA_AVISO_VENCIMENTO;
        \App\Pagamento::whereNull("data_pagamento");
    }

    public static function emDia()
    {
        $hoje = date('Y-m-01');
        $dias_para_vencimento = Associado::DIAS_PARA_AVISO_VENCIMENTO;        
        $dataVencimento = date("Y-m-$dias_para_vencimento");
        $dataVencimentoMaisUmMes = date("Y-m-$dias_para_vencimento");

        return Associado::WhereNotExists(function($q) {
            $q->from('pagamentos')->whereRaw('pagamentos.associado_id = associados.id')->whereNull('data_pagamento');
        }); 
    }

}
