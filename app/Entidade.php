<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Database\Eloquent\Builder;

class Entidade extends Model implements HasMedia
{
    use HasMediaTrait;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('userid', function(Builder $builder) {
            if (auth()->user()) {
                if (!auth()->user()->hasRole('admin') && auth()->user()->hasRole('gestor'))
                {
                    $builder->whereHas('users', function($q) {
                        $q->where('user_id', auth()->user()->id);
                    });
                }                
            }
        });
    }

    public function cidades()
    {
        return $this->hasMany('App\Cidade');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'entidade_user');
    }

    public function cidadePrincipalRegiao()
    {
        return $this->belongsTo(Cidade::class, 'regiao_cidade_principal_id');
    }
}
