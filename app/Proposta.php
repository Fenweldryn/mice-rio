<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposta extends Model
{
    public function cidade()
    {
        return $this->belongsTo('App\Cidade');
    }

    public function status()
    {
        return $this->hasMany('App\Status');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i:s');
    }
    public function getInicioAttribute($value)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }
    public function getFimAttribute($value)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }
    public function getNecessitaHospedagemAttribute($value)
    {
        return $value ? "Sim" : "Não";
    }
    public function getPasseiosAttribute($value)
    {
        return $value ? "Sim" : "Não";
    }
    public function getAtividadesSociaisAttribute($value)
    {
        return $value ? "Sim" : "Não";
    }
    
}
