<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContatoPrincipal extends Mailable
{
    use Queueable, SerializesModels;

    public $destinatario = 'contato@mice-rio.com.br';
    public $copia = 'contato@krotec.com.br';    
    public $mensagem;
    public $assunto;
    public $remetente;
    public $nome;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($remetente, $nome, $assunto, $mensagem)
    {
        $this->mensagem = $mensagem;
        $this->assunto = $assunto;
        $this->remetente = $remetente;
        $this->nome = $nome;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        return $this->to($this->destinatario, 'contato mice rio')
            ->cc($this->copia) 
            ->replyTo($this->remetente,$this->nome)            
            ->subject($this->assunto)            
            ->view('emails.contato');
    }
}
