<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistrado extends Mailable
{
    use Queueable, SerializesModels;

    public $destinatario = 'contato@mice-rio.com.br';
    public $copia = 'contato@krotec.com.br';    
    public $remetente = "contato@mice-rio.com.br";
    public $assunto = "Novo usuário registrado.";
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->destinatario, 'MICE Rio')
            ->cc($this->copia)
            ->replyTo($this->remetente)
            ->subject($this->assunto)
            ->view('emails.usuario_registrado');
    }
}
