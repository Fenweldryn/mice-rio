<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permuta;

class HistoricoPermuta extends Model
{
    public static function boot()
    {
        parent::boot();

        self::created(function($historico){
            $historico->removerQuantidadeDisponivel();
        });

        self::updated(function($historico){
            $historico->removerQuantidadeDisponivel();
        });

        self::deleting(function($historico){
            $historico->adicionarQuantidadeDisponivel();
        });
    }

    private function removerQuantidadeDisponivel()
    {
        $permuta = Permuta::find($this->permuta_id);
        $permuta->quantidade_disponivel = $permuta->quantidade_disponivel - $this->quantidade;
        $permuta->update();
    }

    private function adicionarQuantidadeDisponivel()
    {
        $permuta = Permuta::find($this->permuta_id);
        $permuta->quantidade_disponivel = $permuta->quantidade_disponivel + $this->quantidade;
        $permuta->update();
    }

    public function permuta()
    {
        return $this->belongsTo(Permuta::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }



}
