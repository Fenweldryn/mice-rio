<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalEvento extends Model
{
    public function cidade()
    {
        return $this->belongsTo("App\Cidade");
    }
    public function LocalEventoTipo()
    {
        return $this->belongsTo("App\LocalEventoTipo");
    }
}
