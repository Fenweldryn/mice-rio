<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Pagamento extends Model
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('userid', function(Builder $builder) {
            if (!auth()->user()->hasRole('admin'))
            {
                $builder->whereHas('associado.entidade.users', function($q) {
                    $q->where('user_id', auth()->user()->id);
                });
            }
        });
    }

    public function associado()
    {
        return $this->belongsTo("\App\Associado");
    }

    public static function vencidos()
    {
        return Pagamento::whereNull('data_pagamento');
    }

    public static function vencendo()
    {
        return Pagamento::whereNull('data_pagamento')
            ->whereRaw('dia_vencimento <= ' . date('d'))
            ->where('data_referencia', date('Y-m-d'));
    }

    public static function totalAteHoje()
    {
        $totalPagamento = DB::table('pagamentos')
                            ->select(DB::raw("sum(valor) as total"))
                            ->whereNotNull('data_pagamento')
                            ->get()
                            ->first()->total;
        return number_format($totalPagamento, 2, ',' ,'.');
    }

    public static function recebidos($entidadeID = 0)
    {
        $pagamentos = Pagamento::select(
            DB::raw("sum(valor) as valor, 
                month(data_referencia) as mes, 
                year(data_referencia) as ano")
            )
        ->whereNotNull('data_pagamento');
            
        if ($entidadeID > 0) {            
            $pagamentos->whereHas('associado', function($q) use ($entidadeID) {
               $q->where('entidade_id', $entidadeID);
            });
        }

        return $pagamentos->groupBy('data_referencia')->get();
    }

    public static function pendentes($entidadeID = 0)
    {
        $pagamentos = Pagamento::select(
            DB::raw("sum(valor) as valor, 
                month(data_referencia) as mes, 
                year(data_referencia) as ano")
            )
        ->whereNull('data_pagamento');
            
        if ($entidadeID > 0) {            
            $pagamentos->whereHas('associado', function($q) use ($entidadeID) {
                $q->where('entidade_id', $entidadeID);
            });
        }

        return $pagamentos->groupBy('data_referencia')->get();
    }

    public static function pagamentosDoAno($ano)
    {
        return Pagamento::select(DB::raw("sum(valor) as valor, month(data_referencia) as mes"))->whereNotNull('data_pagamento')->groupBy('data_referencia')->get();
    }

    public function status()
    {
        return $this->data_pagamento ? 'Pago' : 'Pendente';
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i:s');
    }
}
