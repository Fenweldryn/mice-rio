<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modal extends Model
{
    protected $table = "modais";

    public function cidade()
    {
        return $this->belongsTo("App\Cidade");
    }
    public function modalTipo()
    {
        return $this->belongsTo("App\ModalTipo");
    }
}
