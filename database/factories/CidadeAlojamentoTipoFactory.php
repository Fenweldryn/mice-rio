<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    $cidadesCount = \App\Cidade::count();
    $alojamentoTipoCount = \App\Cidade::count();

    return [
        'cidade_id' => $faker->numberBetween(1, $cidadesCount),
        'alojamento_tipo_id' => $faker->numberBetween(1, $alojamentoTipoCount),       
    ];
});
