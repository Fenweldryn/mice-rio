<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pagamento;
use Faker\Generator as Faker;

$factory->define(Pagamento::class, function (Faker $faker) {
    $associado = \App\Associado::all()->random();
    $data_referencia = \Carbon\Carbon::parse(date('Y-'.$faker->numberBetween(1,12).'-01'))->format('Y-m-d');
    $data_existente = \App\Pagamento::where('associado_id',$associado->id)->where('data_referencia')->get();

    if ($data_existente->count() > 0) {
        while ($data_referencia == $data_existente->data_referencia) {
            $data_referencia = \Carbon\Carbon::parse(date('Y-'.$faker->numberBetween(1,12).'-01'))->format('Y-m-d');
        }        
    }
    return [
        'associado_id' => $associado->id,
        'valor' => $associado->mensalidade,
        'data_referencia' => $data_referencia,
        'dia_vencimento' => $associado->dia_vencimento
    ];
});
