<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Cidade;
use Faker\Generator as Faker;

$factory->define(Cidade::class, function (Faker $faker) {
    return [
        'nome' => $faker->city,
        'descricao' => $faker->paragraph(),
        'estado' => $faker->state,
        'estado_sigla' => $faker->stateAbbr,
        'distancia_capital' => $faker->numberBetween(0, 500),
        'tem_rodovia_federal' => $faker->boolean,
        'tem_rodovia_estadual' => $faker->boolean,
        'tem_centro_atendimento' => $faker->boolean,
        'capacidade_maior_espaco' => $faker->numberBetween(0, 1500),
        'capacidade_maior_auditorio' => $faker->numberBetween(0, 1500),
        'capacidade_maior_banquete' => $faker->numberBetween(0, 1500),
        'capacidade_total_quartos' => $faker->numberBetween(0, 1500),
        'capacidade_total_leitos' => $faker->numberBetween(0, 1500),
        'capacidade_quartos_maior_hotel' => $faker->numberBetween(0, 1500),
        'numero_instituicoes_ensino_superior' => $faker->numberBetween(0, 10),
        'areas_expertise_referencia' => $faker->paragraph(),
        'principais_eventos' => $faker->paragraph(),
        'approval_status' => 1
    ];
});
