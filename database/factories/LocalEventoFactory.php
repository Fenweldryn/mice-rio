<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\LocalEvento;
use Faker\Generator as Faker;

$factory->define(LocalEvento::class, function (Faker $faker) {
    return [
        'nome' => $faker->lastName,
        'descricao' => $faker->paragraph,
        'capacidade_auditorio' => $faker->numberBetween(10, 1000),
        'local_evento_tipo_id' => $faker->numberBetween(1, App\LocalEventoTipo::count()),
        'cidade_id' => $faker->numberBetween(1, App\Cidade::count()),
    ];
});
