<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Associado;
use Faker\Generator as Faker;

$factory->define(Associado::class, function (Faker $faker) {
    return [
        "nome" => $faker->company,
        "mensalidade" => $faker->randomFloat(2,100,1000),
        "entidade_id" => \App\Entidade::all()->random()->id,
        "dia_vencimento" => $faker->numberBetween(1,15),
        "ativo" => 1
    ];
});
