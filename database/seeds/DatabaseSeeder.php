<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ModalTiposSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(AdminRoleSeeder::class);
        $this->call(GestorSeeder::class);
        $this->call(MainAdminSeeder::class);
        $this->call(AlojamentoTipoSeeder::class);
        $this->call(AlojamentoCategoriaSeeder::class);        
        $this->call(CentroEsportivoTipoSeeder::class);
        $this->call(LocalEventoTipoSeeder::class);
        $this->call(CidadeFactorySeeder::class);
    }
}
