<?php

use Illuminate\Database\Seeder;

class ModalTiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modal_tipos')->insert([
            ['nome' => 'Aéreo', 'tipo' => 'acesso', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Ferroviário', 'tipo' => 'acesso', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Hidroviário', 'tipo' => 'acesso', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Lacustre', 'tipo' => 'acesso', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Maritmo', 'tipo' => 'acesso', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Rodoviário', 'tipo' => 'acesso', 'created_at' => date('Y-m-d h:i:s')],

            ['nome' => 'Ônibus Regulares', 'tipo' => 'local', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Ônibus Executivos', 'tipo' => 'local', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Shuttle Aeroporto e rodoviária', 'tipo' => 'local', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Metrô', 'tipo' => 'local', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Trem de Superfície', 'tipo' => 'local', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Vans/Microônibus', 'tipo' => 'local', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Táxis', 'tipo' => 'local', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Transporte de Aplicativos (Uber, Cabify, etc)', 'tipo' => 'local', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Mototáxi', 'tipo' => 'local', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Balsas', 'tipo' => 'local', 'created_at' => date('Y-m-d h:i:s')]            
        ]);
    }
}
