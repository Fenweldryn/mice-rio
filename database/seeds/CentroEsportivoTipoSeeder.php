<?php

use Illuminate\Database\Seeder;

class CentroEsportivoTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('centro_esportivo_tipos')->insert([
            ['nome' => 'Arena Multiesportiva Coberta', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Arena Multiesportiva Cberta', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Estádios de Futebol', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Ginásio Esportivo Coberto', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Parque Aquático', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Autódromos', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'kartódromos', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Velódromos', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Hipódromos', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Campos de Golfe', 'created_at' => date('Y-m-d h:i:s')],        
        ]);
    }
}
