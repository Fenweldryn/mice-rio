<?php

use Illuminate\Database\Seeder;

class LocalEventoTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('local_evento_tipos')->insert([
            ['nome' => 'Pavilhão de exposições independente e coberto', 'created_at' => date('Y-m-d h:i:s')],   
            ['nome' => 'Centro de exposições agropecuárias', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Centro de convenções e exposições independente', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Hotel com centro de eventos', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => 'Hotel com salas para eventos', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Salões e auditórios independentes ou instalados em instituições', 'created_at' => date('Y-m-d h:i:s')],
        ]);
    }
}
