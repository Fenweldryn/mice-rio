<?php

use Illuminate\Database\Seeder;

class MainAdminSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'Bruno Martins', 'email' => 'bruno.mdsc@gmail.com', 'password'=> Hash::make('teste1 ! 3teste'),'created_at' => date('Y-m-d h:i:s')],
        ]);        
        $id = DB::getPdo()->lastInsertId();;
        DB::table('model_has_roles')->insert([
            ['model_type' => 'App\User', 'model_id' => $id, 'role_id' => $id]
        ]);

        DB::table('users')->insert([
            ['name' => 'Waltemar Junior', 'email' => 'waltemarjunior@hotmail.com', 'password'=> Hash::make('batata! 88mm'),'created_at' => date('Y-m-d h:i:s')],
        ]);        
        $id = DB::getPdo()->lastInsertId();;
        DB::table('model_has_roles')->insert([
            ['model_type' => 'App\User', 'model_id' => $id, 'role_id' => $id]
        ]);
    }
}
