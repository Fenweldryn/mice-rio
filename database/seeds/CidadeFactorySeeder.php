<?php

use Illuminate\Database\Seeder;

class CidadeFactorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Cidade::class, 20)->create();

        $faker = \Faker\Factory::create();
        $cidadeCount = \App\Cidade::count() - 1;
        $alojamentoTipoCount = \App\AlojamentoTipo::count() - 1;
        $alojamentoCategoriaCount = \App\AlojamentoCategoria::count() - 1;
        $modalTipoCount = \App\ModalTipo::count() - 1;
        $localEventoTipoCount = \App\LocalEventoTipo::count() - 1;

        for ($i=1; $i < $cidadeCount; $i++) { 
            DB::table('cidade_alojamento_tipo')->insert([
                ['cidade_id' => $i, 'alojamento_tipo_id' => $faker->unique(true)->numberBetween(1, $alojamentoTipoCount)]
            ]);            
            DB::table('cidade_alojamento_categoria')->insert([
                ['cidade_id' => $i, 'alojamento_categoria_id' => $faker->unique(true)->numberBetween(1, $alojamentoCategoriaCount)],  
            ]);            
            DB::table('cidade_modal_tipo')->insert([
                ['cidade_id' => $i, 'modal_tipo_id' => $faker->unique(true)->numberBetween(1, $modalTipoCount)],  
            ]);            
            DB::table('cidade_local_evento_tipo')->insert([
                ['cidade_id' => $i, 'local_evento_tipo_id' => $faker->unique(true)->numberBetween(1, $localEventoTipoCount)],  
            ]);            
            
        }
    }
}
