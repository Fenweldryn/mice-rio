<?php

use Illuminate\Database\Seeder;

class AlojamentoCategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alojamento_categorias')->insert([
            ['nome' => '5 estrelas', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => '4 estrelas', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => '3 estrelas', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => '2 estrelas', 'created_at' => date('Y-m-d h:i:s')],        
            ['nome' => '1 estrela / pousadas', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'sem classificação', 'created_at' => date('Y-m-d h:i:s')]
        ]);
    }
}
