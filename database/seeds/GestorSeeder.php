<?php

use Illuminate\Database\Seeder;

class GestorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['name' => 'gestor', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],
        ]);
        
        $id = DB::getPdo()->lastInsertId();;

        DB::table('role_has_permissions')->insert([
            ['permission_id' => 1, 'role_id' => $id],  
            ['permission_id' => 10, 'role_id' => $id],  
            ['permission_id' => 11, 'role_id' => $id],  
            ['permission_id' => 12, 'role_id' => $id],  
            ['permission_id' => 13, 'role_id' => $id],  
            ['permission_id' => 14, 'role_id' => $id],  
            ['permission_id' => 15, 'role_id' => $id],  
            ['permission_id' => 16, 'role_id' => $id],  
            ['permission_id' => 17, 'role_id' => $id],  
            ['permission_id' => 18, 'role_id' => $id],  
            ['permission_id' => 19, 'role_id' => $id],  
            ['permission_id' => 20, 'role_id' => $id],  
        ]);
    }
}
