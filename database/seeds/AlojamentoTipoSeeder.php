<?php

use Illuminate\Database\Seeder;

class AlojamentoTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alojamento_tipos')->insert([
            // ['nome' => 'Hotéis individuais', 'created_at' => date('Y-m-d h:i:s')],        
            // ['nome' => 'Rede hoteleira local', 'created_at' => date('Y-m-d h:i:s')],        
            // ['nome' => 'Rede hoteleira estadual', 'created_at' => date('Y-m-d h:i:s')],
            // ['nome' => 'Rede hoteleira nacional', 'created_at' => date('Y-m-d h:i:s')],        
            // ['nome' => 'Rede hoteleira internacional', 'created_at' => date('Y-m-d h:i:s')]
            ['nome' => 'Hotel', 'created_at' => date('Y-m-d h:i:s')],
            ['nome' => 'Pousada', 'created_at' => date('Y-m-d h:i:s')],
        ]);
    }
}
