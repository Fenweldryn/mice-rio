<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['name' => 'ver painel adm', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        

            ['name' => 'ver aprovação', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            ['name' => 'gerir aprovação', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        

            ['name' => 'ver usuários', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            ['name' => 'gerir usuários', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            
            ['name' => 'ver perfis', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            ['name' => 'gerir perfis', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            
            ['name' => 'ver permissões', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            ['name' => 'gerir permissões', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],   
            
            ['name' => 'ver cidades', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            ['name' => 'gerir cidades', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],       
            
            ['name' => 'ver entidades', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            ['name' => 'gerir entidades', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        

            ['name' => 'ver modais', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            ['name' => 'gerir modais', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            
            ['name' => 'ver hospedagens', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            ['name' => 'gerir hospedagens', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            
            ['name' => 'ver locais evento', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            ['name' => 'gerir locais evento', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        
            
            ['name' => 'ver propostas', 'guard_name' => 'web', 'created_at' => date('Y-m-d h:i:s')],        

        ]);
    }
}
