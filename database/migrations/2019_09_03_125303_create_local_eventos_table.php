<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->text('descricao');
            $table->unsignedInteger('capacidade_auditorio');                        
            $table->unsignedInteger('local_evento_tipo_id');            
            $table->unsignedInteger('cidade_id');            
            $table->timestamps();

            $table->foreign("local_evento_tipo_id")->references('id')->on('local_evento_tipos');
            $table->foreign("cidade_id")->references('id')->on('cidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_eventos');
    }
}
