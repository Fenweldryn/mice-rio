<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlojamentoCategoriaCidadePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidade_alojamento_categoria', function (Blueprint $table) {
            $table->integer('alojamento_categoria_id')->unsigned()->index();
            $table->foreign('alojamento_categoria_id')->references('id')->on('alojamento_categorias')->onDelete('cascade');
            $table->integer('cidade_id')->unsigned()->index();
            $table->foreign('cidade_id')->references('id')->on('cidades')->onDelete('cascade');
            $table->primary(['alojamento_categoria_id', 'cidade_id'],'alojcat_cidade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cidade_alojamento_categoria');
    }
}
