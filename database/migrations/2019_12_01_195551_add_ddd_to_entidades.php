<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDddToEntidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entidades', function (Blueprint $table) {
            $table->unsignedInteger('ddd_telefone');
            $table->unsignedInteger('ddd_whatsapp');            
            $table->unsignedInteger('ddd_telefone_fixo_presidente');
            $table->unsignedInteger('ddd_celular_presidente');
            $table->unsignedInteger('ddd_telefone_fixo_financeiro');
            $table->unsignedInteger('ddd_celular_financeiro');
            $table->unsignedInteger('ddd_telefone_fixo_gerente');
            $table->unsignedInteger('ddd_celular_gerente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entidades', function (Blueprint $table) {
            $table->dropColumn('ddd_telefone');
            $table->dropColumn('ddd_whatsapp');
            $table->dropColumn('ddd_telefone_fixo_presidente');
            $table->dropColumn('ddd_celular_presidente');
            $table->dropColumn('ddd_telefone_fixo_financeiro');
            $table->dropColumn('ddd_celular_financeiro');
            $table->dropColumn('ddd_telefone_fixo_gerente');
            $table->dropColumn('ddd_celular_gerente');
        });
    }
}
