<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('associado_id');
            $table->decimal('valor', 8, 2);            
            $table->string('tipo', 20)->default('dinheiro');            
            $table->string('descricao_permuta', 250)->nullable();            
            $table->date('data_referencia');            
            $table->tinyInteger('dia_vencimento');            
            $table->date('data_pagamento')->nullable();            
            $table->timestamps();

            $table->foreign('associado_id')->references('id')->on('associados')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagamentos');
    }
}
