<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCidadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('slug')->unique();
            $table->text("descricao");            
            $table->string('estado');
            $table->string('estado_sigla');
            $table->integer('distancia_capital');
            $table->boolean('tem_rodovia_federal');
            $table->boolean('tem_rodovia_estadual');            
            $table->boolean('tem_centro_atendimento');            
            $table->unsignedInteger("capacidade_maior_espaco");
            $table->unsignedInteger("capacidade_maior_auditorio");
            $table->unsignedInteger("capacidade_maior_banquete");
            $table->unsignedInteger("capacidade_total_quartos");
            $table->unsignedInteger("capacidade_total_leitos");
            $table->unsignedInteger("capacidade_quartos_maior_hotel");
            $table->unsignedInteger("numero_instituicoes_ensino_superior");
            $table->text("areas_expertise_referencia");
            $table->text("principais_eventos");            
            $table->tinyInteger("approval_status");            
            $table->timestamp("approval_at")->nullable();            
            $table->string("motivo_rejeicao")->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cidades');
    }
}
