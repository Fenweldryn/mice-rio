<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropostasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propostas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('cidade_id');
            $table->string('nome_evento');
            $table->string('tipo_evento');
            $table->date('inicio');
            $table->date('fim');
            $table->unsignedInteger('pessoas');
            $table->string('formato');
            $table->unsignedInteger('salas_paralelas');
            $table->unsignedInteger('area_exposicao_m2');
            $table->boolean('necessita_hospedagem');
            $table->string('tipo_hotel');
            $table->unsignedInteger('quartos');
            $table->boolean('passeios');
            $table->boolean('atividades_sociais');
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('cidade_id')->references('id')->on('cidades')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propostas');
    }
}
