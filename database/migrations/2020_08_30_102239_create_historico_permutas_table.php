<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoPermutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_permutas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('permuta_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->integer('quantidade');
            $table->date('usado_em');
            $table->timestamps();

            $table->foreign('permuta_id')->references('id')->on('permutas')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_permutas');
    }
}
