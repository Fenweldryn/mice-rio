<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permutas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao');
            $table->unsignedInteger('entidade_id');
            $table->unsignedInteger('associado_id');
            $table->boolean('is_active')->default(1);
            $table->integer('quantidade_por_mes');
            $table->integer('quantidade_disponivel');
            $table->timestamps();

            $table->foreign('entidade_id')->references('id')->on('entidades')->onDelete('cascade');
            $table->foreign('associado_id')->references('id')->on('associados')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permutas');
    }
}
