<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCidadeAlojamentoTipoPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidade_alojamento_tipo', function (Blueprint $table) {
            $table->integer('cidade_id')->unsigned()->index();
            $table->foreign('cidade_id')->references('id')->on('cidades')->onDelete('cascade');
            $table->integer('alojamento_tipo_id')->unsigned()->index();
            $table->foreign('alojamento_tipo_id')->references('id')->on('alojamento_tipos')->onDelete('cascade');
            $table->primary(['cidade_id', 'alojamento_tipo_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cidade_alojamento_tipo');
    }
}
