<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCidadeModalTipoPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidade_modal_tipo', function (Blueprint $table) {
            $table->integer('cidade_id')->unsigned()->index();
            $table->foreign('cidade_id')->references('id')->on('cidades')->onDelete('cascade');
            $table->integer('modal_tipo_id')->unsigned()->index();
            $table->foreign('modal_tipo_id')->references('id')->on('modal_tipos')->onDelete('cascade');
            $table->primary(['cidade_id', 'modal_tipo_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cidade_modal_tipo');
    }
}
