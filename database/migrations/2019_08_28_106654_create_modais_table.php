<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modais', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');            
            $table->unsignedInteger('modal_tipo_id');            
            $table->unsignedInteger('cidade_id');            
            $table->timestamps();

            $table->foreign("modal_tipo_id")->references('id')->on('modal_tipos')->onDelete('cascade');
            $table->foreign("cidade_id")->references('id')->on('cidades')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modais');
    }
}
