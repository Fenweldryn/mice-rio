<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDadosToEntidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entidades', function (Blueprint $table) {
            $table->string('nome_presidente');
            $table->string('email_presidente');
            $table->string('telefone_fixo_presidente');
            $table->string('celular_presidente');

            $table->string('nome_financeiro');
            $table->string('email_financeiro');
            $table->string('telefone_fixo_financeiro');
            $table->string('celular_financeiro');

            $table->string('nome_gerente');
            $table->string('email_gerente');
            $table->string('telefone_fixo_gerente');
            $table->string('celular_gerente');

            $table->string('instagram');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entidades', function (Blueprint $table) {
            $table->dropColumn('nome_presidente');
            $table->dropColumn('email_presidente');
            $table->dropColumn('telefone_fixo_presidente');
            $table->dropColumn('celular_presidente');

            $table->dropColumn('nome_financeiro');
            $table->dropColumn('email_financeiro');
            $table->dropColumn('telefone_fixo_financeiro');
            $table->dropColumn('celular_financeiro');

            $table->dropColumn('nome_gerente');
            $table->dropColumn('email_gerente');
            $table->dropColumn('telefone_fixo_gerente');
            $table->dropColumn('celular_gerente');

            $table->dropColumn('instagram');
        });
    }
}
