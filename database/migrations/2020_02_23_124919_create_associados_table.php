<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('cnpj', 30); 
            $table->decimal('mensalidade', 8, 2);
            $table->unsignedInteger('dia_vencimento');
            $table->unsignedInteger('entidade_id');
            $table->string('tipo_type');
            $table->string('tipo_id');            
            $table->boolean('ativo')->default(1);
            $table->string('email');
            $table->string('endereco');
            $table->unsignedInteger('telefone')->nullable();
            $table->string('geolocalizacao')->nullable();
            $table->string('site');
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('pinterest')->nullable();            
            $table->unsignedInteger('distancia_centro_km')->nullable();
            $table->unsignedInteger('area_construida_total_m2')->nullable();
            $table->unsignedInteger('area_evento_total_m2')->nullable();
            $table->unsignedInteger('capacidade_lugares_auditorio')->nullable();
            $table->unsignedInteger('capacidade_lugares_coquetel')->nullable();
            $table->timestamps();

            $table->foreign('entidade_id')->references('id')->on('entidades')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associados');
    }
}
