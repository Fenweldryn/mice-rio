<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlojamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alojamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('descricao');
            $table->unsignedInteger('capacidade');
            $table->unsignedInteger('alojamento_categoria_id');
            $table->unsignedInteger('alojamento_tipo_id');
            $table->unsignedInteger('cidade_id');
            $table->timestamps();

            $table->foreign('alojamento_categoria_id')->references('id')->on('alojamento_categorias');
            $table->foreign('alojamento_tipo_id')->references('id')->on('alojamento_tipos');
            $table->foreign('cidade_id')->references('id')->on('cidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alojamentos');
    }
}
