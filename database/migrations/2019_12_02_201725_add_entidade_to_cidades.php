<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEntidadeToCidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('cidade_entidade');
        Schema::table('cidades', function (Blueprint $table) {
            $table->unsignedInteger('entidade_id')->nullable();

            $table->foreign('entidade_id')->references('id')->on('entidades')->onDelete('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('cidades', function (Blueprint $table) {            
            $table->dropForeign('entidade_id')->references('id')->on('entidades');
            $table->dropColumn('entidade_id');
        });

        Schema::create('cidade_entidade', function (Blueprint $table) {
            $table->integer('cidade_id')->unsigned()->index();
            $table->foreign('cidade_id')->references('id')->on('cidades')->onDelete('cascade');
            $table->integer('entidade_id')->unsigned()->index();
            $table->foreign('entidade_id')->references('id')->on('entidades')->onDelete('cascade');
            $table->primary(['cidade_id', 'entidade_id']);
        });
        Schema::enableForeignKeyConstraints();
    }
}
