<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCidadeModalAcessoPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidade_modal_acesso', function (Blueprint $table) {
            $table->integer('cidade_id')->unsigned()->index();
            $table->foreign('cidade_id')->references('id')->on('cidades')->onDelete('cascade');
            $table->integer('modal_acesso_id')->unsigned()->index();
            $table->foreign('modal_acesso_id')->references('id')->on('modal_acessos')->onDelete('cascade');
            $table->primary(['cidade_id', 'modal_acesso_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cidade_modal_acesso');
    }
}
