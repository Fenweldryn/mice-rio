<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCidadeLocalEventoTipoPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidade_local_evento_tipo', function (Blueprint $table) {
            $table->integer('cidade_id')->unsigned()->index();
            $table->foreign('cidade_id')->references('id')->on('cidades')->onDelete('cascade');
            $table->integer('local_evento_tipo_id')->unsigned()->index();
            $table->foreign('local_evento_tipo_id')->references('id')->on('local_evento_tipos')->onDelete('cascade');
            $table->primary(['cidade_id', 'local_evento_tipo_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cidade_local_evento_tipo');
    }
}
