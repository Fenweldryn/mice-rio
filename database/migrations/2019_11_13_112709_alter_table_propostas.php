<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePropostas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('propostas', function (Blueprint $table) {
            $table->string('nome_empresa');
            $table->string('cnpj');
            $table->string('nome_contato');
            $table->string('telefone_contato');
            $table->dropColumn(['tipo_hotel']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('propostas', function (Blueprint $table) {
            $table->dropColumn(['nome_empresa','cnpj','nome_contato','telefone_contato']);
            $table->string('tipo_hotel');
        });
    }
}
