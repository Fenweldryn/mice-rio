<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentroEsportivoTipoCidadePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centro_esportivo_tipo_cidade', function (Blueprint $table) {
            $table->integer('centro_esportivo_tipo_id')->unsigned()->index();
            $table->foreign('centro_esportivo_tipo_id','cet_cidade_foreign')->references('id')->on('centro_esportivo_tipos')->onDelete('cascade');
            $table->integer('cidade_id')->unsigned()->index();
            $table->foreign('cidade_id','cidade_cet_foireign')->references('id')->on('cidades')->onDelete('cascade');
            $table->primary(['centro_esportivo_tipo_id', 'cidade_id'],'cet_cidade_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('centro_esportivo_tipo_cidade');
    }
}
