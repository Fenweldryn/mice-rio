<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');
Route::get('/buscador', 'BuscadorController@index')->name('buscador');
Route::get('/resultado', 'BuscadorController@buscar')->name('resultado');
Route::get('/busca', 'BuscadorController@buscar')->name('busca');
Route::get('/cidades/{slug}', 'CidadeController@show');
Route::get('/cidades/{slug}/solicitarproposta', 'PropostaController@solicitarProposta');
Route::post('/propostas', 'PropostaController@store')->name('propostas.store');
Route::get('/proposta_enviada', function(){return view('proposta_enviada');});
Route::post('/enviaremail', "EmailController@enviar");

Auth::routes();

Route::get('/ver', function(){
    return view('proposta_enviada');
});

Route::group(['middleware' => ['auth','permission:ver painel adm'], 'prefix' => 'admin'], function () {    
    
    Route::get('/', 'AdminController@index')->name('admin');
    Route::get('/users/list', 'UserController@list');    
    Route::get('/permissions/list', 'PermissionController@list');
    Route::get('/roles/list', 'RoleController@list');    
    Route::get('/cidades/list', 'CidadeController@list');  
    Route::get('/alojamentotipos/list', 'AlojamentoTipoController@list');  
    Route::get('/modaltipos/list', 'ModalTipoController@list');  
    Route::get('/localeventotipos/list', 'LocalEventoTipoController@list');  
    Route::get('/alojamentocategorias/list', 'AlojamentoCategoriaController@list');  
    
    Route::post('/cidades/{cidade}/destroyMedia', 'CidadeController@destroyMedia');      
    Route::post('/cidades/{cidade}/definirFotoPrincipal', 'CidadeController@definirFotoPrincipal');      
    Route::post('/cidades/{cidade}/inativar', 'CidadeController@inativar');      
    Route::post('/cidades/{cidade}/ativar', 'CidadeController@ativar');      
    Route::get('/users/create', 'UserController@create')->name('users.create');
    Route::get('/financeiro', 'FinanceiroController@index')->name('financeiro.index');
    Route::post('/financeiro/recebido', 'FinanceiroController@graficoPagamentosRecebidos');
    Route::post('/financeiro/pendente', 'FinanceiroController@graficoPagamentosPendentes');
    Route::post('/pagamentos/batch', 'PagamentoController@storeBatch');
    Route::get('/pagamentos/vencidos', 'PagamentoController@vencidos');
    Route::get('/permuta/{permuta}/baixa', 'HistoricoPermutaController@create');
    Route::get('/associados/subtipo/{tipo}', 'AssociadoController@getSubtipo');

    Route::get('/propostas/{proposta}/colocarEmAnalise', 'PropostaController@colocarEmAnalise');
    Route::get('/propostas/{proposta}/concluir', 'PropostaController@concluir');

    #region datatable suffix
    Route::get('/users/datatable', 'UserController@datatablesAjax');
    Route::get('/cidades/datatable', 'CidadeController@datatablesAjax');
    Route::get('/entidades/datatable', 'EntidadeController@datatablesAjax');    
    Route::get('/roles/datatable', 'RoleController@datatablesAjax');    
    Route::get('/permissions/datatable', 'PermissionController@datatablesAjax');    
    Route::get('/modais/datatable', 'ModalController@datatablesAjax');    
    Route::get('/modaltipos/datatable', 'ModalTipoController@datatablesAjax');    
    Route::get('/localeventotipos/datatable', 'LocalEventoTipoController@datatablesAjax');    
    Route::get('/localeventos/datatable', 'LocalEventoController@datatablesAjax');    
    Route::get('/alojamentos/datatable', 'AlojamentoController@datatablesAjax');    
    Route::get('/alojamentotipos/datatable', 'AlojamentoTipoController@datatablesAjax');    
    Route::get('/alojamentocategorias/datatable', 'AlojamentoCategoriaController@datatablesAjax');    
    Route::get('/centroesportivotipos/datatable', 'CentroEsportivoTipoController@datatablesAjax');        
    Route::get('/propostas/datatable', 'PropostaController@datatablesAjax');        
    Route::get('/aprovacoes/datatable', 'AprovacaoController@datatablesAjax');        
    Route::get('/associados/datatable', 'AssociadoController@datatablesAjax');                
    Route::get('/pagamentos/datatable', 'PagamentoController@datatablesAjax');                
    Route::get('/restaurantetipos/datatable', 'RestauranteTipoController@datatablesAjax');                
    Route::get('/comercioindustriacpftipos/datatable', 'ComercioIndustriaCpfTipoController@datatablesAjax');                
    #endregion

    Route::resources([
        'users' => 'UserController',
        'cidades' => 'CidadeController',        
        'roles' => 'RoleController',                
        'permissions' => 'PermissionController',                
        'entidades' => 'EntidadeController',                
        'modaltipos' => 'ModalTipoController',                
        'localeventotipos' => 'LocalEventoTipoController',                
        'localeventos' => 'LocalEventoController',                
        'alojamentos' => 'AlojamentoController',                
        'alojamentotipos' => 'AlojamentoTipoController',                
        'alojamentocategorias' => 'AlojamentoCategoriaController',                
        'centroesportivotipos' => 'CentroEsportivoTipoController',                
        'propostas' => 'PropostaController',                
        'associados' => 'AssociadoController',                
        'pagamentos' => 'PagamentoController',                
        'permutas' => 'PermutaController',                
        'historicopermutas' => 'HistoricoPermutaController',                
        'restaurantetipos' => 'RestauranteTipoController',                
        'comercioindustriacpftipos' => 'ComercioIndustriaCpfTipoController',                
    ]);
    
    #region analytics
    Route::post('/analytics/visitorsAndPageViews', 'AnalyticsController@visitorsAndPageViews');
    Route::post('/analytics/mostVisitedPages', 'AnalyticsController@mostVisitedPages');
    Route::post('/analytics/visitorsCountry', 'AnalyticsController@visitorsCountry');
    #endregion
        
    #region entidades-usuarios
    Route::get('/entidades/{entidade}/usuarios', 'EntidadeController@usuarios');
    Route::post('/entidades/{entidade}/usuarios', 'EntidadeController@addUsers');
    Route::delete('/entidades/{entidade}/usuarios', 'EntidadeController@destroyUser');
    #endregion
    #region entidades-cidades
    Route::get('/entidades/{entidade}/cidades', 'EntidadeController@cidades');
    Route::post('/entidades/{entidade}/cidades', 'EntidadeController@addCidades');
    Route::delete('/entidades/{entidade}/cidades', 'EntidadeController@destroyCidade');
    #endregion
    #region roles-permissions
    Route::get('/roles/{role}/permissions', 'RoleController@permissions');
    Route::post('/roles/{role}/permissions', 'RoleController@addPermissions');
    Route::delete('/roles/{role}/permissions', 'RoleController@destroyPermission');
    #endregion
    #region users-roles
    Route::get('/users/{user}/roles', 'UserController@roles');
    Route::post('/users/{user}/roles', 'UserController@addRoles');
    Route::delete('/users/{user}/roles', 'UserController@destroyRole');
    #endregion
    #region cidades-alojamentoTipos
    Route::get('/cidades/{cidade}/alojamentoTipos', 'CidadeController@alojamentoTipos');
    Route::post('/cidades/{cidade}/addAlojamentoTipos', 'CidadeController@addAlojamentoTipos');
    Route::delete('/cidades/{cidade}/alojamentoTipos', 'CidadeController@destroyAlojamentoTipos');
    #endregion
    #region cidades-alojamentoTipos
    Route::get('/cidades/{cidade}/alojamentoCategorias', 'CidadeController@alojamentoCategorias');
    Route::post('/cidades/{cidade}/addAlojamentoCategorias', 'CidadeController@addAlojamentoCategorias');
    Route::delete('/cidades/{cidade}/alojamentoCategorias', 'CidadeController@destroyAlojamentoCategorias');
    #endregion
    #region cidades-modalTipos
    Route::get('/cidades/{cidade}/modalTipos', 'CidadeController@modalTipos');
    Route::post('/cidades/{cidade}/addModalTipos', 'CidadeController@addModalTipos');
    Route::delete('/cidades/{cidade}/modalTipos', 'CidadeController@destroyModalTipos');
    #endregion
    #region cidades-localEventoTipos
    Route::get('/cidades/{cidade}/localEventoTipos', 'CidadeController@localEventoTipos');
    Route::post('/cidades/{cidade}/addLocalEventoTipos', 'CidadeController@addLocalEventoTipos');
    Route::delete('/cidades/{cidade}/localEventoTipos', 'CidadeController@destroyLocalEventoTipos');
    #endregion
        
    #region MODAIS RESOURCE
    Route::post('/modais', 'ModalController@store')->name('modais.store');
    Route::get('/modais', 'ModalController@index')->name('modais.index');
    Route::get('/modais', 'ModalController@index')->name('modais.index');
    Route::get('/modais/create', 'ModalController@create')->name('modais.create');
    Route::put('/modais/{modal}', 'ModalController@update')->name('modais.update');    
    Route::patch('/modais/{modal}', 'ModalController@update')->name('modais.update');    
    Route::get('/modais/{modal}', 'ModalController@show')->name('modais.show');    
    Route::delete('/modais/{modal}', 'ModalController@destroy')->name('modais.destroy');    
    Route::get('/modais/{modal}/edit', 'ModalController@edit')->name('modais.edit');        
    #endregion
    #region APROVACOES RESOURCE
    Route::get('/aprovacoes', 'AprovacaoController@index')->name('aprovacoes.index');
    Route::get('/aprovacoes/{logID}', 'AprovacaoController@show')->name('aprovacoes.show');    
    Route::delete('/aprovacoes/{logID}', 'AprovacaoController@destroy')->name('aprovacoes.destroy');  
    Route::put('/aprovacoes/{logID}', 'AprovacaoController@update')->name('aprovacoes.update');    
    Route::patch('/aprovacoes/{logID}', 'AprovacaoController@update')->name('aprovacoes.update');      
    // Route::put('/aprovacoes/{logID}', 'AprovacaoController@aprovar')->name('aprovacoes.aprovar');        
    // Route::patch('/aprovacoes/{logID}', 'AprovacaoController@rejeitar')->name('aprovacoes.rejeitar');        
    Route::get('/aprovacoes/{logID}/edit', 'AprovacaoController@edit')->name('modais.edit');        
    Route::post('/aprovacoes/approve', 'AprovacaoController@approve');
    Route::post('/aprovacoes/reject', 'AprovacaoController@reject');
    #endregion
});