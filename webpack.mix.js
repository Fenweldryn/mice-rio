const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public_html/');
mix.js('resources/js/app.js', 'public_html/js').version()
    .sass('resources/sass/app.scss', 'public_html/css').version()
    .sass('resources/sass/index/style.scss', 'public_html/css').version()    
    .sass('resources/sass/admin.scss', 'public_html/css').version()    
    .copy('resources/plugins/datatables', 'public_html/plugins/datatables')
    .copy('node_modules/chart.js/dist/Chart.min.js', 'public_html/plugins/chartjs')
    .copy('node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', 'public_html/plugins/bootstrap-datepicker')
    .copy('node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', 'public_html/plugins/bootstrap-datepicker')
    .copy('node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', 'public_html/plugins/bootstrap-datepicker');
