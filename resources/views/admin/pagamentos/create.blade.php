@extends('layouts.admin')
@section('title','Informar Pagamento')

@section('css')
    <link href="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <style>
        .custom-control-label {
            font-weight: 500 !important;
        }
    </style>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin/associados')}}">Associados</a></li>    
    <li class="breadcrumb-item active">Informar Pagamento</li>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <a class="btn btn-default mr-2" href="{{ url('admin/pagamentos') }}"><i class='fas fa-arrow-left'></i> Voltar</a>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        @if(Session::has('info'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{Session::get('info')}}
            </div>        
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>            
                {{Session::get('success')}}
                <b>{{Session::get('target')}}</b>
            </div>        
        @endif
        
    </div>
</div>
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/pagamentos/batch') }}">            
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class="form-group mt-3 mb-3">
                    <label>Data</label>
                    <input id="data" class="form-control" name="data" placeholder="escolha uma data">
                </div>  

                <ul class="list-group mb-3"></ul>

                <p id='contador' style="display:none"><b id='selecionados'>0</b> selecionados. 
                    <a class='ml-3' href='#' onclick="desmarcarTodos()">desmarcar todos</a>
                    <a class='ml-3' href='#' onclick="marcarTodos()">marcar todos</a>
                </p>
                <button type="submit" class="btn btn-success" disabled>
                    <i class="fas fa-check"></i> Informar Pagamentos 
                </button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js')}}"></script>
<script>
    function marcarTodos() {
        $('form li input').prop('checked', true);
        $("#selecionados").html($('form li input').length);
    }
    function desmarcarTodos() {
        $('form li input').prop('checked', false);
        $("#selecionados").html(0);
    }

    $('form').on('click', 'li', function (e) {
        e.preventDefault();
        if ($(this).find('input').prop('checked')) {
            $("#selecionados").html(parseInt($("#selecionados").html()) + 1);            
        } else {
            $("#selecionados").html(parseInt($("#selecionados").html()) - 1);            
        }
        if (parseInt($("#selecionados").html()) > 0) {
            $("form .btn-success").prop('disabled', false);
        } else {
            $("form .btn-success").prop('disabled', true);
        }
    });

    $('#data').datepicker({
        language:'pt-BR',
        clearBtn: true,
        startView: 1,
        minViewMode: 1,
        maxViewMode: 2,
        format: "mm/yyyy",
        orientation: "bottom auto",
        autoclose: true
    });
    $("#data").change(function (e) { 
        e.preventDefault();
        $('form').LoadingOverlay('show');
        $.ajax({
            type: "GET",
            url: "{{url('admin/pagamentos/vencidos')}}",
            data: {'data':$("#data").val()},
            success: function (response) {
                
                if (response.length > 0) {
                    $("form ul").html('');
                    $.each(response, function (index, val) {                                                   
                        $("form ul").append('<li class="list-group-item" onclick="$(\'#associado'+val.id+'\').prop(\'checked\', !$(\'#associado'+val.id+'\').prop(\'checked\'))">'+
                            '<div class="custom-control custom-checkbox">'+
                                '<input class="custom-control-input" name="associados[]" type="checkbox" id="associado'+val.id+'" value="'+val.id+'">'+
                                '<label for="associado'+val.id+'" class="custom-control-label">'+val.nome+'</label>'+
                            '</div>'+
                        '</li>');
                    });
                    $('#contador').show();
                } else {
                    $('form ul').html('nenhuma pendência encontrada para a data selecionada');
                    $('#contador').hide();
                }
                $('form').LoadingOverlay('hide');
            }
        });
    });
</script>
@endsection