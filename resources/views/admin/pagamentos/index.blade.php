@extends('layouts.admin')

@section('title')
    Pagamentos
    @if(auth()->user()->can('gerir pagamentos'))
    {{-- <a href="{{route('pagamentos.create')}}" class="btn btn-success mr-2"><i class="fas fa-plus"></i> Cadastrar</a> --}}
    <a href="{{route('pagamentos.create')}}" class="btn btn-primary"><i class="fas fa-hand-holding-usd"></i> Informar Pagamento em Massa</a>
    @endif
@endsection

@section('css')
    <link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>    
    <script>

    function informarPagamento(botao, pagamento_id){       
        $(botao).LoadingOverlay('show');
        $.ajax({
            type: "POST",
            url: "{{route('pagamentos.store')}}",
            data: {pagamento_id:pagamento_id},
            success: function (response) {
                // location.reload();
            }
        });
    }
    
    $(".content").LoadingOverlay("show");
    $(document).ready(function() {
        
        $('table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ url('admin/pagamentos/datatable') }}",  
            columns: [
                {data: 'created_at'},
                {data: 'status'},                
                {data: 'entidade'},                
                {data: 'associado'},                
                {data: 'dia_vencimento'},                
                {data: 'valor'},                
                @if(auth()->user()->can('gerir pagamentos'))
                {data: 'action', orderable: false, searchable: false},      
                @endif          
            ],
            dom: '<"float-right" B>lrtip',
            buttons: [
                {
                    text: '<i class="fas fa-file-excel"></i> Exportar para excel',
                    extend:'excelHtml5',
                    title:"MICE Rio | Pagamentos | exportado em {{date('m/d/Y')}}",
                }
            ],
            language: {
                url: "{{ asset('plugins/datatables/pt-br.json') }}"
            },
            initComplete: function(){
                $(".content").LoadingOverlay("hide");
            }
        });
    });
    </script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">Pagamentos</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('success')}}
        </div>        
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-body">                
                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Status</th>
                            <th>CVB</th>
                            <th>Associado</th>
                            <th>Dia Vencimento</th>
                            <th>Valor</th>
                            @if(auth()->user()->can('gerir pagamentos'))
                            <th>Ações</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    
@endsection