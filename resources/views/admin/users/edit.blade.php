@extends('layouts.admin')
@section('title','Editar Usuário')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Usuários</a></li>
    <li class="breadcrumb-item active">Editar Usuário</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/users',$user->id) }}">
            @method('PATCH')
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class='row'>                    
                    <label>Nome</label>
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') === null ? $user->name : (old('name') === null ? $user->name : old('name')) }}" required >

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Email</label>
                    <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') === null ? $user->email : (old('email') === null ? $user->email : old('email')) }}" readonly>
                </div>

                <div class='row'>                    
                    <label>Senha</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="" >
                </div>

                <div class='row'>                    
                    <label>Confirmar senha</label>
                    <input id="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" value="" >
                </div>
                
                <div class="row mt-2">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Salvar edição
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection