@extends('layouts.admin')

@section('title')
Usuários
@if (auth()->user()->hasRole('admin'))
    <a href="{{route('users.create')}}" class="btn btn-success"><i class="fas fa-plus"></i> Cadastrar</a>
@endif
@endsection

@section('css')
    <link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
    <script>
    idUser = 0;
    tituloModal = '';
    
    $('#perfis').on('hide.bs.modal', function (e) {
        clearFields();
    });
    $(document).ready(function() {
        loadPerfis();    
        $('table').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ url('admin/users/datatable') }}",   
            columns: [
                {data: 'id'},
                {data: 'name'},
                {data: 'email'},
                @if(auth()->user()->can('gerir usuários'))
                {data: 'roles'},
                @endif
                {data: 'created_at'},
                @if(auth()->user()->can('gerir usuários'))
                {data: 'action', orderable: false, searchable: false},                
                @endif
            ],
            language: {
                url: "{{ asset('plugins/datatables/pt-br.json') }}"
            }
        });
    });
    function ajaxThenCallModal(id, titulo) {        
        conteudo = "";        
        tituloModal = titulo;
        $(".modal-body").LoadingOverlay("show");
        $.ajax("{{url('')}}/admin/users/"+id+"/roles").always(function(response) {
            if (response) {
                conteudo = "<table class='table table-bordered'><thead><th>Perfil</th><th>Ações</th></thead><tbody>";
                $.each(response, function (index, value) {                    
                    conteudo += '<tr><td>'+value.name+'</td>'+   
                    '<td><button class="btn btn-danger" onclick="removePerfil('+id+','+value.id+')"><i class="fas fa-trash"></i></button></tr>';                        
                });                    
                conteudo += "</tbody></table>";            
            } else {
                conteudo = "Nenhum perfil encontrado.";
            }
            $('.modal .modal-title').html('Perfis de ' + titulo);
            $('.modal .modal-body').html(conteudo);              
            idUser = id;    
            $('.modal table').DataTable({
                destroy: true,
                language: {
                    url: "{{ asset('plugins/datatables/pt-br.json') }}"
                }
            });
            $(".modal-body").LoadingOverlay("hide");   
            $('.modal').modal('show');                    
        });
    }

    function removePerfil(userID, perfilID) {
        r = confirm("Tem certeza que deseja remover?");
        if (r) {
            $.ajax({
                type: "DELETE",
                url: "{{url("")}}/admin/users/"+userID+"/roles",
                data: {"id": perfilID},                        
            }).always(function(response){
                ajaxThenCallModal(userID, tituloModal);        
            });            
        }
    }

    function loadPerfis() {
        $.ajax("{{url('admin/roles/list')}}").always(function(response) {            
            perfis = "";
            $.each(response, function (index, value) { 
                perfis += "<option value="+value.id+">"+value.name+"</option>";
            });            
            $('.modal .modal-footer #perfisSelect').html(perfis);
            $('#perfisSelect').SumoSelect({placeholder: "Selecione uma opção"});            
        });   
    }

    function addPerfis() {                
        id = $('#perfisSelect option:selected').val();                      
        $.ajax({
            url: "{{url('')}}/admin/users/"+idUser+"/roles",
            data: {"id": id},
            type: 'POST'
        }).always(function(response) {   
            console.log(response); 
            ajaxThenCallModal(idUser, tituloModal);        
            clearFields();
        });        
    }

    function editPerfis() {        
        perfis = [];        
        $('#perfisSelect option:selected').each(function(i){
            perfis.push($(this).val());
        });        
        console.log(perfis);
        $.ajax({
            url: "{{url('')}}/admin/users/"+idUser+"/roles",
            data: {"perfis": perfis},
            type: 'POST'
        }).always(function(response) {   
            console.log(response); 
            ajaxThenCallModal(idUser, tituloModal);        
            clearFields();
        });        
    }

    function clearFields() {
        $('#perfisSelect')[0].sumo.unSelectAll();
        $('#perfilValor').val("");
    }
    </script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">Usuários</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('success')}}
        </div>        
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-body">                
                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Email</th>
                            @if(auth()->user()->can('gerir usuários'))
                            <th>Perfis</th>
                            @endif
                            <th>Criado em</th>
                            @if(auth()->user()->can('gerir usuários'))
                            <th>Ações</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    @if(auth()->user()->can('gerir usuários'))
    <div id="perfis" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">                                       
                    <div class='row w-100'>
                        <div class='col-md-7 col-sm-12 col-xs-12'>
                            <select id="perfisSelect" class="form-control" name=""></select>  
                        </div>                             
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addPerfis()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>
                    </div>                    
                </div>                
            </div>
        </div>
    </div>   
    @endif
@endsection