@extends('layouts.admin')
@section('title','Cadastrar Usuário')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Perfis</a></li>
    <li class="breadcrumb-item active">Cadastrar Usuário</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/users') }}">            
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class='row'>                    
                    <label>Nome</label>
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" required >

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" user="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Email</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required >

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" user="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif                    
                </div>
               
                <div class='row'>                    
                    <label>Senha</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required >

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" user="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Confirmar Senha</label>
                    <input id="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required >

                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" user="alert">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif                    
                </div>
               
                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Cadastrar </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection