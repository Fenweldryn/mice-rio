@extends('layouts.admin')

@section('title')
    Permutas
    <a href="{{route('permutas.create')}}" class="btn btn-success"><i class="fas fa-plus"></i> Cadastrar</a>
    {{-- <a href="{{route('permutas.create')}}" class="btn btn-primary"><i class="fas fa-minus"></i> Dar baixa</a> --}}
@endsection

@section('css')
@endsection

@section('js')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">Permutas</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('success')}}
        </div>        
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-body">                
                <table class="table table-bordered w-100" width="100%">
                    <thead>
                        <tr>
                            <th>Associado</th>
                            <th>CVB</th>
                            <th>Descrição</th>
                            <th>Quantidade<br>por mês</th>
                            <th>Quantidade<br>disponível</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($permutas as $permuta)
                            <tr>
                                <td>{{ $permuta->associado->nome }}</td>
                                <td>{{ $permuta->entidade->nome_fantasia }}</td>
                                <td>{{ $permuta->descricao }}</td>
                                <td>{{ $permuta->quantidade_por_mes }}</td>
                                <td>{{ $permuta->quantidade_disponivel }}</td>
                                <td width="20%"">
                                    <div class="d-flex" style="justify-content: space-evenly">
                                        <a href={{ url("admin/permuta/$permuta->id/baixa") }} class="btn btn-secondary"><i class="fas fa-check"></i> dar baixa</a>
                                       
                                        <a href={{ route("permutas.edit", $permuta->id) }} class="btn btn-primary"><i class="fas fa-pencil-alt"></i> </a>
                                        
                                        <form action={{ route("permutas.destroy", $permuta->id) }} method="POST"                        
                                                onsubmit="return confirm('Tem certeza que deseja excluir?');">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @csrf
                                            <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                                        </form>

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
   
@endsection