@extends('layouts.admin')
@section('title','Dar baixa')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin/permutas')}}">Permutas</a></li>
    <li class="breadcrumb-item active">Dar baixa</li>
@endsection

@section('css')
    <link href="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js')}}"></script>
    <script>
        $('#data').datepicker({
            language:'pt-BR',
            clearBtn: true,
            format: "dd/mm/yyyy",
            orientation: "bottom auto",
            autoclose: true
        });
    </script>

@endsection

@section('content')
<div class="row">
    <div class="col">
        <a href="{{ url('admin/permutas') }}" class='btn btn-default'><i class='fas fa-arrow-left'></i> Voltar</a>        
    </div>
</div>
<div class="row">
    <div class="col-md-4">                 
        <form method="POST" action="{{ url('/admin/historicopermutas') }}">            
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{Session::get('success')}}
                </div>        
            @endif
            

            <div class="card card-primary card-body">       
                <div class="form-group">
                    <label for="">Associado</label>
                    <select id="" class="form-control" name="associado_id" readonly >
                        <option value='{{ $permuta->associado->id }}'>{{ $permuta->associado->nome }}</option>                            
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Permuta</label>
                    <input class="form-control" type="text" name="" readonly value={{ $permuta->descricao }}>
                    <input class="form-control" style='display:none' type="text" name="permuta_id" readonly value={{ $permuta->id }}>
                </div>

                <div class="form-group">
                    <label for="">Quantidade Usada</label>
                    <input class="form-control" type="text" name="quantidade" required>
                </div>

                <div class="form-group">
                    <label for="">Usado em</label>
                    <input class="form-control" id='data' type="text" name="usado_em" required>
                </div>

                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Dar baixa </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-8">    
        <h3>Histórico de baixas</h3>             
        <table class="table table-bordered table-striped">
            <thead class="">
                <tr>
                    <th>Associado</th>
                    <th>Usuário</th>
                    <th>Descrição</th>
                    <th>Quantidade Usada</th>
                    <th>Usado em</th>
                    <th>Baixa em</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($historicos as $historico)
                <tr>
                    <td>{{ $historico->permuta->associado->nome }}</td>
                    <td>{{ $historico->user->name }}</td>
                    <td>{{ $historico->permuta->descricao }}</td>
                    <td>{{ $historico->quantidade }}</td>
                    <td>{{ date('d/m/Y', strtotime($historico->usado_em)) }}</td>
                    <td>{{ date('d/m/Y h:i:s', strtotime($historico->created_at)) }}</td>
                    <td> 
                        <form action={{ route("historicopermutas.destroy", $historico->id) }} method="POST"                        
                                onsubmit="return confirm('Tem certeza que deseja excluir?');">
                            <input type="hidden" name="_method" value="DELETE">
                            @csrf
                            <button class="btn btn-danger"><i class="fas fa-trash"></i> </button>
                        </form>
                    </td>
                </tr>                    
                @endforeach
            </tbody>            
        </table>
    </div>
</div>
@endsection