@extends('layouts.admin')
@section('title','Editar Modal')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Modals</a></li>
    <li class="breadcrumb-item active">Editar Modal</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/modais',$modal->id) }}">
            @method('PATCH')
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class='row'>                    
                    <label>Nome</label>
                    <input id="nome" type="text" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" name="nome" value="{{$modal->nome }}" required >

                    @if ($errors->has('nome'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nome') }}</strong>
                        </span>
                    @endif                    
                </div>
                
                <div class='row'>                    
                    <label>Tipo</label>
                    <select required class="form-control" name="modal_tipo_id" id="modal_tipo_id">
                        @foreach ($modal_tipos as $tipo)
                            @if ($tipo->id == $modal->modal_tipo_id || old('modal_tipo_id') == $modal->modal_tipo_id)
                                <option value="{{$tipo->id}}" selected>{{$tipo->nome}}</option>
                            @else    
                                <option value="{{$tipo->id}}">{{$tipo->nome}}</option>
                            @endif    
                        @endforeach                        
                    </select>    

                    @if ($errors->has('tipo'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tipo') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Cidade</label>
                    <select required class="form-control" name="cidade_id" id="cidade_id">
                        @foreach ($cidades as $cidade)
                            @if ($cidade->id == $modal->cidade_id || old('cidade_id') == $modal->cidade_id)
                                <option value="{{$cidade->id}}" selected>{{$cidade->nome}}</option>
                            @else    
                                <option value="{{$cidade->id}}">{{$cidade->nome}}</option>
                            @endif    
                        @endforeach                        
                    </select>    

                    @if ($errors->has('cidade_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('cidade_id') }}</strong>
                        </span>
                    @endif                    
                </div>         
                                
                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Salvar edição
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection