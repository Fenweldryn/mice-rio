@extends('layouts.admin')
@section('title','Cadastrar Modal')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Modals</a></li>
    <li class="breadcrumb-item active">Cadastrar Modal</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/modais') }}">            
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class='row'>                    
                    <label>Nome</label>
                    <input id="nome" type="text" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" name="nome" value='{{old("nome")}}' required >

                    @if ($errors->has('nome'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nome') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Tipo</label>
                    <select required class="form-control" name="modal_tipo_id" id="modal_tipo_id">
                        <option value=""></option>
                        @foreach ($modal_tipos as $tipo)
                            <option value="{{$tipo->id}}">{{$tipo->nome}}</option>
                        @endforeach                        
                    </select>    

                    @if ($errors->has('tipo'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tipo') }}</strong>
                        </span>
                    @endif                    
                </div>
               
                <div class='row'>                    
                    <label>Cidade</label>
                    <select name="cidade_id" class='form-control' id="" required>
                        <option value=""></option>
                        @foreach ($cidades as $cidade)
                            <option value="{{$cidade->id}}">{{ $cidade->nome }}</option>
                        @endforeach
                    </select>
                    
                    @if ($errors->has('cidade_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('cidade_id') }}</strong>
                        </span>
                    @endif                    
                </div>   
                
                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Cadastrar </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection