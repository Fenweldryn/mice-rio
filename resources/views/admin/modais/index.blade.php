@extends('layouts.admin')

@section('title')
    Modais
    @if(auth()->user()->can('gerir modais'))
    <a href="{{route('modais.create')}}" class="btn btn-success"><i class="fas fa-plus"></i> Cadastrar</a>
    @endif
@endsection

@section('css')
    <link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>    
    <script>
    idModal = 0;
    tituloModal = '';
    
    $(".content").LoadingOverlay("show");
    $(document).ready(function() {
        
        $('table').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ url('admin/modais/datatable') }}",   
            columns: [
                {data: 'id'},
                {data: 'nome'},
                {data: 'tipo'},
                {data: 'cidade'},                
                {data: 'created_at'},
                @if(auth()->user()->can('gerir modais'))
                {data: 'action', orderable: false, searchable: false},                
                @endif
            ],
            language: {
                url: "{{ asset('plugins/datatables/pt-br.json') }}"
            },
            initComplete: function(){
                $(".content").LoadingOverlay("hide");
            }
        });
    });

    function ajaxThenCallModal(id, titulo) {        
        conteudo = "";        
        tituloModal = titulo;
        $(".modal-body").LoadingOverlay("show");        
    }
    </script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">Modals</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('success')}}
        </div>        
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-body">                
                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Tipo</th>
                            <th>Cidade</th>
                            <th>Criado em</th>
                            @if(auth()->user()->can('gerir modais'))
                            <th>Ações</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    
@endsection