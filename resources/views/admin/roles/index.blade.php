@extends('layouts.admin')

@section('title')
    Perfis
    <a href="{{route('roles.create')}}" class="btn btn-success"><i class="fas fa-plus"></i> Cadastrar</a>
@endsection

@section('css')
    <link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>    
    <script>
    idRole = 0;
    tituloModal = '';
    
    $('#permissions').on('hide.bs.modal', function (e) {
        clearFields();
    });
    $(".content").LoadingOverlay("show");
    $(document).ready(function() {
        loadPermissions();
        $('table').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ url('admin/roles/datatable') }}",   
            columns: [
                {data: 'id'},
                {data: 'name'},
                {data: 'permissions'},
                {data: 'created_at'},
                {data: 'action', orderable: false, searchable: false},                
            ],
            language: {
                url: "{{ asset('plugins/datatables/pt-br.json') }}"
            },
            initComplete: function(){
                $(".content").LoadingOverlay("hide");
            }
        });
    });
    function ajaxThenCallModal(id, titulo) {        
        conteudo = "";        
        tituloModal = titulo;
        $(".modal-body").LoadingOverlay("show");
        $.ajax("{{url('')}}/admin/roles/"+id+"/permissions").always(function(response) {
            if (response) {
                conteudo = "<table class='table table-bordered'><thead><th>Permissão</th><th>Ações</th></thead><tbody>";
                $.each(response, function (index, value) { 
                    conteudo += '<tr><td>'+value.name+'</td>'+   
                    '<td><button class="btn btn-danger" onclick="removePermission('+id+','+value.id+')"><i class="fas fa-trash"></i></button></td></tr>';
                });                    
                conteudo += "</tbody></table>";            
            } else {
                conteudo = "Nenhuma permissão encontrada.";
            }
            $('.modal .modal-title').html('Permissões do ' + titulo);
            $('.modal .modal-body').html(conteudo);              
            idRole = id;    
            $('.modal table').DataTable({
                destroy: true,
                pageLength: 5,
                language: {
                    url: "{{ asset('plugins/datatables/pt-br.json') }}"
                }
            });
            $(".modal-body").LoadingOverlay("hide");   
            $('.modal').modal('show');                    
        });
    }

    function removePermission(roleID, permissionID) {
        r = confirm("Tem certeza que deseja remover?");
        if (r) {
            $.ajax({
                type: "DELETE",
                url: "{{url("")}}/admin/roles/"+roleID+"/permissions",
                data: {"id": permissionID},                        
            }).always(function(response){
                ajaxThenCallModal(roleID, tituloModal);        
            });            
        }
    }

    function loadPermissions() {
        $.ajax("{{url('admin/permissions/list')}}").always(function(response) {            
            permissions = "";
            console.log(response);
            $.each(response, function (index, value) { 
                permissions += "<option value="+value.id+">"+value.name+"</option>";
            });            
            $('.modal .modal-footer #permissionsSelect').html(permissions);
            $('#permissionsSelect').SumoSelect({placeholder: "Selecione uma opção"});            
        });   
    }

    function addPermissions() {                
        id = $('#permissionsSelect option:selected').val();        
        $.ajax({
            url: "{{url('')}}/admin/roles/"+idRole+"/permissions",
            data: {"id": id},
            type: 'POST'
        }).always(function(response) {   
            console.log(response); 
            ajaxThenCallModal(idRole, tituloModal);        
            clearFields();
        });        
    }

    function clearFields() {
        try {
            $('#permissionsSelect')[0].sumo.unSelectAll();
            $('#caracteristicaValor').val("");    
        } catch (error) {
            
        }
        
    }

    </script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">Perfis</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('success')}}
        </div>        
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-body">                
                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Permissões</th>
                            <th>Criado em</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>    
    <div id="permissions" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">                                       
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select id="permissionsSelect" class="form-control" name=""></select>  
                        </div>                             
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addPermissions()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>
                    </div>                    
                </div>                
            </div>
        </div>
    </div>   
@endsection