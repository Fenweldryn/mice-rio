@extends('layouts.admin')
@section('title','Cadastrar Perfil')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Perfis</a></li>
    <li class="breadcrumb-item active">Cadastrar Perfil</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/roles') }}">            
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class='row'>                    
                    <label>Nome</label>
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" required >

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif                    
                </div>
               
                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Cadastrar </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection