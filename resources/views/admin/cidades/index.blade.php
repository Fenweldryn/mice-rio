@extends('layouts.admin')

@section('title')
    Cidades
    @if (auth()->user()->can('cadastrar cidade'))
        <a href="{{route('cidades.create')}}" class="btn btn-success"><i class="fas fa-plus"></i> Cadastrar</a>
    @endif
@endsection

@section('css')
    <link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>    
    <script>
    idCidade = 0;
    tituloModal = '';
    $('#modalTipos').on('hide.bs.modal', function (e) {
        $('#modalTiposSelect')[0].sumo.unSelectAll();
    });
    $('#alojamentoTipos').on('hide.bs.modal', function (e) {
        $('#alojamentoTiposSelect')[0].sumo.unSelectAll();
    });
    $('#localEventoTipos').on('hide.bs.modal', function (e) {
        $('#localEventoTiposSelect')[0].sumo.unSelectAll();
    });
    $(".content-wrapper").LoadingOverlay("show");
    $(document).ready(function() {
        loadAlojamentoTipos();
        loadModalTipos();
        loadLocalEventoTipos();
        $('table').DataTable({
            fixedHeader: true,
            processing: true,
            serverSide: true,
            responsive: true,            
            ajax: "{{ url('admin/cidades/datatable') }}",               
            columns: [
                {data: 'id', responsivePriority: 1},
                {data: 'is_active'},
                {data: 'approval_status'},
                {data: 'nome',responsivePriority: 1},
                {data: 'estado', responsivePriority: 1},
                {data: 'estado_sigla', responsivePriority: 1},                
                {data: 'alojamentoTipos', responsivePriority: 1},                
                {data: 'modalTipos', responsivePriority: 1},                
                {data: 'localEventoTipos', responsivePriority: 1},                
                {data: 'distancia_capital'},                
                {data: 'tem_centro_atendimento'},                
                {data: 'tem_rodovia_estadual'},                
                {data: 'tem_rodovia_federal'},    
                {data: "capacidade_maior_espaco"},
                {data: "capacidade_maior_auditorio"},
                {data: "capacidade_maior_banquete"},
                {data: "capacidade_total_quartos"},
                {data: "capacidade_total_leitos"},
                {data: "capacidade_quartos_maior_hotel"},
                {data: "numero_instituicoes_ensino_superior"},
                {data: "areas_expertise_referencia"},
                {data: "principais_eventos"},                           
                {data: 'created_at'},
                @if(auth()->user()->can('gerir cidades'))
                {data: 'action', orderable: false, searchable: false, responsivePriority: 1},                
                @endif
            ],
            language: {
                url: "{{ asset('plugins/datatables/pt-br.json') }}"
            },
            initComplete: function(){
                $("table").show();
                $(".content-wrapper").LoadingOverlay("hide");
            }
        });
    });

    function ajaxThenCallModalAlojamentoTipos(id, titulo) {        
        conteudo = "";        
        tituloModal = titulo;
        $("#alojamentoTipos .modal-body").LoadingOverlay("show");
        $.ajax("{{url('')}}/admin/cidades/"+id+"/alojamentoTipos").always(function(response) {
            if (response) {
                conteudo = "<table class='table table-bordered'><thead><th>Nome</th><th>Ações</th></thead><tbody>";
                $.each(response, function (index, value) { 
                    conteudo += '<tr><td>'+value.nome+'</td><td><button class="btn btn-danger" onclick="removeAlojamentoTipos('+id+','+value.id+')"><i class="fas fa-trash"></i> </button></td></tr>';
                });                    
                conteudo += "</tbody></table>";            
            } else {
                conteudo = "Nenhum tipo de alojamento encontrado.";
            }
            $('#alojamentoTipos .modal-title').html('Tipos de Alojamento de ' + titulo);
            $('#alojamentoTipos .modal-body').html(conteudo);              
            idCidade = id;    
            $('#alojamentoTipos table').DataTable({
                destroy: true,
                language: {
                    url: "{{ asset('plugins/datatables/pt-br.json') }}"
                }
            });
            $("#alojamentoTipos .modal-body").LoadingOverlay("hide");   
            $('#alojamentoTipos').modal('show');              
        });
    }
    function removeAlojamentoTipos(cidadeID, alojamentoTiposID) {
        r = confirm("Tem certeza que deseja remover?");
        if (r) {
            $.ajax({
                type: "DELETE",
                url: "{{url("")}}/admin/cidades/"+cidadeID+"/alojamentoTipos",
                data: {"alojamentoTiposID": alojamentoTiposID},                        
            }).always(function(response){
                ajaxThenCallModalAlojamentoTipos(cidadeID, tituloModal);        
            });            
        }
    }
    function loadAlojamentoTipos() {
        $.ajax("{{url('admin/alojamentotipos/list')}}").always(function(response) {            
            alojamentoTipos = "";
            $.each(response, function (index, value) { 
                alojamentoTipos += "<option value="+value.id+">"+value.nome+"</option>";
            });            
            $('.modal .modal-footer #alojamentoTiposSelect').html(alojamentoTipos);
            $('#alojamentoTiposSelect').SumoSelect({placeholder:"Selecione uma ou várias opções",forceCustomRendering: true});            
        });   
    }
    function addAlojamentoTipos() {        
        alojamentoTipos = [];        
        $('#alojamentoTiposSelect option:selected').each(function(i){
            alojamentoTipos.push($(this).val());
        });        
        $.ajax({
            url: "{{url('')}}/admin/cidades/"+idCidade+"/addAlojamentoTipos",
            data: {"alojamentoTipos": alojamentoTipos},
            type: 'POST'
        }).always(function(response) {   
            console.log(response); 
            ajaxThenCallModalAlojamentoTipos(idCidade, tituloModal);        
            $('#alojamentoTiposSelect')[0].sumo.unSelectAll();
        });        
    }

    function ajaxThenCallModalModalTipos(id, titulo) {        
        conteudo = "";        
        tituloModal = titulo;
        $("#modalTipos .modal-body").LoadingOverlay("show");
        $.ajax("{{url('')}}/admin/cidades/"+id+"/modalTipos").always(function(response) {
            if (response) {
                conteudo = "<table class='table table-bordered'><thead><th>Nome</th><th>Ações</th></thead><tbody>";
                $.each(response, function (index, value) { 
                    conteudo += '<tr><td>'+value.nome+'</td><td><button class="btn btn-danger" onclick="removeModalTipos('+id+','+value.id+')"><i class="fas fa-trash"></i> </button></td></tr>';
                });                    
                conteudo += "</tbody></table>";            
            } else {
                conteudo = "Nenhum tipo de modal encontrado.";
            }
            $('#modalTipos .modal-title').html('Tipos de Modal de ' + titulo);
            $('#modalTipos .modal-body').html(conteudo);              
            idCidade = id;    
            $('#modalTipos table').DataTable({
                destroy: true,
                language: {
                    url: "{{ asset('plugins/datatables/pt-br.json') }}"
                }
            });
            $("#modalTipos .modal-body").LoadingOverlay("hide");   
            $('#modalTipos').modal('show');              
        });
    }
    function removeModalTipos(cidadeID, modalTiposID) {
        r = confirm("Tem certeza que deseja remover?");
        if (r) {
            $.ajax({
                type: "DELETE",
                url: "{{url("")}}/admin/cidades/"+cidadeID+"/modalTipos",
                data: {"modalTiposID": modalTiposID},                        
            }).always(function(response){
                ajaxThenCallModalModalTipos(cidadeID, tituloModal);        
            });            
        }
    }
    function loadModalTipos() {
        $.ajax("{{url('admin/modaltipos/list')}}").always(function(response) {            
            modalTipos = "";
            $.each(response, function (index, value) { 
                modalTipos += "<option value="+value.id+">"+value.nome+" ("+value.tipo+")</option>";
            });            
            $('.modal .modal-footer #modalTiposSelect').html(modalTipos);
            $('#modalTiposSelect').SumoSelect({placeholder:"Selecione uma ou várias opções", forceCustomRendering: true});            
        });   
    }
    function addModalTipos() {        
        modalTipos = [];        
        $('#modalTiposSelect option:selected').each(function(i){
            modalTipos.push($(this).val());
        });        
        $.ajax({
            url: "{{url('')}}/admin/cidades/"+idCidade+"/addModalTipos",
            data: {"modalTipos": modalTipos},
            type: 'POST'
        }).always(function(response) {   
            console.log(response); 
            ajaxThenCallModalModalTipos(idCidade, tituloModal);        
            $('#modalTiposSelect')[0].sumo.unSelectAll();
        });        
    }

    function ajaxThenCallModalLocalEventoTipos(id, titulo) {        
        conteudo = "";        
        tituloModal = titulo;
        $("#localEventoTipos .modal-body").LoadingOverlay("show");
        $.ajax("{{url('')}}/admin/cidades/"+id+"/localEventoTipos").always(function(response) {
            if (response) {
                conteudo = "<table class='table table-bordered'><thead><th>Nome</th><th>Ações</th></thead><tbody>";
                $.each(response, function (index, value) { 
                    conteudo += '<tr><td>'+value.nome+'</td><td><button class="btn btn-danger" onclick="removeLocalEventoTipos('+id+','+value.id+')"><i class="fas fa-trash"></i> </button></td></tr>';
                });                    
                conteudo += "</tbody></table>";            
            } else {
                conteudo = "Nenhum tipo de local encontrado.";
            }
            $('#localEventoTipos .modal-title').html('Tipos de Local de Evento ' + titulo);
            $('#localEventoTipos .modal-body').html(conteudo);              
            idCidade = id;    
            $('#localEventoTipos table').DataTable({
                destroy: true,
                language: {
                    url: "{{ asset('plugins/datatables/pt-br.json') }}"
                }
            });
            $("#localEventoTipos .modal-body").LoadingOverlay("hide");   
            $('#localEventoTipos').modal('show');              
        });
    }
    function removeLocalEventoTipos(cidadeID, localEventoTiposID) {
        r = confirm("Tem certeza que deseja remover?");
        if (r) {
            $.ajax({
                type: "DELETE",
                url: "{{url("")}}/admin/cidades/"+cidadeID+"/localEventoTipos",
                data: {"localEventoTiposID": localEventoTiposID},                        
            }).always(function(response){
                ajaxThenCallModalLocalEventoTipos(cidadeID, tituloModal);        
            });            
        }
    }
    function loadLocalEventoTipos() {
        $.ajax("{{url('admin/localeventotipos/list')}}").always(function(response) {            
            localEventoTipos = "";
            $.each(response, function (index, value) { 
                localEventoTipos += "<option value="+value.id+">"+value.nome+"</option>";
            });            
            $('.modal .modal-footer #localEventoTiposSelect').html(localEventoTipos);
            $('#localEventoTiposSelect').SumoSelect({placeholder:"Selecione uma ou várias opções", forceCustomRendering: true});            
        });   
    }
    function addLocalEventoTipos() {        
        localEventoTipos = [];        
        $('#localEventoTiposSelect option:selected').each(function(i){
            localEventoTipos.push($(this).val());
        });        
        $.ajax({
            url: "{{url('')}}/admin/cidades/"+idCidade+"/addLocalEventoTipos",
            data: {"localEventoTipos": localEventoTipos},
            type: 'POST'
        }).always(function(response) {   
            console.log(response); 
            ajaxThenCallModalLocalEventoTipos(idCidade, tituloModal);        
            $('#localEventoTiposSelect')[0].sumo.unSelectAll();
        });        
    }

    function ajaxThenCallModalAlojamentoCategorias(id, titulo) {        
        conteudo = "";        
        tituloModal = titulo;
        $("#alojamentoCategorias .modal-body").LoadingOverlay("show");
        $.ajax("{{url('')}}/admin/cidades/"+id+"/alojamentoCategorias").always(function(response) {
            if (response) {
                conteudo = "<table class='table table-bordered'><thead><th>Nome</th><th>Ações</th></thead><tbody>";
                $.each(response, function (index, value) { 
                    conteudo += '<tr><td>'+value.nome+'</td><td><button class="btn btn-danger" onclick="removeAlojamentoCategorias('+id+','+value.id+')"><i class="fas fa-trash"></i> </button></td></tr>';
                });                    
                conteudo += "</tbody></table>";            
            } else {
                conteudo = "Nenhum tipo de local encontrado.";
            }
            $('#alojamentoCategorias .modal-title').html('Tipos de Modal de ' + titulo);
            $('#alojamentoCategorias .modal-body').html(conteudo);              
            idCidade = id;    
            $('#alojamentoCategorias table').DataTable({
                destroy: true,
                language: {
                    url: "{{ asset('plugins/datatables/pt-br.json') }}"
                }
            });
            $("#alojamentoCategorias .modal-body").LoadingOverlay("hide");   
            $('#alojamentoCategorias').modal('show');              
        });
    }
    function removeAlojamentoCategorias(cidadeID, alojamentoCategoriasID) {
        r = confirm("Tem certeza que deseja remover?");
        if (r) {
            $.ajax({
                type: "DELETE",
                url: "{{url("")}}/admin/cidades/"+cidadeID+"/alojamentoCategorias",
                data: {"alojamentoCategoriasID": alojamentoCategoriasID},                        
            }).always(function(response){
                ajaxThenCallModalAlojamentoCategorias(cidadeID, tituloModal);        
            });            
        }
    }
    function loadAlojamentoCategorias() {
        $.ajax("{{url('admin/alojamentocategorias/list')}}").always(function(response) {            
            alojamentoCategorias = "";
            $.each(response, function (index, value) { 
                alojamentoCategorias += "<option value="+value.id+">"+value.nome+"</option>";
            });            
            $('.modal .modal-footer #alojamentoCategoriasSelect').html(alojamentoCategorias);
            $('#alojamentoCategoriasSelect').SumoSelect({placeholder:"Selecione uma ou várias opções", forceCustomRendering: true});            
        });   
    }
    function addAlojamentoCategorias() {        
        alojamentoCategorias = [];        
        $('#alojamentoCategoriasSelect option:selected').each(function(i){
            alojamentoCategorias.push($(this).val());
        });        
        $.ajax({
            url: "{{url('')}}/admin/cidades/"+idCidade+"/addAlojamentoCategorias",
            data: {"alojamentoCategorias": alojamentoCategorias},
            type: 'POST'
        }).always(function(response) {   
            console.log(response); 
            ajaxThenCallModalAlojamentoCategorias(idCidade, tituloModal);        
            $('#alojamentoCategoriasSelect')[0].sumo.unSelectAll();
        });        
    }

    </script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">Cidades</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('success')}}
        </div>        
    @endif
    @if(Session::has('error'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('error')}}
        </div>        
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-body">                
                <table class="table table-bordered w-100" style="display:none">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Ativo?</th>
                            <th>Aprovado?</th>
                            <th>Nome</th>
                            <th>Estado</th>
                            <th>Sigla</th>                            
                            <th>Tipos Alojamento</th>
                            <th>Tipos Modal</th>
                            <th>Tipos Local Evento</th>
                            <th>Distância Capital</th>                            
                            <th>Tem Centro Atendimento</th>                            
                            <th>Tem Rodovia Estadual</th>                            
                            <th>Tem Rodovia Federal</th>      
                            <th>Capacidade Maior Espaço</th>
                            <th>Capacidade Maior Auditório</th>
                            <th>Capacidade Maior Banquete</th>
                            <th>Capacidade Total Quartos</th>
                            <th>Capacidade Total Leitos</th>
                            <th>Capacidade Quartos Maior Hotel</th>
                            <th>Número Instituições Ensino Superior</th>
                            <th>Áreas de Expertise Referência</th>
                            <th>Cinco Principais Eventos</th>                      
                            <th>Criado em</th>
                            @if (auth()->user()->can('gerir cidades'))
                            <th>Ações</th>                                
                            @endif
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="alojamentoTipos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">   
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select multiple="multiple" id="alojamentoTiposSelect" class="form-control" name=""></select>   
                        </div>
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addAlojamentoTipos()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>                    
                    </div>                                    
                </div>
                
            </div>
        </div>
    </div>
    <div id="modalTipos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">   
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select multiple="multiple" id="modalTiposSelect" class="form-control" name=""></select>   
                        </div>
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addModalTipos()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>                    
                    </div>                                    
                </div>
                
            </div>
        </div>
    </div>
    <div id="localEventoTipos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">   
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select multiple="multiple" id="localEventoTiposSelect" class="form-control" name=""></select>   
                        </div>
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addLocalEventoTipos()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>                    
                    </div>                                    
                </div>
                
            </div>
        </div>
    </div>
    <div id="alojamentoCategorias" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">   
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select multiple="multiple" id="alojamentoCategoriasSelect" class="form-control" name=""></select>   
                        </div>
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addAlojamentoCategorias()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>                    
                    </div>                                    
                </div>
                
            </div>
        </div>
    </div>
@endsection