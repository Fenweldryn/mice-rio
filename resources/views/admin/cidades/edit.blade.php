@extends('layouts.admin')
@section('title','Editar Cidade')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Cidades</a></li>
    <li class="breadcrumb-item active">Editar Cidade</li>    
@endsection

@section('js')
    <script>        
        function inativar() {            
            $.ajax({
                type: "POST",
                url: "{{url("")}}/admin/cidades/{{$cidade->id}}/inativar",                                    
            }).always(function(response){     
                window.location = '/admin/cidades';
            });
        }   
        function ativar() {            
            $.ajax({
                type: "POST",
                url: "{{url("")}}/admin/cidades/{{$cidade->id}}/ativar",                                    
            }).always(function(response){     
                window.location = '/admin/cidades';
            });
        }        
        function reject(logID) {
            motivo = prompt("Qual o motivo da rejeição?");
            if (motivo != null) {
            $.ajax({
                    type: "POST",
                    url: "{{url("")}}/admin/aprovacoes/reject",
                    data: {"motivo": motivo, "logID" : logID},                        
                }).always(function(response){     
                    window.location = '/admin/aprovacoes';
                });
            }
        }
        function approve_(logID) {
            r = confirm("Tem certeza que deseja aprovar?");
            if(r){
                $.ajax({
                    url: "{{url('')}}/admin/aprovacoes/approve",
                    type: 'POST',
                    data: {logID: logID},
                }).always(function(response) {
                    window.location = '/admin/aprovacoes';
                }); 
            }
        }
        function previewImage() {
            var total_file=document.getElementById("fotos").files.length;
            $('#image_preview').html('');
            for(var i=0;i<total_file;i++){
                $('#image_preview').append("<img class='img-fluid' src='"+URL.createObjectURL(event.target.files[i])+"'>");
            }         
        }  
        function destroyMedia(mediaID, object){       
            c = confirm("tem certeza que deseja excluir? Esta ação é irreversível.");
            if (c) {
                $.ajax({
                    url: "{{url('')}}/admin/cidades/{{$cidade->id}}/destroyMedia",
                    type: 'POST',
                    data: {mediaID: mediaID},
                }).always(function(response) {
                    $(object).parent().remove();
                });                
            }
        }
        function definirPrincipal(mediaID, object){       
            $.ajax({
                url: "{{url('')}}/admin/cidades/{{$cidade->id}}/definirFotoPrincipal",
                type: 'POST',
                data: {mediaID: mediaID},
            }).always(function(response) {
                $("#saved_images .principal").removeClass("foto-principal");
                $("#saved_images .principal").prop('title','definir foto principal');
                $(object).prop('title','esta é a foto principal');
                $(object).addClass("foto-principal");

            });                
        }
    </script>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">       
        @isset($cidade_new)
        <h3>ANTIGO</h3>                        
        @endisset          
        <form method="POST" action="{{ url('/admin/cidades',$cidade->id) }}" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            @isset($cidade_new)
            <input type="hidden" name="approve" value="{{$logID}}">                
            @endisset
            <div class="card card-primary card-body">     
                 <div class='row'>                    
                    <label>Nome {!!isset($cidade_new['nome']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="nome" 
                        type="text" 
                        name="nome" 
                        class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" 
                        value="{{ old('nome') ?? $cidade->nome }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>
                </div>
                
                <div class='row'>                    
                    <label>CVB</label>
                    <select required class="form-control" name="entidade_id" id="entidade_id">
                        @foreach ($entidades as $entidade)
                            @if ($cidade->entidade_id == $entidade->id || old('entidade_id') == $entidade->id)
                                <option value="{{$entidade->id}}" selected>{{$entidade->nome}}</option>
                            @else    
                                <option value="{{$entidade->id}}">{{$entidade->nome}}</option>
                            @endif    
                        @endforeach                        
                    </select>    
                </div>

                <div class='row'>                    
                    <label>Descrição {!!isset($cidade_new['descricao']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <textarea 
                        id="descricao" 
                        type="text" 
                        class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" 
                        name="descricao" 
                        required 
                        rows=5 
                        {{ isset($cidade_new) ? "disabled" : '' }}
                    >{{ old('descricao') ?? $cidade->descricao }}</textarea>

                </div>

                <div class='row'>                    
                    <label>Estado {!!isset($cidade_new['estado']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="estado" 
                        type="text" 
                        name="estado" 
                        class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" 
                        value="{{ old('estado') ?? $cidade->estado }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>
                </div>

                <div class='row'>                    
                    <label>Sigla {!!isset($cidade_new['estado_sigla']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="estado_sigla" 
                        type="text" 
                        name="estado_sigla" 
                        class="form-control{{ $errors->has('estado_sigla') ? ' is-invalid' : '' }}" 
                        value="{{ old('estado_sigla') ?? $cidade->estado_sigla }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>
                </div>         

                <div class='row'>                    
                    <label>Distância da capital {!!isset($cidade_new['distancia_capital']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="distancia_capital" 
                        type="text" 
                        name="distancia_capital" 
                        class="form-control{{ $errors->has('distancia_capital') ? ' is-invalid' : '' }}" 
                        value="{{ old('distancia_capital') ?? $cidade->distancia_capital }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>
                </div>   
                
                <div class='row'>                    
                    <label>Tem centro atendimento? {!!isset($cidade_new['tem_centro_atendimento']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <select required 
                        class="form-control" 
                        name="tem_centro_atendimento" 
                        id="tem_centro_atendimento"
                        {{ isset($cidade_new) ? "disabled" : '' }}>
                        @foreach (['Não', 'Sim'] as $key => $item)
                            <option value="{{$key}}" {{(old('tem_centro_atendimento') ?? $cidade->tem_centro_atendimento) == $key ? 'selected' : ''}}>{{$item}}</option>
                        @endforeach                        
                    </select>    
                </div>   

                <div class='row'>                    
                    <label>Tem rodovia estadual? {!!isset($cidade_new['tem_rodovia_estadual']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <select required 
                        class="form-control" 
                        name="tem_rodovia_estadual" 
                        id="tem_rodovia_estadual"
                        {{ isset($cidade_new) ? "disabled" : '' }}>
                        @foreach (['Não', 'Sim'] as $key => $item)
                            <option value="{{$key}}" {{(old('tem_rodovia_estadual') ?? $cidade->tem_rodovia_estadual) == $key ? 'selected' : ''}}>{{$item}}</option>
                        @endforeach                        
                    </select>
                </div>

                <div class='row'>                    
                    <label>Tem rodovia federal? {!!isset($cidade_new['tem_rodovia_federal']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <select required 
                        class="form-control" 
                        name="tem_rodovia_federal" 
                        id="tem_rodovia_federal"
                        {{ isset($cidade_new) ? "disabled" : '' }}>
                        @foreach (['Não', 'Sim'] as $key => $item)
                            <option value="{{$key}}" {{(old('tem_rodovia_federal') ?? $cidade->tem_rodovia_federal) == $key ? 'selected' : ''}}>{{$item}}</option>
                        @endforeach                        
                    </select>    
                </div>

                <div class='row'>                    
                    <label>Capacidade do maior espaço {!!isset($cidade_new['capacidade_maior_espaco']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="capacidade_maior_espaco" 
                        type="text" 
                        name="capacidade_maior_espaco" 
                        class="form-control{{ $errors->has('capacidade_maior_espaco') ? ' is-invalid' : '' }}" 
                        value="{{ old('capacidade_maior_espaco') ?? $cidade->capacidade_maior_espaco }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>
                </div>

                <div class='row'>                    
                    <label>Capacidade do maior auditório {!!isset($cidade_new['capacidade_maior_auditorio']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="capacidade_maior_auditorio" 
                        type="text" 
                        name="capacidade_maior_auditorio" 
                        class="form-control{{ $errors->has('capacidade_maior_auditorio') ? ' is-invalid' : '' }}" 
                        value="{{ old('capacidade_maior_auditorio') ?? $cidade->capacidade_maior_auditorio }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>
                </div>
                
                <div class='row'>                    
                    <label>Capacidade do maior banquete {!!isset($cidade_new['capacidade_maior_banquete']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="capacidade_maior_banquete" 
                        type="text" 
                        name="capacidade_maior_banquete" 
                        class="form-control{{ $errors->has('capacidade_maior_banquete') ? ' is-invalid' : '' }}" 
                        value="{{ old('capacidade_maior_banquete') ?? $cidade->capacidade_maior_banquete }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>

                </div>
                
                <div class='row'>                    
                    <label>Capacidade total de quartos {!!isset($cidade_new['capacidade_total_quartos']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="capacidade_total_quartos" 
                        type="text" 
                        name="capacidade_total_quartos" 
                        class="form-control{{ $errors->has('capacidade_total_quartos') ? ' is-invalid' : '' }}" 
                        value="{{ old('capacidade_total_quartos') ?? $cidade->capacidade_total_quartos }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>

                </div>
                
                <div class='row'>                    
                    <label>Capacidade total de leitos {!!isset($cidade_new['capacidade_total_leitos']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="capacidade_total_leitos" 
                        type="text" 
                        name="capacidade_total_leitos" 
                        class="form-control{{ $errors->has('capacidade_total_leitos') ? ' is-invalid' : '' }}" 
                        value="{{ old('capacidade_total_leitos') ?? $cidade->capacidade_total_leitos }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>

                </div>
                
                <div class='row'>                    
                    <label>Capacidade de quartos do maior hotel {!!isset($cidade_new['capacidade_quartos_maior_hotel']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="capacidade_quartos_maior_hotel" 
                        type="text" 
                        name="capacidade_quartos_maior_hotel" 
                        class="form-control{{ $errors->has('capacidade_quartos_maior_hotel') ? ' is-invalid' : '' }}" 
                        value="{{ old('capacidade_quartos_maior_hotel') ?? $cidade->capacidade_quartos_maior_hotel }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>

                </div>
                
                <div class='row'>                    
                    <label>Número de instituições de ensino superior {!!isset($cidade_new['numero_instituicoes_ensino_superior']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <input id="numero_instituicoes_ensino_superior" 
                        type="text" 
                        name="numero_instituicoes_ensino_superior" 
                        class="form-control{{ $errors->has('numero_instituicoes_ensino_superior') ? ' is-invalid' : '' }}" 
                        value="{{ old('numero_instituicoes_ensino_superior') ?? $cidade->numero_instituicoes_ensino_superior }}" 
                        {{ isset($cidade_new) ? "disabled" : '' }} 
                    required>

                </div>

                <div class='row'>                    
                    <label>Áreas de expertise referência {!!isset($cidade_new['areas_expertise_referencia']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <textarea 
                        id="areas_expertise_referencia" 
                        type="text" 
                        class="form-control{{ $errors->has('areas_expertise_referencia') ? ' is-invalid' : '' }}" 
                        name="areas_expertise_referencia" 
                        required 
                        rows=5 
                        {{ isset($cidade_new) ? "disabled" : '' }}
                    >{{ old('areas_expertise_referencia') ?? $cidade->areas_expertise_referencia }}</textarea>

                </div>
                
                <div class='row'>                    
                    <label>Cinco principais eventos {!!isset($cidade_new['principais_eventos']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                    <textarea 
                        id="principais_eventos" 
                        type="text" 
                        class="form-control{{ $errors->has('principais_eventos') ? ' is-invalid' : '' }}" 
                        name="principais_eventos" 
                        required 
                        rows=5 
                        {{ isset($cidade_new) ? "disabled" : '' }}
                    >{{ old('principais_eventos') ?? $cidade->principais_eventos }}</textarea>

                </div>

                <div class='row'>                                        
                    <label>Fotos Salvas</label>                                        
                    <div id="saved_images">
                        @isset($cidade->foto_principal)
                        <div>
                            <i class="fas fa-home fa-3x principal foto-principal" data-toggle="tooltip" data-placement="top" title="esta é a foto principal"></i>
                            <i class="fas fa-trash-alt fa-3x delete" onclick="destroyMedia({{$cidade->foto_principal}},this)"></i>
                            <img class="img-fluid" data-toggle="tooltip" data-placement="top" title="excluir foto" src="{{$cidade->getMedia()->where('id',$cidade->foto_principal)->first()->getFullUrl()}}" alt="">
                        </div>            
                        @endisset        
                        @foreach ($cidade->getMedia() as $foto)
                            @if($foto->id != $cidade->foto_principal)
                            <div>
                            <i class="fas fa-home fa-3x principal {{$cidade->foto_principal == $foto->id ? 'foto-principal' : ''}}" data-toggle="tooltip" data-placement="top" title="definir foto principal" onclick="definirPrincipal({{$foto->id}},this)"></i>
                                <i class="fas fa-trash-alt fa-3x delete" onclick="destroyMedia({{$foto->id}},this)"></i>
                                <img class="img-fluid" data-toggle="tooltip" data-placement="top" title="excluir foto" src="{{$foto->getFullUrl()}}" alt="">
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>   

                <div class='row'>                                        
                    <label>Fotos</label>
                    @if(!isset($cidade_new))
                    <input type="file" name="fotos[]" id='fotos' class='form-control' multiple="multiple" onchange="previewImage();">
                    @endif
                    <div id="image_preview"></div>                        

                </div>   
                
                <div class="row mt-4">
                    @if(!isset($cidade_new))
                        @if (auth()->user()->hasRole("admin"))
                        <button type="submit" onclick="$.LoadingOverlay('show')" class="btn btn-success">
                            <i class="fas fa-check"></i> Salvar edição
                        </button>                    
                                                
                        @if ($cidade->is_active)
                        <button type="button" onclick="inativar()" class="ml-2 btn btn-warning">
                            <i class="fas fa-lock"></i> Inativar
                        </button>                                                
                        @else
                        <button type="button" onclick="ativar()" class="ml-2 btn btn-info">
                            <i class="fas fa-lock-open"></i> Ativar
                        </button>                    
                        @endif


                        @else
                        <button type="submit" class="btn btn-success">
                            <i class="fas fa-check"></i> Enviar para Aprovação
                        </button>                    
                        @endif
                    @endif
                </div>
            </div>
        </form>
    </div>
    @isset($cidade_new)
    <div class="col-md-6">                 
        <h3>NOVO</h3>            
        @isset($cidade_new)
        <input type="hidden" name="approve" value="{{$logID}}">                
        @endisset
        <div class="card card-primary card-body">       
            <div class='row'>                    
                <label>Nome {!!isset($cidade_new['nome']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input id="nome" 
                    type="text" 
                    name="nome" 
                    class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['nome'] ?? $cidade->nome }}" 
                    disabled 
                required>
            </div>

            <div class='row'>                    
                <label>Descrição {!!isset($cidade_new['descricao']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <textarea 
                     
                    type="text" 
                    class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" 
                    name="descricao" 
                    required 
                    rows=5 
                    disabled
                >{{$cidade_new['descricao'] ?? $cidade->descricao}}</textarea>

            </div>

            <div class='row'>                    
                <label>Estado {!!isset($cidade_new['estado']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input  
                    type="text" 
                    name="estado" 
                    class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['estado'] ?? $cidade->estado }}" 
                    disabled 
                required>
            </div>

            <div class='row'>                    
                <label>Sigla {!!isset($cidade_new['estado_sigla']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input  
                    type="text" 
                    name="estado_sigla" 
                    class="form-control{{ $errors->has('estado_sigla') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['estado_sigla'] ?? $cidade->estado_sigla }}" 
                    disabled 
                required>
            </div>         

            <div class='row'>                    
                <label>Distância da capital {!!isset($cidade_new['distancia_capital']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input  
                    type="text" 
                    name="distancia_capital" 
                    class="form-control{{ $errors->has('distancia_capital') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['distancia_capital'] ?? $cidade->distancia_capital }}" 
                    disabled 
                required>
            </div>   
            
            <div class='row'>                    
                <label>Tem centro atendimento? {!!isset($cidade_new['tem_centro_atendimento']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <select required 
                    class="form-control" 
                    name="tem_centro_atendimento" 
                    
                    disabled>
                    @foreach (['Não', 'Sim'] as $key => $item)
                        <option value="{{$key}}" {{($cidade_new['tem_centro_atendimento'] ?? $cidade->tem_centro_atendimento) == $key ? 'selected' : ''}}>{{$item}}</option>
                    @endforeach                        
                </select>    
            </div>   

            <div class='row'>                    
                <label>Tem rodovia estadual? {!!isset($cidade_new['tem_rodovia_estadual']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <select required 
                    class="form-control" 
                    name="tem_rodovia_estadual" 
                    
                    disabled>
                    @foreach (['Não', 'Sim'] as $key => $item)
                        <option value="{{$key}}" {{($cidade_new['tem_rodovia_estadual'] ?? $cidade->tem_rodovia_estadual) == $key ? 'selected' : ''}}>{{$item}}</option>
                    @endforeach                        
                </select>
            </div>

            <div class='row'>                    
                <label>Tem rodovia federal? {!!isset($cidade_new['tem_rodovia_federal']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <select required 
                    class="form-control" 
                    name="tem_rodovia_federal" 
                    
                    disabled>
                    @foreach (['Não', 'Sim'] as $key => $item)
                        <option value="{{$key}}" {{($cidade_new['tem_rodovia_federal'] ?? $cidade->tem_rodovia_federal) == $key ? 'selected' : ''}}>{{$item}}</option>
                    @endforeach                        
                </select>    
            </div>

            <div class='row'>                    
                <label>Capacidade do maior espaço {!!isset($cidade_new['capacidade_maior_espaco']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input  
                    type="text" 
                    name="capacidade_maior_espaco" 
                    class="form-control{{ $errors->has('capacidade_maior_espaco') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['capacidade_maior_espaco'] ?? $cidade->capacidade_maior_espaco }}" 
                    disabled 
                required>
            </div>

            <div class='row'>                    
                <label>Capacidade do maior auditório {!!isset($cidade_new['capacidade_maior_auditorio']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input  
                    type="text" 
                    name="capacidade_maior_auditorio" 
                    class="form-control{{ $errors->has('capacidade_maior_auditorio') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['capacidade_maior_auditorio'] ?? $cidade->capacidade_maior_auditorio }}" 
                    disabled 
                required>
            </div>
            
            <div class='row'>                    
                <label>Capacidade do maior banquete {!!isset($cidade_new['capacidade_maior_banquete']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input  
                    type="text" 
                    name="capacidade_maior_banquete" 
                    class="form-control{{ $errors->has('capacidade_maior_banquete') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['capacidade_maior_banquete'] ?? $cidade->capacidade_maior_banquete }}" 
                    disabled 
                required>

            </div>
            
            <div class='row'>                    
                <label>Capacidade total de quartos {!!isset($cidade_new['capacidade_total_quartos']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input  
                    type="text" 
                    name="capacidade_total_quartos" 
                    class="form-control{{ $errors->has('capacidade_total_quartos') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['capacidade_total_quartos'] ?? $cidade->capacidade_total_quartos }}" 
                    disabled 
                required>

            </div>
            
            <div class='row'>                    
                <label>Capacidade total de leitos {!!isset($cidade_new['capacidade_total_leitos']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input  
                    type="text" 
                    name="capacidade_total_leitos" 
                    class="form-control{{ $errors->has('capacidade_total_leitos') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['capacidade_total_leitos'] ?? $cidade->capacidade_total_leitos }}" 
                    disabled 
                required>

            </div>
            
            <div class='row'>                    
                <label>Capacidade de quartos do maior hotel {!!isset($cidade_new['capacidade_quartos_maior_hotel']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input  
                    type="text" 
                    name="capacidade_quartos_maior_hotel" 
                    class="form-control{{ $errors->has('capacidade_quartos_maior_hotel') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['capacidade_quartos_maior_hotel'] ?? $cidade->capacidade_quartos_maior_hotel }}" 
                    disabled 
                required>

            </div>
            
            <div class='row'>                    
                <label>Número de instituições de ensino superior {!!isset($cidade_new['numero_instituicoes_ensino_superior']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <input  
                    type="text" 
                    name="numero_instituicoes_ensino_superior" 
                    class="form-control{{ $errors->has('numero_instituicoes_ensino_superior') ? ' is-invalid' : '' }}" 
                    value="{{ $cidade_new['numero_instituicoes_ensino_superior'] ?? $cidade->numero_instituicoes_ensino_superior }}" 
                    disabled 
                required>

            </div>

            <div class='row'>                    
                <label>Áreas de expertise referência {!!isset($cidade_new['areas_expertise_referencia']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <textarea 
                     
                    type="text" 
                    class="form-control{{ $errors->has('areas_expertise_referencia') ? ' is-invalid' : '' }}" 
                    name="areas_expertise_referencia" 
                    required 
                    rows=5 
                    disabled
                >{{ $cidade_new['areas_expertise_referencia'] ?? $cidade->areas_expertise_referencia }}</textarea>

            </div>
            
            <div class='row'>                    
                <label>Cinco principais eventos {!!isset($cidade_new['principais_eventos']) ? '<span class="badge badge-primary right">CAMPO MODIFICADO</span>' : '' !!}</label>
                <textarea 
                     
                    type="text" 
                    class="form-control{{ $errors->has('principais_eventos') ? ' is-invalid' : '' }}" 
                    name="principais_eventos" 
                    required 
                    rows=5 
                    disabled
                >{{ $cidade_new['principais_eventos'] ?? $cidade->principais_eventos }}</textarea>

            </div>

            <div class='row'>                                        
                <label>Fotos Salvas</label>                                        
                <div >                        
                    @foreach ($cidade->getMedia() as $foto)
                        <span>
                            <i class="fas fa-trash-alt fa-3x" onclick="destroyMedia({{$foto->id}},this)"></i>
                            <img class="img-fluid" src="{{$foto->getFullUrl()}}" alt="">
                        </span>
                    @endforeach
                </div>
            </div>   

            <div class='row'>                                        
                <label>Fotos</label>
                <div></div>
            </div>   

           <div class="row mt-4">
                @if(isset($cidade_new))
                <button type="button" onclick="approve_({{$logID}})" class="btn btn-success mr-3">
                    <i class="fas fa-check"></i> Aprovar edição
                </button>                               
                <button onclick="reject({{$logID}})" type="button" class="btn btn-danger">
                    <i class="fas fa-ban"></i> Rejeitar edição
                </button>                                                            
                @endif
            </div>
        </div>
    </div>       
    @endisset
</div>
@endsection