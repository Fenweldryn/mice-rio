@extends('layouts.admin')
@section('title','Cadastrar Cidade')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Cidades</a></li>
    <li class="breadcrumb-item active">Cadastrar Cidade</li>  
@endsection

@section('js')
    <script>        
        function previewImage() {
            var total_file=document.getElementById("fotos").files.length;
            $('#image_preview').html('');
            for(var i=0;i<total_file;i++){
                $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
            }
        }  
    </script>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/cidades') }}" enctype="multipart/form-data">            
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">     
                 <div class='row'>                    
                    <label>Nome</label>
                    <input id="nome" type="text" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" name="nome" required value="{{old('nome')}}">
                </div>

                <div class='row'>                    
                    <label>CVB</label>
                    <select required class="form-control" name="entidade_id" id="entidade_id">
                        <option value=""></option>
                        @foreach ($entidades as $entidade)
                           <option value="{{$entidade->id}}">{{$entidade->nome}}</option>
                        @endforeach                        
                    </select>          
                </div>  

                <div class='row'>                    
                    <label>Descrição</label>
                    <textarea id="descricao" type="text" class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" name="descricao" required rows=5>{{old("descricao")}}</textarea>
                </div>

                <div class='row'>                    
                    <label>Estado</label>
                    <input id="estado" type="text" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado" required value="{{old('estado')}}">
                </div>

                <div class='row'>                    
                    <label>Sigla</label>
                    <input id="estado_sigla" type="text" class="form-control{{ $errors->has('estado_sigla') ? ' is-invalid' : '' }}" name="estado_sigla" required value="{{old('estado_sigla')}}">                
                </div>          
                
                <div class='row'>                    
                    <label>Distância Capital (em km, somente número)</label>
                    <input id="distancia_capital" type="text" class="form-control{{ $errors->has('distancia_capital') ? ' is-invalid' : '' }}" name="distancia_capital" required value="{{old('distancia_capital')}}">
                </div>   

                <div class='row'>                    
                    <label>Tem Centro de Atendimento?</label>
                    <select name="tem_centro_atendimento" class='form-control' id="" required>
                        <option value=""></option>
                        <option value="1">Sim</option>
                        <option value="0">Não</option>
                    </select>         
                </div>   

                <div class='row'>                    
                    <label>Tem Rodovia Estadual?</label>
                    <select name="tem_rodovia_estadual" class='form-control' id="" required>
                        <option value=""></option>
                        <option value="1">Sim</option>
                        <option value="0">Não</option>
                    </select>     
                </div>   

                <div class='row'>                    
                    <label>Tem Rodovia Federal?</label>
                    <select name="tem_rodovia_federal" class='form-control' id="" required>
                        <option value=""></option>
                        <option value="1">Sim</option>
                        <option value="0">Não</option>
                    </select>         
                </div>   

                <div class='row'>                    
                    <label>Capacidade Maior Espaço</label>                        
                    <input id="capacidade_maior_espaco" type="number" class="form-control{{ $errors->has('capacidade_maior_espaco') ? ' is-invalid' : '' }}" name="capacidade_maior_espaco" value='{{old("capacidade_maior_espaco")}}' required value="{{old('capacidade_maior_espaco')}}">         
                </div>

                <div class='row'>                    
                    <label>Capacidade Maior Auditório</label>                    
                    <input id="capacidade_maior_auditorio" type="number" class="form-control{{ $errors->has('capacidade_maior_auditorio') ? ' is-invalid' : '' }}" name="capacidade_maior_auditorio" value='{{old("capacidade_maior_auditorio")}}' required value="{{old('capacidade_maior_auditorio')}}">
                </div>

                <div class='row'>                    
                    <label>Capacidade Maior Banquete</label>                    
                    <input id="capacidade_maior_banquete" type="number" class="form-control{{ $errors->has('capacidade_maior_banquete') ? ' is-invalid' : '' }}" name="capacidade_maior_banquete" value='{{old("capacidade_maior_banquete")}}' required value="{{old('capacidade_maior_banquete')}}">
                </div>

                <div class='row'>                    
                    <label>Capacidade Total Quartos</label>                    
                    <input id="capacidade_total_quartos" type="number" class="form-control{{ $errors->has('capacidade_total_quartos') ? ' is-invalid' : '' }}" name="capacidade_total_quartos" value='{{old("capacidade_total_quartos")}}' required value="{{old('capacidade_total_quartos')}}">           
                </div>

                <div class='row'>                    
                    <label>Capacidade Total Leitos</label>                    
                    <input id="capacidade_total_leitos" type="number" class="form-control{{ $errors->has('capacidade_total_leitos') ? ' is-invalid' : '' }}" name="capacidade_total_leitos" value='{{old("capacidade_total_leitos")}}' required  value="{{old('capacidade_total_leitos')}}">
                </div>

                <div class='row'>                    
                    <label>Capacidade Quartos Maior Hotel</label>                    
                    <input id="capacidade_quartos_maior_hotel" type="number" class="form-control{{ $errors->has('capacidade_quartos_maior_hotel') ? ' is-invalid' : '' }}" name="capacidade_quartos_maior_hotel" value='{{old("capacidade_quartos_maior_hotel")}}' required value="{{old('capacidade_quartos_maior_hotel')}}">
                </div>
                
                <div class='row'>                    
                    <label>Número Instituições Ensino Superior</label>                      
                    <input id="numero_instituicoes_ensino_superior" type="number" class="form-control{{ $errors->has('numero_instituicoes_ensino_superior') ? ' is-invalid' : '' }}" name="numero_instituicoes_ensino_superior" value='{{old("numero_instituicoes_ensino_superior")}}' required value="{{old('numero_instituicoes_ensino_superior')}}">
                </div>

                <div class='row'>                    
                    <label>Áreas de Expertise Referência</label>
                    <textarea id="areas_expertise_referencia" type="text" class="form-control{{ $errors->has('areas_expertise_referencia') ? ' is-invalid' : '' }}" name="areas_expertise_referencia" required rows=5>{{old("areas_expertise_referencia")}}</textarea>
                </div>

                <div class='row'>                    
                    <label>Cinco Principais Eventos</label>                                        
                    <textarea id="principais_eventos" type="text" class="form-control{{ $errors->has('principais_eventos') ? ' is-invalid' : '' }}" name="principais_eventos" required rows=5>{{old("principais_eventos")}}</textarea>
                </div>

                <div class='row'>                    
                    <label>Fotos</label>
                    <input type="file" name="fotos[]" id='fotos' class='form-control' multiple onchange="previewImage();">
                    <div id="image_preview"></div>

                    @if ($errors->has('fotos'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('fotos') }}</strong>
                        </span>
                    @endif                    
                </div>   
                
                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Cadastrar </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection