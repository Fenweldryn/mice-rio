@extends('layouts.admin')
@section('title','Editar Modal Tipos')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Modals</a></li>
    <li class="breadcrumb-item active">Editar Modal Tipos</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/modaltipos',$modaltipo->id) }}">
            @method('PATCH')
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class='row'>                    
                    <label>Nome</label>
                    <input id="nome" type="text" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" name="nome" value="{{ old('nome') === null ? $modaltipo->nome : old('nome') === null ? $modaltipo->nome : old('nome') }}" required >

                    @if ($errors->has('nome'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nome') }}</strong>
                        </span>
                    @endif                    
                </div> 

                <div class='row'>                    
                    <label>Tipo</label>
                    <select name="tipo" class='form-control' id="" required>
                        <option value="local">local</option>
                        <option value="acesso">acesso</option>
                    </select>
                    
                    @if ($errors->has('tipo'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tipo') }}</strong>
                        </span>
                    @endif                    
                </div>
                                
                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Salvar edição
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection