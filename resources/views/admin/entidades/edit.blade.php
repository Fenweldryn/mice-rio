@extends('layouts.admin')
@section('title','Editar Entidade')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Entidades</a></li>
    <li class="breadcrumb-item active">Editar Entidade</li>
@endsection

@section('js')
    <script>
        function copiarDadosPresidente(destino, checkbox) {
            if($(checkbox).prop('checked')) {
                $("#nome_"+destino+"").val($("#nome_presidente").val());                
                $("#email_"+destino+"").val($("#email_presidente").val());                
                $("#telefone_fixo_"+destino+"").val($("#telefone_fixo_presidente").val());                
                $("#celular_"+destino+"").val($("#celular_presidente").val());                
                ("#ddd_telefone_fixo_"+destino+"").val($("#ddd_telefone_fixo_presidente").val());                
                $("#ddd_celular_"+destino+"").val($("#ddd_celular_presidente").val());  
                $("[id$='_"+destino+"'").attr('readonly',true);
            } else {
                $("[id$='_"+destino+"'").val("");
                $("[id$='_"+destino+"'").removeAttr('readonly');
            }
        }
        function buscarDadosCnpj() {
            if ($("#cnpj").val()) {
                cnpj = $("#cnpj").val().match(/\d+/g).join('');
                $.ajax({
                    type: "get",
                    contentType: "application/json",
                    dataType: "jsonp",
                    url: "https://www.receitaws.com.br/v1/cnpj/"+cnpj+"?callback=processarDadosCnpj"
                });
            }
        }
        function processarDadosCnpj(dados) {
            console.log(dados);
            $("#nome").val(dados.nome);
            $("#nome_fantasia").val(dados.fantasia);
            $("#endereco").val(dados.logradouro+", "+dados.numero+", "+dados.bairro+" - "+dados.municipio+"-"+dados.uf);
            $("#telefone").val(dados.telefone);
            
        }
        $(document).ready(function () {
            financeiro = {{@old('presidentefinanceiro') ? 1 : 0}};
            gerente = {{@old('presidentegerente') ? 1 : 0}};

            if(financeiro) {
                $("#presidentefinanceiro").click();
            }
            if(gerente) {
                $("#presidentegerente").click();
            }
        });

        function previewImage() {
            var total_file=document.getElementById("foto").files.length;
            $('#image_preview').html('');
            for(var i=0;i<total_file;i++){
                $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
            }
        }  
    </script>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/entidades',$entidade->id) }}" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">     
                <div class='row'>                    
                    <label>CNPJ</label>
                    <input id="cnpj" type="text" class="form-control{{ $errors->has('cnpj') ? ' is-invalid' : '' }}" name="cnpj" value="{{ old('cnpj') ?? $entidade->cnpj }}" required >
                </div>

                <div class='row'>                    
                    <label>Nome</label>
                    <input id="nome" type="text" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" name="nome" value="{{ old('nome') ?? $entidade->nome }}" required >
                </div>

                 <div class='row'>                    
                    <label>Nome Fantasia</label>
                    <input id="nome_fantasia" value="{{ old('nome_fantasia') ?? $entidade->nome_fantasia }}" type="text" class="form-control{{ $errors->has('nome_fantasia') ? ' is-invalid' : '' }}" name="nome_fantasia" required >
                </div>

                <div class='row'>                    
                    <label>Endereço</label>
                    <input id="endereco" type="text" class="form-control{{ $errors->has('endereco') ? ' is-invalid' : '' }}" name="endereco" value="{{ old('endereco') ?? $entidade->endereco }}" required >
                </div>

                <div class='row'>                    
                    <label>Email</label>
                    <input id="email" value="{{ old('email') ?? $entidade->email }}" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required >
                </div>     

                <div class='row'>  
                    <div class="col p-0">
                        <label>Telefone</label>
                        <div class="row">
                            <div class="col-3 col-md-1">
                                <input id="ddd_telefone" value="{{ old('ddd_telefone') ?? $entidade->ddd_telefone }}" type="text" class="form-control{{ $errors->has('ddd_telefone') ? ' is-invalid' : '' }}" name="ddd_telefone" required placeholder="ddd">
                            </div>
                            <div class="col">
                                <input id="telefone" value="{{ old('telefone') ?? $entidade->telefone }}" type="text" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" name="telefone" required >
                            </div>
                        </div>
                    </div>
                </div>     

                <div class='row'>  
                    <div class="col p-0">
                        <label>Whatsapp</label>
                        <div class="row">
                            <div class="col-3 col-md-1">
                                <input id="ddd_whatsapp" value="{{ old('ddd_whatsapp') ?? $entidade->ddd_whatsapp }}" type="text" class="form-control{{ $errors->has('ddd_whatsapp') ? ' is-invalid' : '' }}" name="ddd_whatsapp" required placeholder="ddd">
                            </div>
                            <div class="col">
                                <input id="whatsapp" value="{{ old('whatsapp') ?? $entidade->whatsapp }}" type="text" class="form-control{{ $errors->has('whatsapp') ? ' is-invalid' : '' }}" name="whatsapp" required >
                            </div>
                        </div>
                    </div>
                </div>       

                <div class='row'>                    
                    <label>Site</label>
                    <input id="site" value="{{ old('site') ?? $entidade->site }}" type="text" class="form-control{{ $errors->has('site') ? ' is-invalid' : '' }}" name="site" required >
                </div>     

                <div class='row'>                    
                    <label>Facebook</label>
                    <input id="facebook" value="{{ old('facebook') ?? $entidade->facebook }}" type="text" class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }}" name="facebook" required >
                </div> 

                <div class='row'>                    
                    <label>Instagram</label>
                    <input id="instagram" value="{{ old('instagram') ?? $entidade->instagram }}" type="text" class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }}" name="instagram" required >
                </div>     

                <div class='row'>                    
                    <label>Twitter</label>
                    <input id="twitter" value="{{ old('twitter') ?? $entidade->twitter }}" type="text" class="form-control{{ $errors->has('twitter') ? ' is-invalid' : '' }}" name="twitter" required >
                </div>     

                <div class='row'>                    
                    <label>LinkedIn</label>
                    <input id="linkedin" value="{{ old('linkedin') ?? $entidade->linkedin }}" type="text" class="form-control{{ $errors->has('linkedin') ? ' is-invalid' : '' }}" name="linkedin" required >
                </div>    

                <br>
                <br>
                
                <div class='row'>                    
                    <label>Nome Presidente</label>
                    <input id="nome_presidente" value="{{ old('nome_presidente') ?? $entidade->nome_presidente }}" type="text" class="form-control{{ $errors->has('nome_presidente') ? ' is-invalid' : '' }}" name="nome_presidente" required >
                </div> 
                <div class='row'>                    
                    <label>Email Presidente</label>
                    <input id="email_presidente" value="{{ old('email_presidente') ?? $entidade->email_presidente }}" type="text" class="form-control{{ $errors->has('email_presidente') ? ' is-invalid' : '' }}" name="email_presidente" required >
                </div> 
                <div class='row'>  
                    <div class="col p-0">
                        <label>Telefone Fixo Presidente</label>
                        <div class="row">
                            <div class="col-3 col-md-1">
                                <input id="ddd_telefone_fixo_presidente" value="{{ old('ddd_telefone_fixo_presidente') ?? $entidade->ddd_telefone_fixo_presidente }}" type="text" class="form-control{{ $errors->has('ddd_telefone_fixo_presidente') ? ' is-invalid' : '' }}" name="ddd_telefone_fixo_presidente" required placeholder="ddd">
                            </div>
                            <div class="col">
                                <input id="telefone_fixo_presidente" value="{{ old('telefone_fixo_presidente') ?? $entidade->telefone_fixo_presidente }}" type="text" class="form-control{{ $errors->has('telefone_fixo_presidente') ? ' is-invalid' : '' }}" name="telefone_fixo_presidente" required >
                            </div>
                        </div>
                    </div>
                </div>               
                <div class='row'>  
                    <div class="col p-0">
                        <label>Celular Presidente</label>
                        <div class="row">
                            <div class="col-3 col-md-1">
                                <input id="ddd_celular_presidente" value="{{ old('ddd_celular_presidente') ?? $entidade->ddd_celular_presidente }}" type="text" class="form-control{{ $errors->has('ddd_celular_presidente') ? ' is-invalid' : '' }}" name="ddd_celular_presidente" required placeholder="ddd">
                            </div>
                            <div class="col">
                                <input id="celular_presidente" value="{{ old('celular_presidente') ?? $entidade->celular_presidente }}" type="text" class="form-control{{ $errors->has('celular_presidente') ? ' is-invalid' : '' }}" name="celular_presidente" required >
                            </div>
                        </div>
                    </div>
                </div>               

                <br>
                <br>

                <div class='row'>                    
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="presidentefinanceiro" name="presidentefinanceiro" onchange="copiarDadosPresidente('financeiro',this)">
                        <label class="custom-control-label" for="presidentefinanceiro">Presidente também é o financeiro</label>
                    </div>
                </div>
                <div class='row'>                    
                    <label>Nome Financeiro</label>
                    <input id="nome_financeiro" value="{{ old('nome_financeiro') ?? $entidade->nome_financeiro }}" type="text" class="form-control{{ $errors->has('nome_financeiro') ? ' is-invalid' : '' }}" name="nome_financeiro" required >
                </div> 
                <div class='row'>                    
                    <label>Email Financeiro</label>
                    <input id="email_financeiro" value="{{ old('email_financeiro') ?? $entidade->email_financeiro }}" type="text" class="form-control{{ $errors->has('email_financeiro') ? ' is-invalid' : '' }}" name="email_financeiro" required >
                </div> 
                <div class='row'>  
                    <div class="col p-0">
                        <label>Telefone Fixo Financeiro</label>
                        <div class="row">
                            <div class="col-3 col-md-1">
                                <input id="ddd_telefone_fixo_financeiro" value="{{ old('ddd_telefone_fixo_financeiro') ?? $entidade->ddd_telefone_fixo_financeiro }}" type="text" class="form-control{{ $errors->has('ddd_telefone_fixo_financeiro') ? ' is-invalid' : '' }}" name="ddd_telefone_fixo_financeiro" required placeholder="ddd">
                            </div>
                            <div class="col">
                                <input id="telefone_fixo_financeiro" value="{{ old('telefone_fixo_financeiro') ?? $entidade->telefone_fixo_financeiro }}" type="text" class="form-control{{ $errors->has('telefone_fixo_financeiro') ? ' is-invalid' : '' }}" name="telefone_fixo_financeiro" required >
                            </div>
                        </div>
                    </div>
                </div>   
                <div class='row'>  
                    <div class="col p-0">
                        <label>Telefone Celular Financeiro</label>
                        <div class="row">
                            <div class="col-3 col-md-1">
                                <input id="ddd_celular_financeiro" value="{{ old('ddd_celular_financeiro') ?? $entidade->ddd_celular_financeiro }}" type="text" class="form-control{{ $errors->has('ddd_celular_financeiro') ? ' is-invalid' : '' }}" name="ddd_celular_financeiro" required placeholder="ddd">
                            </div>
                            <div class="col">
                                <input id="celular_financeiro" value="{{ old('celular_financeiro') ?? $entidade->celular_financeiro }}" type="text" class="form-control{{ $errors->has('celular_financeiro') ? ' is-invalid' : '' }}" name="celular_financeiro" required >
                            </div>
                        </div>
                    </div>
                </div>   

                <br>
                <br>

                <div class='row'>                    
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="presidentegerente" name="presidentegerente" onchange="copiarDadosPresidente('gerente',this)">
                        <label class="custom-control-label" for="presidentegerente">Presidente também é o gerente</label>
                    </div>
                </div>
                <div class='row'>                    
                    <label>Nome Gerente</label>
                    <input id="nome_gerente" value="{{ old('nome_gerente') ?? $entidade->nome_gerente }}" type="text" class="form-control{{ $errors->has('nome_gerente') ? ' is-invalid' : '' }}" name="nome_gerente" required >
                </div> 
                <div class='row'>                    
                    <label>Email Gerente</label>
                    <input id="email_gerente" value="{{ old('email_gerente') ?? $entidade->email_gerente }}" type="text" class="form-control{{ $errors->has('email_gerente') ? ' is-invalid' : '' }}" name="email_gerente" required >
                </div> 
                <div class='row'>  
                    <div class="col p-0">
                        <label>Telefone Fixo Gerente</label>
                        <div class="row">
                            <div class="col-3 col-md-1">
                                <input id="ddd_telefone_fixo_gerente" value="{{ old('ddd_telefone_fixo_gerente') ?? $entidade->ddd_telefone_fixo_gerente }}" type="text" class="form-control{{ $errors->has('ddd_telefone_fixo_gerente') ? ' is-invalid' : '' }}" name="ddd_telefone_fixo_gerente" required placeholder="ddd">
                            </div>
                            <div class="col">
                                <input id="telefone_fixo_gerente" value="{{ old('telefone_fixo_gerente') ?? $entidade->telefone_fixo_gerente }}" type="text" class="form-control{{ $errors->has('telefone_fixo_gerente') ? ' is-invalid' : '' }}" name="telefone_fixo_gerente" required >
                            </div>
                        </div>
                    </div>
                </div>   
                <div class='row'>  
                    <div class="col p-0">
                        <label>Celular Gerente</label>
                        <div class="row">
                            <div class="col-3 col-md-1">
                                <input id="ddd_celular_gerente" value="{{ old('ddd_celular_gerente') ?? $entidade->ddd_celular_gerente }}" type="text" class="form-control{{ $errors->has('ddd_celular_gerente') ? ' is-invalid' : '' }}" name="ddd_celular_gerente" required placeholder="ddd">
                            </div>
                            <div class="col">
                                <input id="celular_gerente" value="{{ old('celular_gerente') ?? $entidade->celular_gerente }}" type="text" class="form-control{{ $errors->has('celular_gerente') ? ' is-invalid' : '' }}" name="celular_gerente" required >
                            </div>
                        </div>
                    </div>
                </div>   

                <br>
                <div class='row'>  
                    <div class="col p-0">
                        <label>Cidade principal da região (opcional)</label>
                        <legend style="font-size: 1em">Mostrar os números de locais para evento somente da cidade selecionada para todas as cidades da região</legend>
                        <div class="row">                           
                            <div class="col">
                                <select id="my-select" class="form-control" name="regiao_cidade_principal_id">
                                    <option></option>
                                    @foreach ($entidade->cidades as $cidade)
                                        <option value="{{ $cidade->id }}">{{ $cidade->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <br>

                <div class='row'>                                        
                    <label>Logo Salva</label>                                        
                    <div id="saved_images">                           
                        @foreach ($entidade->getMedia() as $foto)
                            <div>
                                <i class="fas fa-trash-alt fa-3x" onclick="destroyMedia({{$foto->id}},this)"></i>
                                <img class="img-fluid" data-toggle="tooltip" data-placement="top" title="excluir foto" src="{{$foto->getFullUrl()}}" alt="">
                            </div>
                        @endforeach
                    </div>
                </div>   

                <div class='row'>                    
                    <label>Enviar nova logo</label>
                    <input type="file" name="foto" id='foto' class='form-control' onchange="previewImage();">
                    <div id="image_preview"></div>

                    @if ($errors->has('fotos'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('fotos') }}</strong>
                        </span>
                    @endif                    
                </div>   

                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Salvar edição
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection