@extends('layouts.admin')

@section('title')
    Entidades
    @if(auth()->user()->hasRole('admin'))
    <a href="{{route('entidades.create')}}" class="btn btn-success"><i class="fas fa-plus"></i> Cadastrar</a>
    @endif
@endsection

@section('css')
    <link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>    
    <script>
    idEntidade = 0;
    tituloModal = '';
    
    $('#usuarios').on('hide.bs.modal', function (e) {
        $('#userSelect')[0].sumo.unSelectAll();
    });
    // $('#cidades').on('hide.bs.modal', function (e) {
    //     $('#cidadeSelect')[0].sumo.unSelectAll();
    // });
    $(".content-wrapper").LoadingOverlay("show");
    $(document).ready(function() {  
        loadUsers();   
        loadCidades();   
        $('table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('admin/entidades/datatable') }}",               
            responsive: true,           
            columns: [
                {data: 'id', responsivePriority: 1},
                {data: 'nome', responsivePriority: 1},
                {data: 'nome_fantasia', responsivePriority: 1},
                {data: 'usuarios', responsivePriority: 1},
                {data: 'cidades', responsivePriority: 1},
                {data: 'endereco'},
                {data: 'cnpj'},                
                {data: 'email'},                
                {data: 'telefone'},                
                {data: 'whatsapp'},                
                {data: 'site'},                
                {data: 'facebook'},                
                {data: 'instagram'},                
                {data: 'twitter'},                
                {data: 'linkedin'},                
                {data: 'nome_presidente'},                
                {data: 'email_presidente'},                
                {data: 'telefone_fixo_presidente'},                
                {data: 'celular_presidente'},                
                {data: 'nome_financeiro'},                
                {data: 'email_financeiro'},                
                {data: 'telefone_fixo_financeiro'},                
                {data: 'celular_financeiro'},                
                {data: 'nome_gerente'},                
                {data: 'email_gerente'},                
                {data: 'telefone_fixo_gerente'},                
                {data: 'celular_gerente'},                
                {data: 'created_at'},
                {data: 'action', orderable: false, searchable: false, responsivePriority: 1},                
            ],
            language: {
                url: "{{ asset('plugins/datatables/pt-br.json') }}"
            },
            initComplete: function(){
                $("table").show();
                $(".content-wrapper").LoadingOverlay("hide");
            }
        });
    });

    function ajaxThenCallModal(id, titulo) {        
        conteudo = "";        
        tituloModal = titulo;
        $("#usuarios .modal-body").LoadingOverlay("show");
        $.ajax("{{url('')}}/admin/entidades/"+id+"/usuarios").always(function(response) {
            if (response) {
                conteudo = "<table class='table table-bordered'><thead><th>Nome</th><th>Email</th><th>Ações</th></thead><tbody>";
                $.each(response, function (index, value) { 
                    conteudo += '<tr><td>'+value.name+'</td><td>'+value.email+'</td>'+   
                    '<td><button class="btn btn-danger" onclick="removeUser('+id+','+value.id+')"><i class="fas fa-trash"></i> </button></td></tr>';
                });                    
                conteudo += "</tbody></table>";            
            } else {
                conteudo = "Nenhum usuário encontrado.";
            }
            $('#usuarios .modal-title').html('Usuários de ' + titulo);
            $('#usuarios .modal-body').html(conteudo);              
            idEntidade = id;    
            $('#usuarios table').DataTable({
                destroy: true,
                language: {
                    url: "{{ asset('plugins/datatables/pt-br.json') }}"
                }
            });
            $("#usuarios .modal-body").LoadingOverlay("hide");   
            $('#usuarios').modal('show');              
        });
    }

    function ajaxThenCallModalCidades(id, titulo) {        
        conteudo = "";        
        tituloModal = titulo;
        $("#cidades .modal-body").LoadingOverlay("show");
        $.ajax("{{url('')}}/admin/entidades/"+id+"/cidades").always(function(response) {            
            if (response) {
                conteudo = "<table class='table table-bordered'><thead><th>Nome</th><th>Ações</th></thead><tbody>";
                $.each(response, function (index, value) { 
                    conteudo += '<tr><td>'+value.nome+'</td>'+   
                    '<td><button class="btn btn-danger" onclick="removeCidade('+id+','+value.id+')"><i class="fas fa-trash"></i> </button></td></tr>';
                });                    
                conteudo += "</tbody></table>";            
            } else {
                conteudo = "Nenhuma cidade encontrada.";
            }
            $('#cidades .modal-title').html('Cidades geridas por ' + titulo);
            $('#cidades .modal-body').html(conteudo);              
            idEntidade = id;    
            $('#cidades table').DataTable({
                destroy: true,
                language: {
                    url: "{{ asset('plugins/datatables/pt-br.json') }}"
                }
            });
            $("#cidades .modal-body").LoadingOverlay("hide");   
            $('#cidades').modal('show');              
        });
    }

    function removeUser(entidadeID, userID) {
        r = confirm("Tem certeza que deseja remover?");
        if (r) {
            $.ajax({
                type: "DELETE",
                url: "{{url("")}}/admin/entidades/"+entidadeID+"/usuarios",
                data: {"userID": userID},                        
            }).always(function(response){
                ajaxThenCallModal(entidadeID, tituloModal);        
            });            
        }
    }
    function loadUsers() {
        $.ajax("{{url('admin/users/list')}}").always(function(response) {            
            usuarios = "";
            $.each(response, function (index, value) { 
                usuarios += "<option value="+value.id+">"+value.name+" - "+value.email+"</option>";
            });            
            $('.modal .modal-footer #userSelect').html(usuarios);
            $('#userSelect').SumoSelect({placeholder:"Selecione uma ou várias opções"});            
        });   
    }
    function addUsers() {        
        users = [];        
        $('#userSelect option:selected').each(function(i){
            users.push($(this).val());
        });        
        $.ajax({
            url: "{{url('')}}/admin/entidades/"+idEntidade+"/usuarios",
            data: {"users": users},
            type: 'POST'
        }).always(function(response) {   
            ajaxThenCallModal(idEntidade, tituloModal);        
            $('#userSelect')[0].sumo.unSelectAll();
        });        
    }

    function removeCidade(entidadeID, cidadeID) {
        r = confirm("Tem certeza que deseja remover?");
        if (r) {
            $.ajax({
                type: "DELETE",
                url: "{{url("")}}/admin/entidades/"+entidadeID+"/cidades",
                data: {"cidadeID": cidadeID},                        
            }).always(function(response){
                ajaxThenCallModalCidades(entidadeID, tituloModal);        
            });            
        }
    }
    function loadCidades() {
        $.ajax("{{url('admin/cidades/list')}}").always(function(response) {            
            cidades = "";
            $.each(response, function (index, value) { 
                cidades += "<option value="+value.id+">"+value.nome+"</option>";
            });            
            $('.modal .modal-footer #cidadeSelect').html(cidades);
            $('#cidadeSelect').SumoSelect({placeholder:"Selecione uma ou várias opções"});            
        });   
    }
    function addCidades() {        
        cidades = [];        
        $('#cidadeSelect option:selected').each(function(i){
            cidades.push($(this).val());
        });                
        $.ajax({
            url: "{{url('')}}/admin/entidades/"+idEntidade+"/cidades",
            data: {"cidades": cidades},
            type: 'POST'
        }).always(function(response) {               
            ajaxThenCallModalCidades(idEntidade, tituloModal);        
            $('#cidadeSelect')[0].sumo.unSelectAll();
        });        
    }
    </script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">entidades</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('success')}}
        </div>        
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-body">                
                <table class="table table-bordered w-100" width="100%" style="display: none">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Nome Fantasia</th>
                            <th>Usuários</th>
                            <th>Cidades</th>
                            <th>Endereço</th>
                            <th>CNPJ</th>
                            <th>Email</th>
                            <th>Telefone</th>
                            <th>Whatsapp</th>
                            <th>Site</th>
                            <th>Facebook</th>
                            <th>Instagram</th>
                            <th>Twitter</th>
                            <th>LinkedIn</th>
                            <th>Nome Presidente</th>
                            <th>Email Presidente</th>
                            <th>Telefone Fixo Presidente</th>
                            <th>Celular Presidente</th>
                            <th>Nome Financeiro</th>
                            <th>Email Financeiro</th>
                            <th>Telefone Fixo Financeiro</th>
                            <th>Celular Financeiro</th>
                            <th>Nome Gerente</th>
                            <th>Email Gerente</th>
                            <th>Telefone Fixo Gerente</th>
                            <th>Celular Gerente</th>
                            <th>Criado em</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="usuarios" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">   
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select multiple="multiple" id="userSelect" class="form-control" name=""></select>   
                        </div>
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addUsers()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>                    
                    </div>                                    
                </div>
                
            </div>
        </div>
    </div>   
    <div id="cidades" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                {{-- <div class="modal-footer justify-content-start">   
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select multiple="multiple" id="cidadeSelect" class="form-control" name=""></select>   
                        </div>
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addCidades()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>                    
                    </div>                                    
                </div> --}}
                
            </div>
        </div>
    </div>
   
@endsection