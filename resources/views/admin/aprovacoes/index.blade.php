@extends('layouts.admin')

@section('title')
    Alterações    
@endsection

@section('css')
    <link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>    
    <script>
    idAprovação = 0;
    tituloModal = '';

    $(".content").LoadingOverlay("show");
    $(document).ready(function() {
        $('table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,            
            ajax: "{{ url('admin/aprovacoes/datatable') }}",    
            order: [[0,"desc"]],
            columns: [
                {data: 'log_created_at'},                
                {data: 'status'},
                {data: 'name'},
                {data: 'nome'},
                {data: 'estado_sigla'},    
                {data: 'motivo_devolucao'},    
                @if (auth()->user()->hasRole('admin'))
                {data: 'action', orderable: false, searchable: false},                
                @endif
            ],
            language: {
                url: "{{ asset('plugins/datatables/pt-br.json') }}"
            },
            initComplete: function(){                
                $(".content").LoadingOverlay("hide");
            }
        });
    });
    function verTodos() {
        $("table").DataTable().search("").draw();
    }
    function verNovos() {
        $("table").DataTable().search("NOVO").draw();
    }
    function verRejeitados() {
        $("table").DataTable().search("REJEITADO").draw();
    }
    </script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">Aprovações</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('success')}}
        </div>        
    @endif
    @if(Session::has('error'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('error')}}
        </div>        
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-body">         
                @if (auth()->user()->hasRole('admin'))
                <div class="row">
                    <button class="btn btn-info mr-2" type="button" onclick="verTodos()"><i class="fas fa-filter"></i> VER TODOS</button>
                    <button class="btn btn-danger mr-2" type="button" onclick="verRejeitados()"><i class="fas fa-filter"></i> VER REJEITADOS</button>
                    <button class="btn btn-warning mr-2" type="button" onclick="verNovos()"><i class="fas fa-filter"></i> VER NOVOS</button>
                </div>
                @endif
                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>Enviado em</th>                            
                            <th>Status</th>
                            <th>Usuário</th>
                            <th>Cidade</th>
                            <th>Estado</th>
                            <th>Motivo Devolução</th>                                
                            @if (auth()->user()->hasRole('admin'))
                            <th>Ações</th>                        
                            @endif        
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="alojamentoTipos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">   
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select multiple="multiple" id="alojamentoTiposSelect" class="form-control" name=""></select>   
                        </div>
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addAlojamentoTipos()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>                    
                    </div>                                    
                </div>
                
            </div>
        </div>
    </div>
    <div id="modalTipos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">   
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select multiple="multiple" id="modalTiposSelect" class="form-control" name=""></select>   
                        </div>
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addModalTipos()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>                    
                    </div>                                    
                </div>
                
            </div>
        </div>
    </div>
    <div id="localEventoTipos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">   
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select multiple="multiple" id="localEventoTiposSelect" class="form-control" name=""></select>   
                        </div>
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addLocalEventoTipos()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>                    
                    </div>                                    
                </div>
                
            </div>
        </div>
    </div>
    <div id="alojamentoCategorias" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-start">   
                    <div class='row w-100'>
                        <div class='col-md-10 col-sm-12 col-xs-12'>
                            <select multiple="multiple" id="alojamentoCategoriasSelect" class="form-control" name=""></select>   
                        </div>
                        <div class="w-100 d-md-none mt-3"></div>                    
                        <div class='col-md-2 col-sm-12 col-xs-12'>
                            <button class="btn btn-success w-100" onclick="addAlojamentoCategorias()"><i class="fas fa-plus"></i> Adicionar</button>
                        </div>                    
                    </div>                                    
                </div>
                
            </div>
        </div>
    </div>
@endsection