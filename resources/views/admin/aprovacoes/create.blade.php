@extends('layouts.admin')
@section('title','Cadastrar Aprovação')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Aprovações</a></li>
    <li class="breadcrumb-item active">Cadastrar Aprovação</li>  
@endsection

@section('js')
    <script>        
        function previewImage() {
            var total_file=document.getElementById("fotos").files.length;
            $('#image_preview').html('');
            for(var i=0;i<total_file;i++){
                $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
            }
        }  
    </script>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/aprovacoes') }}" enctype="multipart/form-data">            
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class='row'>                    
                    <label>Nome</label>
                    <input id="nome" type="text" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" name="nome" required >

                    @if ($errors->has('nome'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nome') }}</strong>
                        </span>
                    @endif                    
                </div>

                 <div class='row'>                    
                    <label>Descrição</label>
                    <textarea id="descricao" type="text" class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" name="descricao" required rows=5>{{old("descricao")}}</textarea>

                    @if ($errors->has('descricao'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('descricao') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Estado</label>
                    <input id="estado" type="text" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}" name="estado" required >

                    @if ($errors->has('estado'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('estado') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Sigla</label>
                    <input id="estado_sigla" type="text" class="form-control{{ $errors->has('estado_sigla') ? ' is-invalid' : '' }}" name="estado_sigla" required >

                    @if ($errors->has('estado_sigla'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('estado_sigla') }}</strong>
                        </span>
                    @endif                    
                </div>          
                
                <div class='row'>                    
                    <label>Distância Capital (em km, somente número)</label>
                    <input id="distancia_capital" type="text" class="form-control{{ $errors->has('distancia_capital') ? ' is-invalid' : '' }}" name="distancia_capital" required >

                    @if ($errors->has('distancia_capital'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('distancia_capital') }}</strong>
                        </span>
                    @endif                    
                </div>   

                <div class='row'>                    
                    <label>Tem Centro de Atendimento?</label>
                    <select name="tem_centro_atendimento" class='form-control' id="" required>
                        <option value=""></option>
                        <option value="1">Sim</option>
                        <option value="0">Não</option>
                    </select>
                    
                    @if ($errors->has('tem_centro_atendimento'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tem_centro_atendimento') }}</strong>
                        </span>
                    @endif                    
                </div>   

                <div class='row'>                    
                    <label>Tem Rodovia Estadual?</label>
                    <select name="tem_rodovia_estadual" class='form-control' id="" required>
                        <option value=""></option>
                        <option value="1">Sim</option>
                        <option value="0">Não</option>
                    </select>

                    @if ($errors->has('tem_rodovia_estadual'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tem_rodovia_estadual') }}</strong>
                        </span>
                    @endif                    
                </div>   

                <div class='row'>                    
                    <label>Tem Rodovia Federal?</label>
                    <select name="tem_rodovia_federal" class='form-control' id="" required>
                        <option value=""></option>
                        <option value="1">Sim</option>
                        <option value="0">Não</option>
                    </select>

                    @if ($errors->has('tem_rodovia_federal'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tem_rodovia_federal') }}</strong>
                        </span>
                    @endif                    
                </div>   

                <div class='row'>                    
                    <label>Capaaprovacao Maior Espaço</label>                        
                    <input id="capaaprovacao_maior_espaco" type="number" class="form-control{{ $errors->has('capaaprovacao_maior_espaco') ? ' is-invalid' : '' }}" name="capaaprovacao_maior_espaco" value='{{old("capaaprovacao_maior_espaco")}}' required >

                    @if ($errors->has('capaaprovacao_maior_espaco'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('capaaprovacao_maior_espaco') }}</strong>
                        </span>
                    @endif                  
                </div>

                <div class='row'>                    
                    <label>Capaaprovacao Maior Auditório</label>                    
                    <input id="capaaprovacao_maior_auditorio" type="number" class="form-control{{ $errors->has('capaaprovacao_maior_auditorio') ? ' is-invalid' : '' }}" name="capaaprovacao_maior_auditorio" value='{{old("capaaprovacao_maior_auditorio")}}' required >

                    @if ($errors->has('capaaprovacao_maior_auditorio'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('capaaprovacao_maior_auditorio') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Capaaprovacao Maior Banquete</label>                    
                    <input id="capaaprovacao_maior_banquete" type="number" class="form-control{{ $errors->has('capaaprovacao_maior_banquete') ? ' is-invalid' : '' }}" name="capaaprovacao_maior_banquete" value='{{old("capaaprovacao_maior_banquete")}}' required >

                    @if ($errors->has('capaaprovacao_maior_banquete'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('capaaprovacao_maior_banquete') }}</strong>
                        </span>
                    @endif                   
                </div>

                <div class='row'>                    
                    <label>Capaaprovacao Total Quartos</label>                    
                    <input id="capaaprovacao_total_quartos" type="number" class="form-control{{ $errors->has('capaaprovacao_total_quartos') ? ' is-invalid' : '' }}" name="capaaprovacao_total_quartos" value='{{old("capaaprovacao_total_quartos")}}' required >

                    @if ($errors->has('capaaprovacao_total_quartos'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('capaaprovacao_total_quartos') }}</strong>
                        </span>
                    @endif                   
                </div>

                <div class='row'>                    
                    <label>Capaaprovacao Total Leitos</label>                    
                    <input id="capaaprovacao_total_leitos" type="number" class="form-control{{ $errors->has('capaaprovacao_total_leitos') ? ' is-invalid' : '' }}" name="capaaprovacao_total_leitos" value='{{old("capaaprovacao_total_leitos")}}' required >

                    @if ($errors->has('capaaprovacao_total_leitos'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('capaaprovacao_total_leitos') }}</strong>
                        </span>
                    @endif                  
                </div>

                <div class='row'>                    
                    <label>Capaaprovacao Quartos Maior Hotel</label>                    
                    <input id="capaaprovacao_quartos_maior_hotel" type="number" class="form-control{{ $errors->has('capaaprovacao_quartos_maior_hotel') ? ' is-invalid' : '' }}" name="capaaprovacao_quartos_maior_hotel" value='{{old("capaaprovacao_quartos_maior_hotel")}}' required >

                    @if ($errors->has('capaaprovacao_quartos_maior_hotel'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('capaaprovacao_quartos_maior_hotel') }}</strong>
                        </span>
                    @endif                        
                </div>
                
                <div class='row'>                    
                    <label>Número Instituições Ensino Superior</label>                      
                    <input id="numero_instituicoes_ensino_superior" type="number" class="form-control{{ $errors->has('numero_instituicoes_ensino_superior') ? ' is-invalid' : '' }}" name="numero_instituicoes_ensino_superior" value='{{old("numero_instituicoes_ensino_superior")}}' required >

                    @if ($errors->has('numero_instituicoes_ensino_superior'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('numero_instituicoes_ensino_superior') }}</strong>
                        </span>
                    @endif                
                </div>

                <div class='row'>                    
                    <label>Áreas de Expertise Referência</label>
                    <textarea id="areas_expertise_referencia" type="text" class="form-control{{ $errors->has('areas_expertise_referencia') ? ' is-invalid' : '' }}" name="areas_expertise_referencia" required rows=5>{{old("areas_expertise_referencia")}}</textarea>

                    @if ($errors->has('areas_expertise_referencia'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('areas_expertise_referencia') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Cinco Principais Eventos</label>                            
                    <textarea id="principais_eventos" type="text" class="form-control{{ $errors->has('principais_eventos') ? ' is-invalid' : '' }}" name="principais_eventos" required rows=5>{{old("principais_eventos")}}</textarea>

                    @if ($errors->has('principais_eventos'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('principais_eventos') }}</strong>
                        </span>
                    @endif                
                </div>

                <div class='row'>                    
                    <label>Fotos</label>
                    <input type="file" name="fotos[]" id='fotos' class='form-control' multiple onchange="previewImage();">
                    <div id="image_preview"></div>

                    @if ($errors->has('fotos'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('fotos') }}</strong>
                        </span>
                    @endif                    
                </div>   
                
                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Cadastrar </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection