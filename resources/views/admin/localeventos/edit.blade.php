@extends('layouts.admin')
@section('title','Editar LocalEvento')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">LocalEventos</a></li>
    <li class="breadcrumb-item active">Editar LocalEvento</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/localeventos',$localevento->id) }}">
            @method('PATCH')
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class='row'>                    
                    <label>Nome</label>
                    <input id="nome" type="text" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" name="nome" value="{{$localevento->nome }}" required >

                    @if ($errors->has('nome'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nome') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Descrição</label>
                    <textarea id="descricao" type="text" class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" name="descricao" required rows=5>{{$localevento->descricao }}</textarea>

                    @if ($errors->has('descricao'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('descricao') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Capacidade Auditório</label>
                    <input id="capacidade_auditorio" type="text" class="form-control{{ $errors->has('capacidade_auditorio') ? ' is-invalid' : '' }}" name="capacidade_auditorio" value="{{$localevento->capacidade_auditorio }}" required >

                    @if ($errors->has('capacidade_auditorio'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('capacidade_auditorio') }}</strong>
                        </span>
                    @endif                    
                </div>
                
                <div class='row'>                    
                    <label>Tipo</label>
                    <select required class="form-control" name="local_evento_tipo_id" id="local_evento_tipo_id">
                        @foreach ($local_evento_tipos as $tipo)
                            @if ($tipo->id == $localevento->local_evento_tipo_id || old('local_evento_tipo_id') == $localevento->local_evento_tipo_id)
                                <option value="{{$tipo->id}}" selected>{{$tipo->nome}}</option>
                            @else    
                                <option value="{{$tipo->id}}">{{$tipo->nome}}</option>
                            @endif    
                        @endforeach                        
                    </select>    

                    @if ($errors->has('local_evento_tipo_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('local_evento_tipo_id') }}</strong>
                        </span>
                    @endif                    
                </div>

                <div class='row'>                    
                    <label>Cidade</label>
                    <select required class="form-control" name="cidade_id" id="cidade_id">
                        @foreach ($cidades as $cidade)
                            @if ($cidade->id == $localevento->cidade_id || old('cidade_id') == $localevento->cidade_id)
                                <option value="{{$cidade->id}}" selected>{{$cidade->nome}}</option>
                            @else    
                                <option value="{{$cidade->id}}">{{$cidade->nome}}</option>
                            @endif    
                        @endforeach                        
                    </select>    

                    @if ($errors->has('cidade_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('cidade_id') }}</strong>
                        </span>
                    @endif                    
                </div>         
                                
                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Salvar edição
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection