@extends('layouts.admin')

@section('title')
    Resumo Financeiro {{!auth()->user()->hasRole('admin') ? (auth()->user()->entidade()->exists() ? 'de '.auth()->user()->entidade->first()->nome_fantasia : '') : '' }}
@endsection

@section('css')
<link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/ApexCharts/apexcharts.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">Financeiro</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('success')}}
        </div>        
    @endif
    <div class="row">
        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box bg-danger">
            <span class="info-box-icon"><i class="fas fa-user-times"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Mensalidades Vencidas</span>
            <span class="info-box-number">
                {{$vencidas}}                 
            </span>
            </div>
        </div>
        </div>

        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box bg-success">
            <span class="info-box-icon"><i class="fas fa-user-check"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Associados Em Dia</span>
            <span class="info-box-number">
                {{$emDia}}               
            </span>
            </div>
        </div>
        </div>

        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box bg-info">
            <span class="info-box-icon"><i class="fas fa-users"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Associados Ativos</span>
            <span class="info-box-number">
                {{$totalAssociados}}               
            </span>
            </div>
        </div>
        </div>

        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box bg-default">
            <span class="info-box-icon"><i class="fas fa-users"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Associados Inativos</span>
            <span class="info-box-number">
                {{$totalAssociadosInativos}}               
            </span>
            </div>
        </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header border-0">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title">Pagamentos recebidos</h3>                    
                    </div>
                </div>
                <div class="card-body">
                    <div class="d-flex">
                    <p class="d-flex flex-column">
                        <span class="text-bold text-lg" id='totalRecebido'>R$ {{$dadosGraficoTodosOsPagamentos['total']}} </span>
                        <span>Recebido até hoje</span>
                    </p>
                    @if (auth()->user()->hasRole('admin'))
                    <div class="ml-auto d-flex flex-column text-right">
                        <select id="cvb" class='form-control'>
                            <option value="0">Todos os CVBs</option>
                            @foreach ($cvbs as $cvb)
                            <option value="{{$cvb->id}}">{{$cvb->nome_fantasia}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    </div>
                    <!-- /.d-flex -->

                    <div class="position-relative mb-4">
                        <div id="chartPagamentosRecebitos"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header border-0">
                    <div class="d-flex justify-content-between">
                    <h3 class="card-title">Pagamentos pendentes</h3>
                    
                    </div>
                </div>
                <div class="card-body">
                    <div class="d-flex">
                    <p class="d-flex flex-column">
                        <span class="text-bold text-lg" id='totalPendente'>R$ {{$dadosGraficoTodosOsPagamentosPendentes['total']}}</span>
                        <span>Pendente até hoje</span>
                    </p>
                    @if (auth()->user()->hasRole('admin'))
                    <div class="ml-auto d-flex flex-column text-right">
                        <select id="cvbPendentes" class='form-control'>
                            <option value="0">Todos os CVBs</option>                                
                            @foreach ($cvbs as $cvb)
                            <option value="{{$cvb->id}}">{{$cvb->nome_fantasia}}</option>
                            @endforeach                                
                        </select>
                    </div>
                    @endif
                    </div>
                    <!-- /.d-flex -->

                    <div class="position-relative mb-4">
                        <div id="chartPagamentosPendentes"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('js')
<script src="{{asset('plugins/ApexCharts/apexcharts.min.js')}}"></script>
<script>
    var chartPagamentosRecebitos;
    var chartPagamentosPendentes;

    $("#cvb").on('change', function () {
        $("#chartPagamentosRecebitos").LoadingOverlay('show');
        $.ajax({
            type: "POST",
            url: "{{url('admin/financeiro/recebido')}}",
            data: {'entidade_id' : $("#cvb option:selected").val()},
            success: function (response) {
                $("#chartPagamentosRecebitos").LoadingOverlay('hide');
                carregarGraficoPagamentosRecebidos(response.grafico);
                $("#totalRecebido").html('R$ '+response.total);
            }
        });
    });

    $("#cvbPendentes").on('change', function () {
        $("#chartPagamentosPendentes").LoadingOverlay('show');
        $.ajax({
            type: "POST",
            url: "{{url('admin/financeiro/pendente')}}",
            data: {'entidade_id' : $("#cvbPendentes option:selected").val()},
            success: function (response) {
                $("#chartPagamentosPendentes").LoadingOverlay('hide');
                carregarGraficoPagamentosPendentes(response.grafico);
                $("#totalPendente").html('R$ '+response.total);
            }
        });
    });

    carregarGraficoPagamentosRecebidos(@json($dadosGraficoTodosOsPagamentos['grafico']));
    carregarGraficoPagamentosPendentes(@json($dadosGraficoTodosOsPagamentosPendentes['grafico']));

    function carregarGraficoPagamentosRecebidos(dados) {   
        
        if (chartPagamentosRecebitos) {
            chartPagamentosRecebitos.updateSeries(dados);
        } else {
            var optionsChartPagamentosRecebitos = {               
                defaultLocale: 'pt-br',
                // series: [{"name":"2020", "data":[125.22, "null", 33.33, "null", 154.44, 587.43, 998.32, 785.17, "null", "null", "null", "null"]}],
                series: dados,
                colors: ['#007bff'],
                chart: {
                    type: 'bar',
                    height: 350,
                },                
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: '55%',  
                        dataLabels: {
                            position: 'top',
                        },              
                    },
                },
                colors: ["#0065D2", "#F27036", "#663F59", "#6A6E94", "#4E88B4", "#00A7C6", "#18D8D8", '#A9D794',
                    '#46AF78', '#A93F55', '#8C5E58', '#2176FF', '#33A1FD', '#7A918D', '#BAFF29'
                ],
                dataLabels: {
                    enabled: true,
                    formatter: function (val) {
                        if (val) {
                            return "R$ " + val.toString().replace('.',',');
                        }
                    },
                    offsetY: -20,
                    background: {
                        enabled: true,
                        foreColor: '#fff',
                        borderRadius: 2,
                        padding: 4,
                        opacity: 0.9,
                        borderWidth: 1,
                        borderColor: '#fff'
                    },
                    style: {
                        fontSize: '12px',
                        colors: ["#304758"]
                    }
                },
                stroke: {
                    show: true,
                    width: 2,
                    colors: ['transparent']
                },
                xaxis: {            
                    categories: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
                },
                yaxis: {
                    labels: {
                        formatter: function (val) {
                            return "R$ " + val.toFixed(0);
                        },
                    },
                    
                },
                fill: {
                    opacity: 1
                },
                tooltip: {
                    enabled: false,
                    y: {
                        formatter: function (val) {
                        return "R$ " + val.toString().replace('.',',')
                        }
                    }
                },
                legend: {
                    show: true,
                    showForSingleSeries: true,
                    position: 'top',
                }
            };        
            chartPagamentosRecebitos = new ApexCharts(document.querySelector("#chartPagamentosRecebitos"), optionsChartPagamentosRecebitos);    
            chartPagamentosRecebitos.render();
        }
    }

    function carregarGraficoPagamentosPendentes(dados) {        
        if (chartPagamentosPendentes) {
            chartPagamentosPendentes.updateSeries(dados);
        } else {
            var optionsChartPagamentosPendentes = {               
                defaultLocale: 'pt-br',
                // series: [{"name":"2020", "data":[125.22, "null", 33.33, "null", 154.44, 587.43, 998.32, 785.17, "null", "null", "null", "null"]}],
                series: dados,
                colors: ['#007bff'],
                chart: {
                    type: 'bar',
                    height: 350,
                },                
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: '55%',  
                        dataLabels: {
                            position: 'top',
                        },              
                    },
                },
                colors: ["#0065D2", "#F27036", "#663F59", "#6A6E94", "#4E88B4", "#00A7C6", "#18D8D8", '#A9D794',
                    '#46AF78', '#A93F55', '#8C5E58', '#2176FF', '#33A1FD', '#7A918D', '#BAFF29'
                ],
                dataLabels: {
                    enabled: true,
                    formatter: function (val) {
                        if (val) {
                            return "R$ " + val.toString().replace('.',',');
                        }
                    },
                    offsetY: -20,
                    background: {
                        enabled: true,
                        foreColor: '#fff',
                        borderRadius: 2,
                        padding: 4,
                        opacity: 0.9,
                        borderWidth: 1,
                        borderColor: '#fff'
                    },
                    style: {
                        fontSize: '12px',
                        colors: ["#304758"]
                    }
                },
                stroke: {
                    show: true,
                    width: 2,
                    colors: ['transparent']
                },
                xaxis: {            
                    categories: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
                },
                yaxis: {
                    labels: {
                        formatter: function (val) {
                            return "R$ " + val.toFixed(0);
                        },
                    },
                    
                },
                fill: {
                    opacity: 1
                },
                tooltip: {
                    enabled: false,
                    y: {
                        formatter: function (val) {
                        return "R$ " + val.toString().replace('.',',')
                        }
                    }
                },
                legend: {
                    show: true,
                    showForSingleSeries: true,
                    position: 'top',
                }
            };        
            chartPagamentosPendentes = new ApexCharts(document.querySelector("#chartPagamentosPendentes"), optionsChartPagamentosPendentes);    
            chartPagamentosPendentes.render();
        }
    }
    
</script>
@endsection