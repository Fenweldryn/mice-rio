@extends('layouts.admin')

@section('title')
    Associados
    <a href="{{route('associados.create')}}" class="btn btn-success mr-2"><i class="fas fa-plus"></i> Cadastrar</a>
    <a href="{{route('pagamentos.create')}}" class="btn btn-primary"><i class="fas fa-hand-holding-usd"></i> Informar Pagamento em Massa</a>
@endsection

@section('css')
    <link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>    
    <script>
    
    $(".content").LoadingOverlay("show");
    $(document).ready(function() {
        
        $('table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ url('admin/associados/datatable') }}",  
            columns: [
                {data: 'id'},                
                {data: 'pagamento', width: "10%"},                
                {data: 'nome'},                
                {data: 'entidade', width: "15%"},                
                {data: 'mensalidade'},                
                {data: 'dia_vencimento'},                
                {data: 'created_at'},
                {data: 'action', orderable: false, searchable: false},      
            ],
            language: {
                url: "{{ asset('plugins/datatables/pt-br.json') }}"
            },
            initComplete: function(){
                $(".content").LoadingOverlay("hide");
            }
        });
    });
    </script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">Associados</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>            
            {{Session::get('success')}}
        </div>        
    @endif
    <div class="row">
        
        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box bg-primary">
            <span class="info-box-icon"><i class="fas fa-users"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Associados Ativos</span>
            <span class="info-box-number">
                {{$totalAssociados}}               
            </span>
            </div>
        </div>
        </div>

        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box bg-secondary">
            <span class="info-box-icon"><i class="fas fa-users"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">Associados inativos</span>
            <span class="info-box-number">
                {{$totalAssociadosInativos}}               
            </span>
            </div>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-body">                
                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Pagamento</th>
                            <th>Nome</th>
                            <th>Associado a</th>
                            <th>Mensalidade</th>
                            <th>Dia Vencimento</th>
                            <th>Criado em</th>                            
                            <th>Ações</th>                            
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    
@endsection