@extends('layouts.admin')
@section('title','Editar Associado')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Associados</a></li>
    <li class="breadcrumb-item active">Editar Associado</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/associados',$associado->id) }}">
            @method('PATCH')
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class='row'>                    
                    <label>Nome</label>
                    <input id="nome" type="text" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" name="nome" value='{{old("nome") ?? $associado->nome}}' required >
                </div>

                <div class='row'>                    
                    <label>CNPJ</label>
                    <input id="cnpj" type="text" class="form-control{{ $errors->has('cnpj') ? ' is-invalid' : '' }}" name="cnpj" value='{{old("cnpj") ?? $associado->cnpj}}' required >
                </div>

                <div class='row'>                    
                    <label>Associado a</label>
                    <select required class="form-control" name="entidade_id" id="entidade_id">
                        @foreach ($entidades as $entidade)
                            @if ($associado->entidade_id == $entidade->id || old('entidade_id') == $entidade->id)
                                <option value="{{$entidade->id}}" selected>{{$entidade->nome}}</option>
                            @else    
                                <option value="{{$entidade->id}}">{{$entidade->nome}}</option>
                            @endif    
                        @endforeach                        
                    </select>    
                </div>

                <div class='row'>                    
                    <label>Data de associação</label>
                    <input id="data_associacao" type="text" class="form-control{{ $errors->has('data_associacao') ? ' is-invalid' : '' }}" name="data_associacao" value='{{old("data_associacao") ?? ($associado->data_associacao ? $associado->data_associacao->format("d/m/Y") : '') }}' required >                  
                </div>

                <div class='row'>                    
                    <label>Mensalidade</label>
                    <input id="mensalidade" type="text" class="form-control{{ $errors->has('mensalidade') ? ' is-invalid' : '' }}" name="mensalidade" value='{{old("mensalidade") ?? number_format(floatval($associado->mensalidade),2,",","")}}' required >                  
                </div>

                <div class='row'>                    
                    <label>Dia Vencimento</label>
                    <input id="dia_vencimento" type="text" class="form-control{{ $errors->has('dia_vencimento') ? ' is-invalid' : '' }}" name="dia_vencimento" value='{{old("dia_vencimento") ?? $associado->dia_vencimento}}' required >
                </div>

                <div class='row'>                    
                    <label>Tipo</label>
                    <select required class="form-control" name="tipo" id="tipo">
                        @foreach ($tipos as $tipo)
                            @if ($associado->tipo_type == $tipo->model || old('tipo') == $tipo->model)
                                <option value="{{$tipo->id}}" selected>{{$tipo->nome}}</option>
                            @else    
                                <option value="{{$tipo->id}}">{{$tipo->nome}}</option>
                            @endif    
                        @endforeach                          
                    </select>    
                </div>

                <div class='row'>                    
                    <label>Subtipo</label>
                    <select required class="form-control" name="subtipo" id="subtipo">
                        @foreach ($subtipos as $subtipo)
                            @if ($associado->tipo_id == $subtipo->id || old('subtipo') == $subtipo->id)
                                <option value="{{$subtipo->id}}" selected>{{$subtipo->nome}}</option>
                            @else    
                                <option value="{{$subtipo->id}}">{{$subtipo->nome}}</option>
                            @endif    
                        @endforeach                                                     
                    </select>    
                </div>

                <div class='row'>                    
                    <label>Email</label>
                    <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value='{{old("email") ?? $associado->email}}' required >
                </div>

                <div class='row'>                    
                    <label>Endereço Completo</label>
                    <input id="endereco" type="text" class="form-control{{ $errors->has('endereco') ? ' is-invalid' : '' }}" name="endereco" value='{{old("endereco") ?? $associado->endereco}}' required >
                </div>

                <div class='row'>                    
                    <label>Telefone</label>
                     <div id="telefones" class="row ml-0 w-100">
                        @foreach ($associado->telefones as $telefone)
                        <input type="number" class="form-control{{ $errors->has('telefones') ? ' is-invalid' : '' }}" 
                            name="telefones[{{ $loop->index }}]"
                            value='{{ old("telefones[ $loop->index ]") ?? ($associado->telefones[$loop->index] ?? '') }}'
                            placeholder="telefone 1" 
                            required
                        />
                             
                        @endforeach                        
                    </div>
                    <button class="btn btn-info row ml-0" type='button' onclick="addTelefoneForm()"><i class="fas fa-plus"></i> adicionar mais telefones</button>
                </div>

                <div class='row'>                    
                    <label>Site</label>
                    <input id="site" type="string" class="form-control{{ $errors->has('site') ? ' is-invalid' : '' }}" name="site" value='{{old("site") ?? $associado->site}}' >
                </div>

                <div class='row'>                    
                    <label>Facebook</label>
                    <input id="facebook" type="string" class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }}" name="facebook" value='{{old("facebook") ?? $associado->facebook}}'>
                </div>

                <div class='row'>                    
                    <label>Instagram</label>
                    <input id="instagram" type="string" class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }}" name="instagram" value='{{old("instagram") ?? $associado->instagram}}'>
                </div>

                <div class='row'>                    
                    <label>LinkedIn</label>
                    <input id="linkedin" type="string" class="form-control{{ $errors->has('linkedin') ? ' is-invalid' : '' }}" name="linkedin" value='{{old("linkedin") ?? $associado->linkedin}}'>
                </div>

                <div class='row'>                    
                    <label>Pinterest</label>
                    <input id="pinterest" type="string" class="form-control{{ $errors->has('pinterest') ? ' is-invalid' : '' }}" name="pinterest" value='{{old("pinterest") ?? $associado->pinterest}}'>
                </div>

                <div class='row'>                    
                    <label>Distância do centro (km)</label>
                    <input id="distancia_centro_km" type="text" class="form-control{{ $errors->has('distancia_centro_km') ? ' is-invalid' : '' }}" name="distancia_centro_km" value='{{old("distancia_centro_km") ?? number_format($associado->distancia_centro_km,2,',','')}}'>
                </div>

                <div class='row'>                    
                    <label>Área construída total (m²)</label>
                    <input id="area_construida_total_m2" type="number" class="form-control{{ $errors->has('area_construida_total_m2') ? ' is-invalid' : '' }}" name="area_construida_total_m2" value='{{old("area_construida_total_m2") ?? $associado->area_construida_total_m2}}'>
                </div>

                <div class='row'>                    
                    <label>Área evento total (m²)</label>
                    <input id="area_evento_total_m2" type="number" class="form-control{{ $errors->has('area_evento_total_m2') ? ' is-invalid' : '' }}" name="area_evento_total_m2" value='{{old("area_evento_total_m2") ?? $associado->area_evento_total_m2}}'>
                </div>

                <div class='row'>                    
                    <label>Capacidade de lugares auditório</label>
                    <input id="capacidade_lugares_auditorio" type="number" class="form-control{{ $errors->has('capacidade_lugares_auditorio') ? ' is-invalid' : '' }}" name="capacidade_lugares_auditorio" value='{{old("capacidade_lugares_auditorio") ?? $associado->capacidade_lugares_auditorio}}'>
                </div>

                <div class='row'>                    
                    <label>Capacidade de lugares coquetel</label>
                    <input id="capacidade_lugares_coquetel" type="number" class="form-control{{ $errors->has('capacidade_lugares_coquetel') ? ' is-invalid' : '' }}" name="capacidade_lugares_coquetel" value='{{old("capacidade_lugares_coquetel") ?? $associado->capacidade_lugares_coquetel}}'>
                </div>
                
                <div class="row mt-4">                    
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Salvar Edição 
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js')    
    <script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js')}}"></script>
    <script>
        window.onload = function () {        
            $("#tipo").change(function (e) {             
                $("#subtipo").LoadingOverlay('show');
                $("#subtipo").html('');
                $.ajax({
                    type: "get",
                    url: "{{ url('admin/associados/subtipo') }}/"+$("#tipo").val(),
                    success: function (response) {
                        $.each(response, function (i, value) { 
                            $("#subtipo").append("<option value="+i+">"+value+"</option>");                     
                        });
                        $("#subtipo").LoadingOverlay('hide');
                    }
                });
            });
            $('#data_associacao').datepicker({
                language:'pt-BR',
                clearBtn: true,
                maxViewMode: 2,
                format: "dd/mm/yyyy",
                orientation: "bottom auto",
                autoclose: true
            });
            
        };
        var formTelefoneCount = {{ count($associado->telefones) }};
        function addTelefoneForm() {
            formTelefoneCount += 1;
            $("#telefones").append("<input id='telefones' type='number' class='form-control mt-1' name='telefones[]' placeholder='telefone "+formTelefoneCount+"'/>");
        }
    </script>
@endsection
@section('css')
    <link href="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection