@extends('layouts.admin')
@section('title','Ver Associado')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin')}}">Associados</a></li>
    <li class="breadcrumb-item active">Ver Associado</li>
@endsection

@section('js')
<script>
    function informarPagamento(botao, pagamento_id){       
        $(botao).LoadingOverlay('show');
        $.ajax({
            type: "POST",
            url: "{{route('pagamentos.store')}}",
            data: {pagamento_id:pagamento_id},
            success: function (response) {
                $(botao).fadeOut(function(){$(botao).parent().parent().remove() });
            }
        });
    }
</script>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <a class="btn btn-default mr-2" href="{{ url('admin/associados') }}"><i class='fas fa-arrow-left'></i> Voltar</a>
    </div>
</div>
<div class="row">
    <div class="col-md-6">                 
        <form action="#">            
                       
            <div class="card card-primary card-body">     
                <div class='row'>                    
                    <h3>Dados do Associado</h3>  
                </div>
                <div class='row'>                    
                    <label>Nome</label>
                    <input id="nome" type="text" class="form-control" value='{{$associado->nome}}' disabled >
                </div>

                <div class='row'>                    
                    <label>Associado a</label>
                    <input id="entidade_id" type="text" class="form-control" value='{{$associado->entidade->nome}}' disabled >
                </div>

                <div class='row'>                    
                    <label>Mensalidade</label>
                    <input id="mensalidade" type="text" class="form-control" value='{{$associado->mensalidade}}' disabled >                  
                </div>

                <div class='row'>                    
                    <label>Dia Vencimento</label>
                    <input id="dia_vencimento" type="text" class="form-control" value='{{$associado->dia_vencimento}}' disabled >
                </div>
                
                <div class="row mt-4">                    
                    <button type="button" onClick="history.back()" class="btn btn-info">
                        <i class="fas fa-arrow-left"></i> Voltar 
                    </button>
                    <a href='{{ route("associados.edit", $associado->id) }}' class="ml-3 btn btn-primary"><i class="fas fa-pencil-alt"></i> Editar </a>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-6">                 
        <form action="#">            
                        
            <div class="card card-primary card-body">     
                <div class='row'>                    
                    <h3>Pagamentos Pendentes</h3>  
                </div>
                <div class='row'>              
                    @if ($associado->PagamentosPendentes()->count() == 0)
                        Não há pagamentos pendentes.
                    @else
                        <table class="table table-bordered table-striped">
                            <thead>
                                <th>Data</th>
                                <th>Valor</th>
                                <th>Dia vencimento</th>
                                <th>Pagamento</th>
                            </thead>
                            <tbody>
                                @foreach ($associado->PagamentosPendentes() as $pagamentoPendente)
                                    <tr>
                                        <td>{{$pagamentoPendente->data_referencia}}</td>
                                        <td>R$ {{number_format($pagamentoPendente->valor,2,',','')}}</td>
                                        <td>{{$pagamentoPendente->dia_vencimento}}</td>
                                        <td><button type='button' class="btn btn-primary" onclick="informarPagamento(this,{{$pagamentoPendente->id}})"><i class="fas fa-hand-holding-usd"></i> Informar Pagamento</button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>
@endsection