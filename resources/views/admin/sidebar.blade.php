<aside class="main-sidebar sidebar-dark-primary">
    <!-- Brand Logo -->
    <a href="{{url('admin') }}" class="brand-link">
        <img src="{{url('/logot.png')}}" alt="ADMINISTRAÇÃO" class="brand-image elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">ADMINISTRAÇÃO</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a class="d-block">{{Auth::user()->email}}</a>
            </div>
        </div> --}}

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{url('/admin')}}" class="nav-link {{ Request::is('admin') ? 'active' : '' }}">
                        <i class="fas fa-tachometer-alt nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                
                <li class="nav-header">GESTÃO</li>
                @if (auth()->user()->can('ver propostas'))
                <li class="nav-item">
                    <a href="{{route('propostas.index')}}" class="nav-link {{ Request::is('admin/analises*') ? 'active' : '' }}">
                        <i class="fas fa-paper-plane nav-icon"></i>
                        <p>Propostas 
                            @if (\App\Proposta::where('status', 'nova')->exists())
                                <span class="badge badge-warning right">{{\App\Proposta::where('status', 'nova')->count()}}</span>
                            @endif
                        </p>
                    </a>
                </li>                    
                @endif

                <li class="nav-item">
                    <a href="{{route('aprovacoes.index')}}" class="nav-link {{ Request::is('admin/aprovacoes*') ? 'active' : '' }}">
                        <i class="fas fa-check nav-icon"></i>
                        <p>Alterações 
                            @if (\App\Cidade::pending()->count() > 0)
                                <span class="badge badge-warning right">{{\App\Cidade::pending()->count()}}</span>
                            @endif
                        </p>
                    </a>
                </li>                    

                @if (auth()->user()->can('ver cidades'))
                <li class="nav-item">
                    <a href="{{route('cidades.index')}}" class="nav-link {{ Request::is('admin/cidades*') ? 'active' : '' }}">
                        <i class="fas fa-city nav-icon"></i>
                        <p>Cidades</p>
                    </a>
                </li>
                @endif

                @if (auth()->user()->can('ver entidades'))
                <li class="nav-item">
                    <a href="{{route('entidades.index')}}" class="nav-link {{ Request::is('admin/entidades*') ? 'active' : '' }}">
                        <i class="fas fa-landmark nav-icon"></i>
                        <p>Entidades</p>
                    </a>
                </li>           
                @endif

                @if (auth()->user()->hasRole('admin') || auth()->user()->hasRole('gestor'))
                <li class="nav-header">SISCVB</li>
                <li class="nav-item">
                    <a href="{{route('associados.index')}}" class="nav-link {{ Request::is('admin/associados*') ? 'active' : '' }}" class="nav-link">
                        <i class="fas fa-user nav-icon"></i>
                        <p>Associados</p>
                    </a>
                </li>           
                <li class="nav-item">
                    <a href="{{route('pagamentos.index')}}" class="nav-link {{ Request::is('admin/pagamentos*') ? 'active' : '' }}" class="nav-link">
                        <i class="fas fa-dollar-sign nav-icon"></i>
                        <p>Pagamentos</p>
                    </a>
                </li>           
                <li class="nav-item">
                    <a href="{{route('permutas.index')}}" class="nav-link {{ Request::is('admin/permutas*') ? 'active' : '' }}" class="nav-link">
                        <i class="far fa-handshake nav-icon"></i>
                        <p>Permutas</p>
                    </a>
                </li>           
                <li class="nav-item">
                    <a href="{{route('financeiro.index')}}" class="nav-link {{ Request::is('admin/financeiro*') ? 'active' : '' }}" class="nav-link">
                        <i class="fas fa-chart-line nav-icon"></i>
                        <p>Resumo Financeiro</p>
                    </a>
                </li>           
                @endif
                
                @if (auth()->user()->can('ver usuários') 
                    | auth()->user()->can('ver perfis') 
                    | auth()->user()->can('ver permissões'))
                <li class="nav-header">CONTROLE DE ACESSO</li>
                @endif
                @if (auth()->user()->can('ver usuários'))
                <li class="nav-item">
                    <a href="{{route('users.index')}}" class="nav-link {{ Request::is('admin/users*') ? 'active' : '' }}">
                        <i class="fas fa-users nav-icon"></i>
                        <p>Usuários</p>
                    </a>
                </li>
                @endif
                @if (auth()->user()->can('ver perfis'))
                <li class="nav-item">
                    <a href="{{route('roles.index')}}" class="nav-link {{ Request::is('admin/roles*') ? 'active' : '' }}">
                        <i class="fas fa-file-alt nav-icon"></i>
                        <p>Perfis</p>
                    </a>
                </li>
                @endif

                @if (auth()->user()->can('ver permissões'))
                <li class="nav-item">
                    <a href="{{route('permissions.index')}}" class="nav-link {{ Request::is('admin/permissions*') ? 'active' : '' }}">
                        <i class="fas fa-file-alt nav-icon"></i>
                        <p>Permissões</p>
                    </a>
                </li>
                @endif
                
                <li class="nav-header">CLASSIFICAÇÃO / TIPOS</li>
                @if (auth()->user()->can('ver hospedagens'))
                <li class="nav-item">
                    <a href="{{route('alojamentotipos.index')}}" class="nav-link {{ Request::is('admin/alojamentotipos*') ? 'active' : '' }}">
                        <i class="fas fa-hotel nav-icon"></i>
                        <p>Alojamento</p>
                    </a>
                </li>               
                <li class="nav-item">
                    <a href="{{route('restaurantetipos.index')}}" class="nav-link {{ Request::is('admin/restaurantetipos*') ? 'active' : '' }}">
                        <i class="fas fa-utensils nav-icon"></i>
                        <p>Restaurante</p>
                    </a>
                </li>               
                <li class="nav-item">
                    <a href="{{route('comercioindustriacpftipos.index')}}" class="nav-link {{ Request::is('admin/comercioindustriacpftipos*') ? 'active' : '' }}">
                        <i class="fas fa-industry nav-icon"></i>
                        <p>Comércio/Indústria/CPF</p>
                    </a>
                </li>               
                @endif

                @if (auth()->user()->can('ver locais evento'))                
                <li class="nav-item">
                    <a href="{{route('localeventotipos.index')}}" class="nav-link {{ Request::is('admin/localeventotipos*') ? 'active' : '' }}">
                        <i class="fas fa-map-marker nav-icon"></i>
                        <p>Locais Evento</p>
                    </a>
                </li>                
                @endif

                @if (auth()->user()->can('ver modais'))
                <li class="nav-item">
                    <a href="{{route('modaltipos.index')}}" class="nav-link {{ Request::is('admin/modaltipos*') ? 'active' : '' }}">
                        <i class="fas fa-plane nav-icon"></i>
                        <p>Modal</p>
                    </a>
                </li>                            
                @endif

                @if (auth()->user()->can('ver centros esportivos'))                
                <li class="nav-item">
                    <a href="{{route('centroesportivotipos.index')}}" class="nav-link {{ Request::is('admin/centroesportivotipos*') ? 'active' : '' }}">
                        <i class="fas fa-running nav-icon"></i>
                        <p>Centro Esportivo</p>
                    </a>
                </li>                
                @endif

               
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>