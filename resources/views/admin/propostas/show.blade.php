@extends('layouts.admin')
@section('title')
    Proposta nº {{ $proposta->id }} <span class="badge badge-warning">{{ $proposta->status }}</span>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin/propostas')}}">Propostas</a></li>
    <li class="breadcrumb-item active">Ver Proposta</li>
@endsection

@section('content')
<div class="row my-2">
    <div class="col">        
        <a class="btn btn-default mr-2" href="{{ url('admin/propostas') }}"><i class='fas fa-arrow-left'></i> Voltar</a>
        <a class="btn btn-primary mr-1" href="{{ url('admin/propostas') .'/'. $proposta->id . '/colocarEmAnalise' }}"><i class='fas fa-search'></i> Colocar em análise</a>
        <a class="btn btn-success mr-1" href="{{ url('admin/propostas') .'/'. $proposta->id . '/concluir' }}"><i class='fas fa-check'></i> Concluir proposta</a>
        <form action=' {{ route("propostas.destroy", $proposta->id) }} ' method="POST"                        
                onsubmit='return confirm("Tem certeza que deseja excluir?");' class='d-inline-block'>
            <input type="hidden" name="_method" value="DELETE">
            @csrf
            <button class="btn btn-danger"><i class="fas fa-trash"></i> Excluir proposta </button>
        </form>
    </div>
</div>
<div class="row mt-3">
    <div class='col-md-4'>
        <div class="card card-primary card-body">
            <div class='row mb-0'>                    
                <h2>Evento</h2>
            </div>
            <div class='row'>                    
                <label>Cidade</label>
                <input type="text" class="form-control" value="{{ $proposta->cidade->nome }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Nome do evento</label>
                <input type="text" class="form-control" value="{{ $proposta->nome_evento }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Tipo do evento</label>
                <input type="text" class="form-control" value="{{ $proposta->tipo_evento }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Data Início</label>
                <input type="text" class="form-control" value="{{ $proposta->inicio }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Data Fim</label>
                <input type="text" class="form-control" value="{{ $proposta->inicio }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Número de pessoas</label>
                <input type="text" class="form-control" value="{{ $proposta->pessoas }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Formato do evento</label>
                <input type="text" class="form-control" value="{{ $proposta->formato }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Salas paralelas</label>
                <input type="text" class="form-control" value="{{ $proposta->salas_paralelas }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Área de exposição em m<sup>2</sup></label>
                <input type="text" class="form-control" value="{{ $proposta->tipo_evento }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Necessita hospedagem?</label>
                <input type="text" class="form-control" value="{{ $proposta->necessita_hospedagem ? 'sim' : não }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Quantidade de quartos</label>
                <input type="text" class="form-control" value="{{ $proposta->quartos }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Passeios?</label>
                <input type="text" class="form-control" value="{{ $proposta->necessita_hospedagem ? 'sim' : não }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Atividades sociais?</label>
                <input type="text" class="form-control" value="{{ $proposta->necessita_hospedagem ? 'sim' : não }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Enviado em</label>
                <input type="text" class="form-control" value="{{ $proposta->created_at }}" readonly>                                 
            </div>
        </div>
                
    </div>
    <div class="col-md-4">                 
        <div class="card card-primary card-body">       
            <div class='row mb-0'>                    
                <h2>Solicitante</h2>
            </div>
            <div class='row'>                    
                <label>Nome</label>
                <input type="text" class="form-control" value="{{ $proposta->user->name }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Email</label>
                <input type="text" class="form-control" value="{{ $proposta->user->email }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Empresa</label>
                <input type="text" class="form-control" value="{{ $proposta->nome_empresa }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>CNPJ Empresa</label>
                <input type="text" class="form-control" value="{{ $proposta->cnpj }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Nome contato</label>
                <input type="text" class="form-control" value="{{ $proposta->nome_contato }}" readonly>                                 
            </div>
            <div class='row'>                    
                <label>Telefone contato</label>
                <input type="text" class="form-control" value="{{ $proposta->telefone_contato }}" readonly>                                 
            </div>
        
        </div>
    </div>
    
</div>
@endsection