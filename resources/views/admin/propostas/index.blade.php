@extends('layouts.admin')

@section('title')
    Propostas
    {{-- @if(auth()->user()->can('gerir propostas'))
    <a href="{{route('propostas.create')}}" class="btn btn-success"><i class="fas fa-plus"></i> Cadastrar</a>
    @endif --}}
@endsection

@section('css')
    <link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>    
    <script>
    
    $(".content").LoadingOverlay("show");
    $(document).ready(function() {
        
        $('table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ url('admin/propostas/datatable') }}",  
            dom: 'Bfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-file-excel"></i> Exportar para excel',
                    extend:'excelHtml5',
                    title:"MICE Rio | Propostas | exportado em {{date('m/d/Y')}}",
                    exportOptions: {
                        columns: [ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ]
                    }
                }
            ],
            columns: [
                {data: 'id'},                
                @if(auth()->user()->can('ver propostas'))
                {data: 'action', orderable: false, searchable: false},      
                @endif          
                {data: 'status'},
                {data: 'created_at'},
                {data: 'cidade'},                                
                {data: 'usuario'},                                
                {data: 'contato'},                                
                {data: 'empresa'},                                
                {data: 'nome_evento' },
                {data: 'tipo_evento' },
                {data: 'inicio' },
                {data: 'fim' },
                {data: 'pessoas' },
                {data: 'formato' },
                {data: 'salas_paralelas' },
                {data: 'area_exposicao_m2' },
                {data: 'necessita_hospedagem' },
                {data: 'quartos' },
                {data: 'passeios' },
                {data: 'atividades_sociais' },
            ],
            language: {
                url: "{{ asset('plugins/datatables/pt-br.json') }}"
            },
            initComplete: function(){
                $(".content").LoadingOverlay("hide");
            }
        });
    });
    </script>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active">Propostas</li>
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('success')}}
        </div>        
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-body">                
                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>ID</th>
                            @if(auth()->user()->can('ver propostas'))
                            <th>Ações</th>
                            @endif
                            <th>status</th>
                            <th>Enviado em</th>
                            <th>Cidade</th>
                            <th>Usuário</th>
                            <th>Contato</th>
                            <th>Empresa</th>
                            <th>Nome Evento</th>
                            <th>Tipo Evento</th>
                            <th>Início</th>
                            <th>Fim</th>
                            <th>Qtd Pessoas</th>
                            <th>Formato</th>
                            <th>Salas Paralelas?</th>
                            <th>Área Exposição (m²)</th>
                            <th>Hospedagem?</th>
                            <th>Qtd Quartos</th>
                            <th>Passeios?</th>
                            <th>Atividades Sociais?</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    
@endsection