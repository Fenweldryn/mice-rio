@extends('layouts.admin')
@section('title','Dashboard')

@section('breadcrumb')
    <li class="breadcrumb-item active">Home</li>    
@endsection

@section('css')
    <link href="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection

@section('js')
<script src="{{asset('plugins/chartjs/Chart.min.js')}}"></script>
<script src="{{asset('plugins/chartjs/chartjs-plugin-labels.min.js')}}"></script>
@if (auth()->user()->hasRole('admin'))
<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js')}}"></script>

<script>
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    $('#datepicker').datepicker({
        language:'pt-BR',
        clearBtn: true,
        format: "dd/mm/yyyy",
        orientation: "bottom auto",
        autoclose: true
    });

    loadVisitors();
    loadVisitedPages();
    loadCountries();

    $("#data-inicial, #data-fim").change(function (e) { 
        e.preventDefault();
        loadVisitors();
        loadVisitedPages();
        loadCountries();
    });
    
    function loadVisitors() {
        $('#visitors-chart').parent().LoadingOverlay('show');
        inicio = $("#data-inicial").val();
        fim = $("#data-fim").val();

        $.ajax({
            url:"{{url('')}}/admin/analytics/visitorsAndPageViews",
            type: 'post',
            data: {'inicio':inicio, 'fim':fim}
        }).always(function(response) {
            
            if (response) {
                var labels = [];
                var visitors = [];
                var pageViews = [];
                var ticksStyle = {
                    fontColor: '#495057',
                    fontStyle: 'bold'
                }
                var mode = 'index';
                var intersect = true;

                $.each(response, function (i, v) { 
                    labels.push(v.date);
                    visitors.push(v.visitors);
                    pageViews.push(v.pageViews);
                });
                
                
                var visitorsChart = new Chart($('#visitors-chart'), {
                    data   : {
                        labels  : labels,
                        datasets: [{
                            label               : "Visitantes",
                            type                : 'line',
                            data                : visitors,
                            backgroundColor     : 'transparent',
                            borderColor         : '#007bff',
                            pointBorderColor    : '#007bff',
                            pointBackgroundColor: '#007bff',
                            fill                : true
                        },
                            {
                            label               : "Visualizações",
                            type                : 'line',
                            data                : pageViews,
                            backgroundColor     : 'tansparent',
                            borderColor         : '#ced4da',
                            pointBorderColor    : '#ced4da',
                            pointBackgroundColor: '#ced4da',
                            fill                : false
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        tooltips           : {
                            mode     : mode,
                            intersect: intersect
                        },
                        hover              : {
                            mode     : mode,
                            intersect: intersect
                        },
                        legend             : {
                            display: true,
                            position: 'bottom'
                        },
                        scales             : {
                            yAxes: [{
                            // display: false,
                            gridLines: {
                                display      : true,
                                lineWidth    : '4px',
                                color        : 'rgba(0, 0, 0, .2)',
                                zeroLineColor: 'transparent'
                            },
                            ticks    : $.extend({
                                beginAtZero : true,
                                // suggestedMax: 200
                            }, ticksStyle)
                            }],
                            xAxes: [{
                            display  : true,
                            gridLines: {
                                display: false
                            },
                            ticks    : ticksStyle
                            }]
                        }
                    }
                })    
            } else {
            
            }
            $('#visitors-chart').parent().LoadingOverlay('hide');
        });
    }

    function loadVisitedPages() {
        $('#visitedPagesTable').parent().LoadingOverlay('show');
        $.ajax({
                url:"{{url('')}}/admin/analytics/mostVisitedPages",
                type: 'post',
                data: {'inicio':inicio, 'fim':fim}
            }).always(function(response) {
            if (response) {
                tbody = "";
                $.each(response, function (i, v) { 
                    tbody += "<tr>";
                    tbody += "<td>"+ v.url +"</td>";
                    tbody += "<td>"+ v.pageViews +"</td>";
                    tbody += "</tr>";
                });
                $("#visitedPagesTable tbody").html(tbody);
            } else {
            
            }
            $('#visitedPagesTable').parent().LoadingOverlay('hide');
        });        
    }

    function loadCountries() {
        $('#country-chart').parent().LoadingOverlay('show');
        $.ajax({
                url:"{{url('')}}/admin/analytics/visitorsCountry",
                type: 'post',
                data: {'inicio':inicio, 'fim':fim}
            }).always(function(response) {
            if (response) {
                var labels = [];
                var colors = [];
                var values = [];
                var mode = 'index';
                
                $.each(response, function (i, v) { 
                    labels.push(v.label);
                    colors.push(getRandomColor());
                    values.push(v.value);
                });
                if (Chart.instances[1] !== undefined) {
                    Chart.instances[1].destroy();   
                }
                var countryChart = new Chart($('#country-chart'), {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: values,
                            backgroundColor: colors,
                            label: 'Dataset 1'
                        }],
                        labels: labels
                    },
                    options: {
                        maintainAspectRatio: false,
                        legend             : {
                            display: true,
                            position: 'bottom'
                        },
                        plugins: {
                            labels: {
                                render: 'value',
                                fontColor: ['white', 'black'],
                                textShadow: true,
                                fontSize: 16,
                            }
                        }
                    },
                });    
            }
            $('#country-chart').parent().LoadingOverlay('hide');

        }); 
    }
    

    
    
</script>
@endif
@endsection

@section('content')
<div id='dashboard'>
@if (auth()->user()->hasRole('admin'))
    <div class="row">
        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box bg-primary">
            <span class="info-box-icon"><i class="fas fa-user"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">Usuários</span>
            <span class="info-box-number">
                {{$usuariosCount}}                  
            </span>
            </div>
        </div>
        </div>

        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box bg-danger">
            <span class="info-box-icon"><i class="fas fa-city"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">Cidades</span>
            <span class="info-box-number">
                {{$cidadesCount}}                  
            </span>
            </div>            
        </div>
        </div>

        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box bg-warning">
            <span class="info-box-icon"><i class="fas fa-landmark"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">Entidades</span>
            <span class="info-box-number">
                {{$entidadesCount}}                  
            </span>
            </div>            
        </div>
        </div>
        
        <!-- fix for small devices only -->
        <div class="clearfix hidden-md-up"></div>
        
        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3 bg-secondary">
            <span class="info-box-icon"><i class="fas fa-file"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">Propostas</span>
            <span class="info-box-number">{{$propostasCount}}  </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
        <div class="card col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-8">            
            <div class="row mt-2">
                <div class="col">
                    <div class="input-daterange input-group" id="datepicker">
                        <input type="text" class="input-sm form-control" id="data-inicial" value="{{ date('d/m/Y', strtotime('-1 week')) }}"/>
                        <span class="input-group-addon m-2">até</span>
                        <input type="text" class="input-sm form-control" id="data-fim"  value="{{ date('d/m/Y') }}"/>
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card card-primary">
                        <div class="card-header border-0">
                        
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title"><i class='fas fa-chart-line mr-2'></i> Visitantes e Visualizações</h3>
                        </div>
                        </div>
                        <div class="card-body border-top" style="height:300px">                
                        
                        <div class="position-relative mb-4 mt-4">
                            <canvas id="visitors-chart" height="200"></canvas>
                        </div>
                    
                        </div>
                    </div>            
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card card-primary">
                        <div class="card-header border-0">
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title"><i class='fas fa-th-list mr-2'></i> Páginas Mais Visitadas</h3>
                        </div>
                        </div>
                        <div class="card-body table-responsive p-0 border-top" style="height:300px">
            
                            <table class="table table-head-fixed" id='visitedPagesTable'>
                                <thead>
                                    <th>Página</th>
                                    <th>Visualizações</th>
                                </thead>
                                <tbody></tbody>
                            </table>
            
                        </div>
                    </div>    
                    
                </div>
                <div class="col">
                    <div class="card card-primary">
                        <div class="card-header border-0">                    
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title"><i class='fas fa-chart-line mr-2'></i> Acessos por País</h3>
                            </div>
                        </div>
                        <div class="card-body border-top" style="height:300px">                
            
                        <div class="position-relative mb-4 mt-4">
                            <canvas id="country-chart" height="200"></canvas>
                        </div>
                    
                        </div>
                    </div>               
                </div>        
            </div>
            
        </div>
        <div class="col">
            <div class="col p-0">
                <div class="card card-success">
                    <div class="card-header">
                        
                        <div class="row m-0 card-title">
                            <div class="col-8 p-0">
                                <i class='fas fa-door-open mr-2'></i> Total de Quartos
                            </div>
                            <div class="col-4 p-0 text-right">
                                <h3 class='m-0'>{{number_format($infoCidades->capacidade_total_quartos,0,',',' ')}}</h3>
                            </div>
                        </div>
                    </div>
                    <div class='card-body p-0' style="max-height:175px;overflow-y:scroll;white-space: nowrap;text-overflow:hidden">
                        <table class="table" id='info-totalLeitos' >
                            <tbody>
                                @foreach ($infoCidades->sortByDesc('capacidade_total_quartos') as $cidade)
                                <tr>
                                    <td>{{$cidade->nome}}</td>
                                    <td>{{number_format($cidade->capacidade_total_quartos,0,',',' ')}}</td>
                                </tr>
                                @endforeach                            
                            </tbody>
                        </table>

                    </div>
                </div>
                
            </div>
            <div class="col p-0">
                <div class="card card-success">
                    <div class="card-header">
                        
                        <div class="row m-0 card-title">
                            <div class="col-8 p-0">
                                <i class='fas fa-bed mr-2'></i> Total de Leitos
                            </div>
                            <div class="col-4 p-0 text-right">
                                <h3 class='m-0'>{{number_format($infoCidades->capacidade_total_leitos,0,',',' ')}}</h3>
                            </div>
                        </div>
                    </div>
                    <div class='card-body p-0' style="max-height:175px;overflow-y:scroll;white-space: nowrap;text-overflow:hidden">
                        <table class="table" id='info-totalLeitos' >
                            <tbody>
                                @foreach ($infoCidades->sortByDesc('capacidade_total_leitos') as $cidade)
                                <tr>
                                    <td>{{$cidade->nome}}</td>
                                    <td>{{number_format($cidade->capacidade_total_leitos,0,',',' ')}}</td>
                                </tr>
                                @endforeach                            
                            </tbody>
                        </table>

                    </div>
                </div>
                
            </div>
            <div class="col p-0">
                <div class="card card-success">
                    <div class="card-header">
                        
                        <div class="row m-0 card-title">
                            <div class="col-8 p-0">
                                <i class='fas fa-chair mr-2'></i> Total de Auditórios
                            </div>
                            <div class="col-4 p-0 text-right">
                                <h3 class='m-0'>{{number_format($infoCidades->capacidade_maior_auditorio,0,',',' ')}}</h3>
                            </div>
                        </div>
                    </div>
                    <div class='card-body p-0' style="max-height:175px;overflow-y:scroll;white-space: nowrap;text-overflow:hidden">
                        <table class="table" id='info-totalLeitos' >
                            <tbody>
                                @foreach ($infoCidades->sortByDesc('capacidade_maior_auditorio') as $cidade)
                                <tr>
                                    <td>{{$cidade->nome}}</td>
                                    <td>{{number_format($cidade->capacidade_maior_auditorio,0,',',' ')}}</td>
                                </tr>
                                @endforeach                            
                            </tbody>
                        </table>

                    </div>
                </div>
                
            </div>
           
        </div>
        
    </div>
    <div class="row">
        
    </div>
    
@endif
</div>
@endsection