@extends('layouts.admin')
@section('title','Cadastrar Permuta')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin/permutas')}}">Permutas</a></li>
    <li class="breadcrumb-item active">Cadastrar Permuta</li>
@endsection

@section('js')
    
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">                 
        <form method="POST" action="{{ url('/admin/permutas', $permuta->id) }}">    
            @method('patch')        
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif  
            
            <div class="card card-primary card-body">       
                <div class="form-group">
                    <label for="">Associado</label>
                    <select id="" class="form-control" name="associado_id">
                        @foreach ($associados as $associado)
                            <option value='{{ $associado->id }}' {{ (old('associado_id') ?? $permuta->associado_id) == $associado->id ? 'selected' : '' }}>{{ $associado->nome }}</option>                            
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Entidade</label>
                    <select id="" class="form-control" name="entidade_id">
                        @foreach ($entidades as $entidade)
                            <option value='{{ $entidade->id }}' {{ ($permuta->entidade_id ?? old('entidade_id')) == $entidade->id ? 'selected' : '' }}>{{ $entidade->nome_fantasia }}</option>                            
                        @endforeach
                    </select>
                </div>

                <div class='form-group'>                    
                    <label>Descrição</label>
                    <input id="descricao" value="{{ old('descricao') ?? $permuta->descricao }}" type="text" class="form-control{{ $errors->has('descricao') ? ' is-invalid' : '' }}" name="descricao" required>
                </div> 

                <div class='form-group'>                    
                    <label>Quantidade por Mês</label>
                    <input id="quantidade_por_mes" value="{{ old('quantidade_por_mes') ?? $permuta->quantidade_por_mes }}" type="number" class="form-control{{ $errors->has('quantidade_por_mes') ? ' is-invalid' : '' }}" name="quantidade_por_mes" required>
                </div> 
                
                <div class="row mt-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i> Cadastrar </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection