<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{ config('app.name', 'Laravel') }} - Registrar</title>

		<!-- Fonts -->
		<link rel="dns-prefetch" href="//fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link href="{{asset('plugins/animate/animate.min.css')}}" rel="stylesheet">
		<link href="{{asset('plugins/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
		<link href="{{asset('plugins/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
		<link href="{{asset('plugins/lightbox/css/lightbox.min.css')}}" rel="stylesheet">
		<link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
<body> 
    <style>
        body {
            background: #f5f8fd url(/img/rj.jpg) center top fixed;   
            height: 100vh;
        }
        .login {
            margin-top: 15vh;
        }
    </style>
	<header id="header" class='keep-header header-scrolled'>
		@include('index_navbar')
		@section('social-bar',"")
	</header> 

	<div class="clearfix"></div>
	<main id="main">   	
    
        <div class="container login">            
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Registrar') }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <input type="hidden" name="mordor" value="">

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Senha') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="mínimo de 8 caracteres">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Senha') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Registrar') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
	</main>

	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
	
	<!-- JavaScript Libraries -->
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="{{asset('plugins/easing/easing.min.js')}}"></script>
	<script src="{{asset('plugins/mobile-nav/mobile-nav.js')}}"></script>
	<script src="{{asset('plugins/wow/wow.min.js')}}"></script>
	<script src="{{asset('plugins/waypoints/waypoints.min.js')}}"></script>
	<script src="{{asset('plugins/counterup/counterup.min.js')}}"></script>
	<script src="{{asset('plugins/owlcarousel/owl.carousel.min.js')}}"></script>
	<script src="{{asset('plugins/isotope/isotope.pkgd.min.js')}}"></script>
	<script src="{{asset('plugins/lightbox/js/lightbox.min.js')}}"></script>  
	<script src="{{asset('js/main.js')}}"></script>
</body>
</html>