<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>Solicitar Proposta para {{$cidade->nome}}</title>

		<!-- Fonts -->
		<link rel="dns-prefetch" href="//fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link href="{{asset('plugins/animate/animate.min.css')}}" rel="stylesheet">
		<link href="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
		<link href="{{asset('css/style.css')}}" rel="stylesheet">
		<style>
			label {
				margin-bottom: 0;
			}
		</style>
</head>
<body>
	
	<header id="header" class='keep-header'>
		@include('index_navbar')
		@section('social-bar',"")
	</header> 
	
	<div class="clearfix"></div>
	<main id="main">   	
		<section id="proposta" class="section-bg ">			
			<div class="container-fluid">					
				<main role="main" class="pb-2 bg-light border-round">
					<div class="row">
						<div class="result-count">
							<a href="{{url('')}}/cidades/{{$cidade->slug}}"> <b><i class='fas fa-arrow-left'></i> Voltar</b> </a>
						</div>							
					</div>
					<div class="row">
						<div class="col-md-7">    
						@if(Session::has('success'))
							<div class="alert alert-success alert-dismissible fade show" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								{{Session::get('success')}}
							</div>        
						@endif
						<h1 style="font-weight: 700; font-size: 3rem;">Solicitar proposta para evento</h1>

						<form method="POST" action="{{ url('/propostas') }}">            
							@csrf
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif  
							<input type="hidden" name="cidade_id" value="{{$cidade->id}}">
							<div class="card card-primary card-body mb-3 p-0 pt-3">       
								<div class='row'>  
									<div class="col-md-6">
										<label>CNPJ</label>
										<input id="cnpj" value="{{ old('cnpj') }}" type="text" class="form-control{{ $errors->has('cnpj') ? ' is-invalid' : '' }}" name="cnpj" required onfocusout="buscarDadosCnpj()">
									</div>      
									<div class="col-md-6">
										<label>Nome da Empresa</label>
										<input id="nome_empresa" value="{{ old('nome_empresa') }}" type="text" class="form-control{{ $errors->has('nome_empresa') ? ' is-invalid' : '' }}" name="nome_empresa" required >
									</div>        
								</div>
								<div class='row'>    
									<div class="col-md-6">
										<label>Nome para Contato</label>
										<input id="nome_contato" value="{{ old('nome_contato') }}" type="text" class="form-control{{ $errors->has('nome_contato') ? ' is-invalid' : '' }}" name="nome_contato" required >
									</div>                
									<div class="col-md-6">
										<label>Telefone para Contato</label>
										<input id="telefone_contato" value="{{ old('telefone_contato') }}" type="text" class="form-control{{ $errors->has('telefone_contato') ? ' is-invalid' : '' }}" name="telefone_contato" required >
									</div>                
								</div>
								<div class='row col'>                    
									<label>Nome do Evento</label>
									<input id="nome_evento" value="{{ old('nome_evento') }}" type="text" class="form-control{{ $errors->has('nome_evento') ? ' is-invalid' : '' }}" name="nome_evento" required >

									@if ($errors->has('nome_evento'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('nome_evento') }}</strong>
										</span>
									@endif                    
								</div>

								<div class='row col'>                    
									<label>Tipo Evento</label>
									<select id="tipo_evento" class="form-control" name="tipo_evento" required>
										<option value=""></option>
										<option value="Cultural">Cultural</option>
										<option value="Esportivo">Esportivo</option>
										<option value="Congresso">Congresso</option>
										<option value="Convenção">Convenção</option>
										<option value="Feira">Feira</option>
										<option value="Outros">Outros</option>
									</select>								
									<input class="form-control" type="text" id="tipo_evento_outros" name="tipo_evento_outros" style="display:none">
									@if ($errors->has('tipo_evento'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('tipo_evento') }}</strong>
										</span>
									@endif                    
								</div>

								<div class='row col'>                    
									<label>Período</label>		
									<div class="input-daterange input-group" id="datepicker">				<input type="text" class="input-sm form-control" value="{{ old('inicio') }}" name="inicio" required/>
									<span class="my-auto">até</span>
									<input type="text" class="input-sm form-control" value="{{ old('fim') }}" name="fim" required/>
									</div>
									@if ($errors->has('periodo'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('periodo') }}</strong>
										</span>
									@endif                    
								</div>

								<div class='row col'>                    
									<label>Número de Pessoas</label>
									<input id="pessoas" value="{{ old('pessoas') }}" type="text" class="form-control{{ $errors->has('pessoas') ? ' is-invalid' : '' }}" name="pessoas" required >

									@if ($errors->has('pessoas'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('pessoas') }}</strong>
										</span>
									@endif                    
								</div>        
								
								<div class='row col'>                    
									<label>Número de Salas Paralelas</label>
									<input id="salas_paralelas" value="{{ old('salas_paralelas') }}" type="numeric" class="form-control{{ $errors->has('salas_paralelas') ? ' is-invalid' : '' }}" name="salas_paralelas" required >

									@if ($errors->has('salas_paralelas'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('salas_paralelas') }}</strong>
										</span>
									@endif                    
								</div>  
								
								<div class='row col'>                    
									<label>Formato</label>
									<select id="formato" class="form-control" name="formato" required>
										<option value=""></option>
										<option value="Coquetel">Coquetel</option>
										<option value="Teatro">Teatro</option>
										<option value="Escola">Escola</option>
										<option value="Espinha de peixe">Espinha de peixe</option>
										<option value="Banquete">Banquete</option>
										<option value="Outros">Outros</option>
									</select>		
									<input class="form-control" type="text" id="formato_outros" name="formato_outros" style="display:none">

									@if ($errors->has('formato'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('formato') }}</strong>
										</span>
									@endif                    
								</div>     

								<div class='row col'>                    
									<label>Área de Exposição Requerida (m2)</label>
									<input id="area_exposicao_m2" value="{{ old('area_exposicao_m2') }}" type="numeric" class="form-control{{ $errors->has('area_exposicao_m2') ? ' is-invalid' : '' }}" name="area_exposicao_m2" required >

									@if ($errors->has('area_exposicao_m2'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('area_exposicao_m2') }}</strong>
										</span>
									@endif                    
								</div>     

								<div class='row col'>                    
									<label>Necessita Hospedagem?</label>
									<div class="form-check w-100">
										<input class="form-check-input" type="radio" name="necessita_hospedagem" id="necessita_hospedagem1" value="1">
										<label class="form-check-label" for="necessita_hospedagem1">Sim</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="necessita_hospedagem" id="necessita_hospedagem2" value="0">
										<label class="form-check-label" for="necessita_hospedagem2">Não</label>
									</div>


									@if ($errors->has('necessita_hospedagem'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('necessita_hospedagem') }}</strong>
										</span>
									@endif                    
								</div>     

								<div class='row col'>                    
									<label>Número de Quartos</label>
									<input id="quartos" value="{{ old('quartos') }}" type="text" class="form-control{{ $errors->has('quartos') ? ' is-invalid' : '' }}" name="quartos" required >

									@if ($errors->has('quartos'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('quartos') }}</strong>
										</span>
									@endif                    
								</div>     

								<div class='row col'>                    
									<label>Atividades Sociais?</label>
									<div class="form-check w-100">
										<input class="form-check-input" type="radio" name="atividades_sociais" id="atividades_sociais1" value="1">
										<label class="form-check-label" for="atividades_sociais1">Sim</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="atividades_sociais" id="atividades_sociais2" value="0">
										<label class="form-check-label" for="atividades_sociais2">Não</label>
									</div>

									@if ($errors->has('atividades_sociais'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('atividades_sociais') }}</strong>
										</span>
									@endif                    
								</div>     

								<div class='row col'>                    
									<label>Passeios?</label>
									<div class="form-check w-100">
										<input class="form-check-input" type="radio" name="passeios" id="passeios1" value="1">
										<label class="form-check-label" for="passeios1">Sim</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="passeios" id="passeios2" value="0">
										<label class="form-check-label" for="passeios2">Não</label>
									</div>

									@if ($errors->has('passeios'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('passeios') }}</strong>
										</span>
									@endif                    
								</div>     
								
							</div>
							<div class="row mt-4">
								<button type="submit" class="btn-main w-100">
									Enviar Solicitação</button>
							</div>
						</form>
						</div>		
						<div class="col-md-5">
							<h1 class="cidade-selecionada" style="font-weight: 700; font-size: 3rem;">Cidade Selecionada</h1>
							<div class="card w-100 row">
								<div class='card-header m-0 p-0 col'>
									<h5 class="card-title text-shadow m-4 text-center"><b class='card-titulo'><i class="fas fa-map-marker-alt"></i> {{$cidade->nome. ' - ' .$cidade->estado_sigla }}</b></h5>
									<img class="img-fluid" src="{{$cidade->getFirstMediaUrl() ? $cidade->getFirstMediaUrl() : "/img/macae3.jpg"}}" alt="">
								</div>										
							</div>			
							<div class="row">
								<div class="col p-0">
									<div class="google-maps wow fadeInUp">
									<iframe id='mapa' width="100%" height="100%" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?output=embed&z=10&q={{str_replace(",", "", str_replace(" ", "+", $cidade->nome." - RJ"))}}"></iframe>
									</div>	
								</div>
							</div>

						</div>					
					</div>

				</main>
			</div>
			
		</section><!-- #services -->
	</main>

	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
	
	<div id="preloader"></div>

	<!-- JavaScript Libraries -->
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="{{asset('plugins/easing/easing.min.js')}}"></script>
	<script src="{{asset('plugins/mobile-nav/mobile-nav.js')}}"></script>
	<script src="{{asset('plugins/wow/wow.min.js')}}"></script>
	<script src="{{asset('js/main.js')}}"></script>
	<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js')}}"></script>
	<script>
		$('select').SumoSelect({placeholder:"Selecione uma opção",forceCustomRendering: true});            
		$('#datepicker').datepicker({language:'pt-BR',clearBtn: true});
		function buscarDadosCnpj() {
            if ($("#cnpj").val()) {
                cnpj = $("#cnpj").val().match(/\d+/g).join('');
                $.ajax({
                    type: "get",
                    contentType: "application/json",
                    dataType: "jsonp",
                    url: "https://www.receitaws.com.br/v1/cnpj/"+cnpj+"?callback=processarDadosCnpj"
                });
            }
        }
        function processarDadosCnpj(dados) {
            console.log(dados);
            $("#nome_empresa").val(dados.nome);
            $("#telefone_contato").val(dados.telefone);
        }
	</script>
</body>
</html>
