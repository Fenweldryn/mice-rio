<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	@if (app()->environment() == 'production')
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149360743-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-149360743-1');
	</script>
	@endif

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>MICE Rio - Resultado da Busca</title>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{asset('plugins/animate/animate.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
<body> 	
	<header id="header" class='keep-header header-scrolled'>
		@include('index_navbar')
		@section('social-bar',"")
	</header> 

	<div class="clearfix"></div>
	<main id="main">   	
		<section id="resultado" class="section-bg">			
			<div class="container-fluid pb-1">					
				<nav class="breadcrumb">
					<a class="breadcrumb-item" href="{{url('')}}">Home</a>
					<span class="breadcrumb-item active">Resultado da busca</span>
				</nav>
				<div class="row mb-5">
					<nav class="col-md-2 d-none d-md-block bg-light sidebar border-round mr-3 h-100" style="position:sticky; top: 85px;">
					<div class="sidebar-sticky mb-4" >	
						<form action={{route('busca')}} method="GET">									
							
						<h5 class="sidebar-heading d-flex mt-4 mb-4 font-weight-bold">
							FILTROS
						</h5>						
						
						<label class="mb-0">Distância da capital</label>
						<select class="form-control align-bottom" name="distancia_capital" id="">	
						<option value="0">indiferente</option>
						<option value="80" {{$parametros->distancia_capital == 80 ? "selected" : ""}}>até 80km</option>
						<option value="160" {{$parametros->distancia_capital == 160 ? "selected" : ""}}>até 160km</option>
						<option value="240" {{$parametros->distancia_capital == 240 ? "selected" : ""}}>até 240km</option>
						<option value="241" {{$parametros->distancia_capital == 241 ? "selected" : ""}}>Mais de 240km</option>
						</select>

						<label class="mt-3 mb-0">Acesso</label>
						<select multiple="multiple" class="form-control" name="acesso[]" id="">
						@foreach ($modalTipos as $item)
							<option value="{{$item->id}}" {{is_array($parametros->acesso) ? (in_array($item->id,$parametros->acesso) ? "selected" : "") : ""}}>{{$item->nome}}</option>
						@endforeach
						</select>	
					
						<label class="mt-3 mb-0">Tipo de Local</label>
						<select multiple="multiple" class="form-control align-bottom" name="tipo_local[]" id="">
						@foreach ($localEventoTipos as $tipo)							
							<option value="{{$tipo->id}}" {{is_array($parametros->tipo_local) ? (in_array($tipo->id,$parametros->tipo_local) ? "selected" : "") : ""}}>{{$tipo->nome}}</option>
						@endforeach	
						</select>

						<label class="mt-3 mb-0">Capacidade Auditório</label>
						<input type="text" maxlength="7" min="0" class="form-control" name="capacidade_auditorio" id="" placeholder="" value={{$parametros->capacidade_auditorio == 0 ? "" : $parametros->capacidade_auditorio}}>
						
						<label class="mt-3 mb-0">Tamanho mínimo do espaço (m2)</label>
						<input type="text" maxlength="7" min="0" class="form-control" name="tamanho_espaco" id="" placeholder="" value={{$parametros->tamanho_espaco == 0 ? "" : $parametros->tamanho_espaco}}>
						
						<label class="mt-3 mb-0">Capacidade Banquete</label>
						<input type="text" maxlength="7" min="0" class="form-control" name="capacidade_banquete" id="" placeholder="" value={{$parametros->capacidade_banquete == 0 ? "" : $parametros->capacidade_banquete}}>

						
						<button type="submit" name="" id="" class="btn btn-success btn-lg btn-buscar mt-3 w-100" role="button"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>	
						</form>
					</div>
					</nav>

					<main role="main" class="col px-4 bg-light border-round">
						<div class="result-count">
							<b>{{count($resultado)}}</b> {{count($resultado) == 1 ? 'resultado encontrado' : 'resultados encontrados'}}
						</div>
						@foreach ($resultado as $cidade)
						<div class="card mb-3 box-shadow-blur" style="">
						<div class="row no-gutters mb-0">
							<div class="col-md-4">
							<img src="{{$cidade->getFirstMediaUrl() ? $cidade->getFirstMediaUrl() : "/img/rj.jpg"}}" class="img-fluid" alt="{{$cidade->nome}}">
							</div>
							<div class="col-md-8">
							<div class="card-body">
								<h1 class="card-title">{{$cidade->nome}}</h1>
								<p class="card-text text-justify">
									@if (strlen($cidade->descricao) > 350)
									{!! nl2br(substr($cidade->descricao,0,350)) !!}<span id="dots">...</span>
										
									@else
									{!! nl2br($cidade->descricao) !!}
									@endif
								</p>
							</div>
							<div class='card-footer card-horizontal-footer d-flex align-items-center'> 
								<div class="col d-none d-md-block d-lg-block">                       
									<div class="float-left text-center mr-4" data-toggle="tooltip" data-placement="top" title="Distância da Capital">
										<i class="fas fa-road fa-2x"></i>
										<span class="d-block mb-0">{{$cidade->distancia_capital}} km</span>
									</div>
									
									<div class="float-left text-center mr-4" data-toggle="tooltip" data-placement="top" title="Capacidade Maior Auditório">
										<i class="fas fa-chair fa-2x"></i>
										<span class="d-block mb-0">{{$cidade->capacidade_maior_auditorio}}</span>
									</div>
									
									<div class="float-left text-center mr-4" data-toggle="tooltip" data-placement="top" title="Capacidade maior espaço">
										<i class="far fa-square fa-2x"></i>
										<span class="d-block mb-0">{{$cidade->capacidade_maior_espaco}} m²</span>
									</div>	

									<div class="float-left text-center mr-4" data-toggle="tooltip" data-placement="top" title="capacidade do maior banquete">
										<i class="fas fa-utensils fa-2x"></i>
										<span class="d-block mb-0">{{$cidade->capacidade_maior_banquete}}</span>
									</div>	  

									<div class="float-left text-center mr-4" data-toggle="tooltip" data-placement="top" title="Capacidade de leitos do maior hotel">
										<i class="fas fa-hotel fa-2x"></i>
										<span class="d-block mb-0">{{$cidade->capacidade_quartos_maior_hotel}}</span>
									</div>	

								</div>              
								
								<div class='col-12 col-md-3 col-lg-3'>
									<a name="" id="" class="btn btn-primary w-100" href="{{url('cidades')."/$cidade->slug"}}" role="button">Mais informações</a>
								</div>
								
							</div>
							
							</div>
						</div>
						</div>					
								
						@endforeach
						
					</main>
				</div>
				
			</div>
			
		</section><!-- #services -->
	</main>

	<!--==========================
		Footer
	============================-->
	<footer id="footer" class="section-bg">
		@include('index_footer')
	</footer><!-- #footer -->

	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
	
	<div id="preloader"></div>

	<!-- JavaScript Libraries -->
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="{{asset('plugins/easing/easing.min.js')}}"></script>
	<script src="{{asset('plugins/mobile-nav/mobile-nav.js')}}"></script>
	<script src="{{asset('plugins/wow/wow.min.js')}}"></script>
	<script src="{{asset('js/main.js')}}"></script>
	<script>
		$('select').SumoSelect({
			placeholder: 'Selecione',
			selectAll : true,
			captionFormatAllSelected: "{0} todos selecionados!",
			locale: ['Ok', 'Cancelar', "Todos"],
		});
		$('input[name=capacidade]').change(function (e) { 		
			if ($(this).val() < 0) {
				$(this).val('');			
			}		
		});		
	</script>
</body>
</html>
