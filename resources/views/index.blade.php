<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	@if (app()->environment() == 'production')
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149360743-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-149360743-1');
	</script>		
	@endif
	
	<meta name="title" content="MICE Rio - Encontre a cidade perfeita para seu evento">
	<meta name="description" content="Encontre a cidade perfeita para seu evento. Meetings Incentives Conferences and Exhibitions Rio de Janeiro.">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Scripts -->
	<script src="{{ mix('js/app.js') }}"></script>

	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Styles -->
	<link href="{{ mix('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/animate/animate.min.css') }}" rel="stylesheet">
	<link href="{{ mix('css/style.css') }}" rel="stylesheet">	
</head>
<body>
	<header id="header">
	@include('index_navbar')
	@section('social-bar')
		<div id="topbar">
			<div class="container">
			<div class="social-links">
				<a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
				<a href="#" class="facebook"><i class="fab fa-facebook"></i></a>
				<a href="#" class="linkedin"><i class="fab fa-linkedin"></i></a>
				<a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
			</div>
			</div>
		</div>
	@endsection
	</header>
  <!--==========================
	Intro Section
  ============================-->

	<section id="intro" class="clearfix">
	<div class="container-fluid h-100">
		<div class="row">
			<div class="intro-info order-md-first order-last mt-5 wow fadeIn" style="visibility: hidden">
				<h2 style="font-size: 5em;">ENCONTRE A CIDADE PERFEITA<br>PARA SEU EVENTO</h2>
				<div class="">
					<a href="#cidades" class="btn-main scrollto mr-2 wow fadeInUp"><i class="fas fa-search"></i> Cidades</a>	<a href="#buscador" class="btn-main scrollto mr-2 wow fadeInUp"><i class="fas fa-search"></i> Buscador</a>	
				</div>
			</div>  
		</div>
	</div>
	</section>

	<section class="scroll">
		<a href="#sobre" class='scrollto'><span></span>Sobre</a>
	</section>
	
	<section id="sobre" class="clearfix" style="background-color:white; top:200px">
		<div class="container-fluid py-4">	
		</div>
			<header class="section-header">
				<h3>Sobre o MICE Rio</h3>
				<div class="justify-content-center d-flex">
					<a href="https://www.fcvbrj.org.br/new/"><img src="/img/Logo-FCVB-RJ.png" class="img-fluid"></a>	
				</div>	
				<p class="mt-3 pb-3">Este portal é um iniciativa da <a href="https://www.fcvbrj.org.br/new/">Federação dos Convention & Visitors Bureaus do Estado do Rio de Janeiro</a>, coordenado e idealizado pela empresa Vaniza Schuler Consultoria, a partir da referência de portais similares, existentes em destinos turísticos  internacionais benchmarking e desenvolvido pela <a href="https://krotec.com.br">Krotec</a>.</p>

				<p class="mb-4">A finalidade do Portal mice-rio.com.br é disponibilizar informações, de forma rápida e eficaz, das estruturas para realização de eventos disponíveis no Estado do Rio de Janeiro, a fim de atender demandas dos organizadores e promotores de congressos, convenções, feiras, campeonatos e espetáculos. Em sua primeira fase, o portal foi alimentado pelos C&VB das cidades Fluminenses de Paraty, Angra dos Reis, Região de Visconde de Mauá, Região do Vale do Café, Petrópolis, Teresópolis, Nova Friburgo, Guapimirim, Niterói, Maricá, Saquarema, Cabo Frio, Armação dos Búzios, Região da Serra & Mar, Rio das Ostras, Macaé e Campos dos Goytacazes, além da Capital Carioca Rio.</p>
			</header>				
		</div>
	</section>

	<div style="background: #f5f8fd url(/img/rj.jpg) center top no-repeat fixed; background-size: cover; height:75px"></div>

	<section id="cidades" class="clearfix" style="background-color:white">
		<div class="container-fluid py-4">	
		</div>
			<header class="section-header">
					<h3>Cidades</h3>
					<p class="pb-4">Busque ou clique em uma cidade para ver mais informações.</p>
			</header>
			<div class="row justify-content-center">
				<div class="col-md-4 input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-search"></i></span>
					</div>
					<input id="busca-cidade" onkeyup="filtro()" class="form-control " type="text" name="" placeholder="digite o nome de uma cidade...">
					<p id='row-cidades-nada-encontrado' style="display:none;width:100%" class='text-center mt-5'>Nenhum resultado encontrado.</p>

				</div>			
			</div>
			<div class="row no-gutters">	
				<div class="col-md-8 col-12">
					<div class="row no-gutters justify-content-center" id='row-cidades'>
						@foreach ($cidades as $cidade)						
						<a class="col-md-2 m-3 card w-100 card-cidade" href="{{url('')}}/cidades/{{$cidade->slug}}">
							<div class='card-header m-0 p-0'>
								<img class="img-fluid img-fit lazy" data-src="{{$cidade->foto_principal ? $cidade->getMedia()->where('id',$cidade->foto_principal)->first()->getUrl() : ($cidade->getFirstMediaUrl() ? $cidade->getFirstMediaUrl() : '/img/macae3.jpg')}}" alt="{{$cidade->nome}}">
							</div>
							<div class="card-body text-center">
								<h5 class="card-title text-shadow mb-0"><b class='card-titulo'><i class="fas fa-map-marker-alt"></i> {{$cidade->nome}}</b></h5>
							</div>			
						</a>				
						@endforeach				
						
					</div>
				</div>		

				<div class="col-md-4 d-none d-sm-block">
					<div class="google-maps mt-3" style="position: sticky;top: 100px;">
					<iframe id='mapa' width="100%" height="100%" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?output=embed&z=7&q={{str_replace(",", "", str_replace(" ", "+", "Estado de Rio de Janeiro"))}}"></iframe>
					</div>		
				</div>	
				
				

			</div>							
		</div>
	</section>
	
 	<div style="background: #f5f8fd url(/img/rj.jpg) center top no-repeat fixed; background-size: cover; height:75px"></div>

  
   <!--==========================
      Buscador
    ============================-->
   	<section id="buscador" class="section-bg py-4">
	   <div class="container-fluid mb-5">						
		  	<header class="section-header">
			  <h3>Buscador</h3>
			  <p>Encontre uma cidade de acordo com as características do seu evento.</p>
			</header>
			<form action={{route('busca')}} method="GET">									
				<div class='row justify-content-center'>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="card box-shadow-blur">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif		
							<div class="card-header bg-primary text-white font-size-16 font-weight-bold">
								LOGÍSTICA E ACESSO
							</div>
							<div class="card-body">																
								<div class="form-group">
									<label for="">Distância da capital</label>
									<select class="form-control align-bottom" name="distancia_capital" id="">					
									<option value="0">indiferente</option>
									<option value="80">até 80km</option>
									<option value="160">até 160km</option>
									<option value="240">até 240km</option>
									<option value="241">Mais de 240km</option>
									</select>
								</div>
	
								<div class="form-group">
									<label for="">Acesso</label>
									<select multiple="multiple" class="form-control" name="acesso[]" id="">
									@foreach ($modalTipos as $modal)
										<option value="{{$modal->id}}">{{$modal->nome}}</option>
									@endforeach
									</select>								
								</div>													
								
							</div>
						</div>						
					</div>
				</div>		
				<div class="row justify-content-center">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="card box-shadow-blur">
							<div class="card-header bg-primary text-white font-size-16 font-weight-bold">
								INFRAESTRUTURA
							</div>
							<div class="card-body">								
								<div class="form-group">
									<label for="">Tipo de local</label>
									<select multiple="multiple" class="form-control align-bottom" name="tipo_local[]" id="">
									@foreach ($localEventoTipos as $tipo)
										<option value="{{$tipo->id}}">{{$tipo->nome}}</option>
									@endforeach											

									</select>
								</div>
	
								<div class="form-group">
									<label for="">Capacidade do Auditório</label>
									<input type="text" maxlength="7" min="0"
										class="form-control" name="capacidade_auditorio" id="" placeholder="">
								</div>													
								<div class="form-group">
									<label for="">Tamanho Mínimo do Espaço (m2)</label>
									<input type="text" maxlength="7" min="0"
										class="form-control" name="tamanho_espaco" id="" placeholder="">
								</div>			
								<div class="form-group">
									<label for="">Capacidade do Banquete</label>
									<input type="text" maxlength="7" min="0"
										class="form-control" name="capacidade_banquete" id="" placeholder="">
								</div>										
								
							</div>
						</div>
					</div>										
				</div>										

				<div class='row mb-0 justify-content-center'>					
					<div class="col-md-6 ">
						<button type="submit" name="" id="" class="w-100 btn-main" role="button"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>	
					</div>
				</div>						
			</form>	
			
		</div>
	</section>

	<div style="background: #f5f8fd url(/img/rj.jpg) center top no-repeat fixed; background-size: cover; height:200px"></div>
	  
	<!--==========================
		Footer
  	============================-->
	<footer id="footer" class="section-bg">
	@include('index_footer')
	</footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  
  {{-- <div id="preloader"></div> --}}

  <!-- JavaScript Libraries -->
  <script src="{{asset('plugins/easing/easing.min.js')}}"></script>
  <script src="{{asset('plugins/mobile-nav/mobile-nav.js')}}"></script>
  <script src="{{asset('plugins/wow/wow.min.js')}}"></script>
  <script src="{{asset('js/main.js')}}"></script>
  <script src="{{asset('js/lazyload.min.js')}}"></script>
  <script>
	var lazyLoadInstance = new LazyLoad({
		elements_selector: ".lazy"
	});
	$('select').SumoSelect({
		placeholder: 'Selecione',
		selectAll : true,
		captionFormatAllSelected: "{0} todos selecionados!",
		locale: ['Ok', 'Cancelar', "Todos"],
	});

	$('input[name=capacidade]').change(function (e) { 		
		if ($(this).val() < 0) {
			$(this).val('');			
		}		
	});

	function filtro() {
		// Declare variables
		var input, filter, span, maindiv, a, i, txtValue;
		input = document.getElementById('busca-cidade');
		filter = input.value.trim().toUpperCase();
		maindiv = document.getElementById("row-cidades");
		notfound = document.getElementById("row-cidades-nada-encontrado");
		span = maindiv.getElementsByTagName('a');
		notfound.style.display = "none";
		hasfound = false;

		for (i = 0; i < span.length; i++) {
			title = span[i].getElementsByTagName("h5")[0];
			txtValue = title.textContent || title.innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				span[i].style.display = "";
				hasfound = true;
			} else {
				span[i].style.display = "none";
			}
		}

		if(!hasfound) {
			notfound.style.display = "";
		}
	}

	$("#row-cidades a").hover(function () {
		nomeCidade = $(this).find('.card-titulo').first().text();
		nomeCidade = nomeCidade.replace(" ", "+");
		nomeCidade = nomeCidade.replace(",", "");
		url = "https://maps.google.com/maps?output=embed&z=8&q=" + nomeCidade;
		$("#mapa").attr('src', url);
	},function() {		
		// nomeCidade = "Estado de Rio de Janeiro";
		// nomeCidade = nomeCidade.replace(" ", "+");
		// nomeCidade = nomeCidade.replace(",", "");		
		// url = "https://maps.google.com/maps?output=embed&z=7&q=" + nomeCidade;
		// $("#mapa").attr('src', url);
	});
	

  </script>

</body>
</html>
