<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>Proposta Enviada com Sucesso</title>

		<!-- Fonts -->
		<link rel="dns-prefetch" href="//fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link href="{{asset('plugins/animate/animate.min.css')}}" rel="stylesheet">
		<link href="{{asset('plugins/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
		<link href="{{asset('plugins/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
		<link href="{{asset('plugins/lightbox/css/lightbox.min.css')}}" rel="stylesheet">
		<link href="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
		<link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
<body>	
	<header id="header" class='keep-header'>
		@include('index_navbar')
		@section('social-bar',"")
	</header> 

	<div class="clearfix"></div>
	<main id="main">   	
		<section id="resultado" class="section-bg" style="height: 100vh">			
				<div class="container">					
					<h1 style="color: white;
					margin-bottom: 40px;
					font-size: 5em;
					font-weight: 700;
					text-shadow: 4px 4px 4px black;"
					>SOLICITAÇÃO ENVIADA COM SUCESSO.</h1>
				<main role="main" class="row p-4 bg-light border-round">										
					<div class="col">
						<p style="font-size: 2em;color: #82858a;">
							Agradecemos seu contato.<br>
							Em até 48 horas o escritório responsável entrará em contato.
						</p>							
					</div>

					<div class="row w-100">
						<div class="col">
							<a href="/" class='btn-main w-100 mt-4 mb-2'>PÁGINA INICIAL</a>
							@if (Session::has('busca_url'))
							<a href="{{session('busca_url')}}" class='btn-main w-100'>RESULTADO DA BUSCA</a>
							@endif
						</div>
					</div>				
				</main>
			</div>
			
		</section><!-- #services -->
	</main>

	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
	
	<div id="preloader"></div>

	<!-- JavaScript Libraries -->
	<script src="{{asset('js/app.js') }}"></script>
	<script src="{{asset('plugins/easing/easing.min.js')}}"></script>
	<script src="{{asset('plugins/mobile-nav/mobile-nav.js')}}"></script>
	<script src="{{asset('plugins/wow/wow.min.js')}}"></script>
	<script src="{{asset('plugins/waypoints/waypoints.min.js')}}"></script>
	<script src="{{asset('plugins/counterup/counterup.min.js')}}"></script>
	<script src="{{asset('plugins/owlcarousel/owl.carousel.min.js')}}"></script>
	<script src="{{asset('plugins/isotope/isotope.pkgd.min.js')}}"></script>
	<script src="{{asset('plugins/lightbox/js/lightbox.min.js')}}"></script>  
	<script src="{{asset('js/main.js')}}"></script>
	<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js')}}"></script>
	<script>
		$('select').SumoSelect({placeholder:"Selecione uma opção",forceCustomRendering: true});            
		$('#datepicker').datepicker({language:'pt-BR',clearBtn: true});
	</script>
</body>
</html>
