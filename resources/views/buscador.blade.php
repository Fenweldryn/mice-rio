<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{asset('plugins/animate/animate.min.css')}}" rel="stylesheet">
	<link href="{{asset('plugins/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
	<link href="{{asset('plugins/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
	<link href="{{asset('plugins/lightbox/css/lightbox.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/style.css')}}" rel="stylesheet">
	<style>
		.SumoSelect .select-all {
			display: table-cell;
		}
	</style>

</head>
<body>
 
  	<header id="header">
		@include('index_navbar')
		@section('social-bar',"")
	</header> 

	<div class="clearfix"></div>
	<main id="main">   	
		<section id="buscador" class="section-bg">
			<div class="container-fluid mb-5">				
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><header class="tab-header">
						<h3>Cidades</h3>
						</header></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#perfil" role="tab" aria-controls="profile" aria-selected="false"><header class="tab-header">
						<h3>Locais para evento</h3>
						</header></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contato" role="tab" aria-controls="contact" aria-selected="false"><header class="tab-header">
						<h3>Serviços</h3>
						</header></a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">		
						<form action={{route('busca')}} method="GET">									
						<div class='row mt-3'>
							<div class="col-md mb-2">
								<div class="card box-shadow-blur">
									@if ($errors->any())
										<div class="alert alert-danger">
											<ul>
												@foreach ($errors->all() as $error)
													<li>{{ $error }}</li>
												@endforeach
											</ul>
										</div>
									@endif		
									<div class="card-header bg-primary text-white font-size-16 font-weight-bold">
										LOGÍSTICA E ACESSO
									</div>
									<div class="card-body">																
										<div class="form-group">
											<label for="">Distância da capital</label>
											<select class="form-control align-bottom" name="distancia_capital" id="">					
											<option value="80">até 80km</option>
											<option value="160">até 160km</option>
											<option value="240">até 240km</option>
											<option value="241">Mais de 240km</option>
											</select>
										</div>
			
										<div class="form-group">
											<label for="">Acesso</label>
											<select multiple="multiple" class="form-control" name="acesso[]" id="">
											@foreach ($modalTipos as $modal)
												<option value="{{$modal->id}}">{{$modal->nome}}</option>
											@endforeach
											</select>								
										</div>													
										
									</div>
								</div>						
							</div>
							<div class="col-md mb-2">
								<div class="card box-shadow-blur">
									<div class="card-header bg-primary text-white font-size-16 font-weight-bold">
										INFRAESTRUTURA
									</div>
									<div class="card-body">								
										<div class="form-group">
											<label for="">Tipo de local</label>
											<select multiple="multiple" class="form-control align-bottom" name="tipo_local[]" id="">
											@foreach ($localEventoTipos as $tipo)
												<option value="{{$tipo->id}}">{{$tipo->nome}}</option>
											@endforeach											

											</select>
										</div>
			
										<div class="form-group">
											<label for="">Capacidade mínima requerida</label>
											<input type="text" maxlength="7" min="0"
												class="form-control" name="capacidade" id="" placeholder="">
										</div>													
										
									</div>
								</div>
							</div>
							<div class="col-md mb-2">
								<div class="card box-shadow-blur">
									<div class="card-header bg-primary text-white font-size-16 font-weight-bold">
										HOSPEDAGEM
									</div>
									<div class="card-body">								
										<div class="form-group">
											<label for="">Categorias de Hospedagem</label>
											<select multiple="multiple" class="form-control align-bottom" name="tipos_hospedagem[]" id="">
											@foreach ($alojamentoTipos as $tipo)
												<option value="{{$tipo->id}}">{{$tipo->nome}}</option>
											@endforeach
											</select>
										</div>
			
										<div class="form-group">
											<label for="">Perfil hoteleiro</label>
											<select multiple="multiple" class="form-control" name="perfil_hoteleiro[]" id="">
											@foreach ($alojamentoCategorias as $categoria)
												<option value="{{$categoria->id}}">{{$categoria->nome}}</option>
											@endforeach
									
											</select>								
										</div>													
										
									</div>
								</div>
							</div>					
						</div>		

						<div class='row col justify-content-end mb-0'>
							
						<button type="submit" name="" id="" class="btn btn-success btn-lg btn-buscar" role="button"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>	
						</form>					
						</div>						
					</div>
					<div class="tab-pane fade" id="perfil" role="tabpanel" aria-labelledby="profile-tab">
						<h1 class="mt-3">Em breve!</h1>
					</div>
					<div class="tab-pane fade" id="contato" role="tabpanel" aria-labelledby="contact-tab">
						<h1 class="mt-3">Em breve!</h1>
					</div>
				</div>	
				
			</div>
		</section><!-- #services -->
	</main>

  <!--==========================
	Footer
  ============================-->
  <footer id="footer" class="section-bg">
	@include('index_footer')
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  
  <div id="preloader"></div>

  <!-- JavaScript Libraries -->
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>  
  <script src="{{asset('plugins/easing/easing.min.js')}}"></script>
  <script src="{{asset('plugins/mobile-nav/mobile-nav.js')}}"></script>
  <script src="{{asset('plugins/wow/wow.min.js')}}"></script>
  <script src="{{asset('plugins/waypoints/waypoints.min.js')}}"></script>
  <script src="{{asset('plugins/counterup/counterup.min.js')}}"></script>
  <script src="{{asset('plugins/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('plugins/isotope/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('plugins/lightbox/js/lightbox.min.js')}}"></script>  
  <script src="{{asset('js/main.js')}}"></script>
  <script>
	$('select').SumoSelect({
		placeholder: 'Selecione',
		selectAll : true,
		captionFormatAllSelected: "{0} todos selecionados!",
		locale: ['Ok', 'Cancelar', "Todos"],
	});

	$('input[name=capacidade]').change(function (e) { 		
		if ($(this).val() < 0) {
			$(this).val('');			
		}		
	});
  </script>

</body>
</html>
