<!DOCTYPE html>
<html lang="pt-br">
<head>
	@if (app()->environment() == 'production')
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149360743-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-149360743-1');
	</script>		
	@endif
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>MICE Rio | Administraçao</title>
	
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/adminlte.min.css') }}" rel="stylesheet">  
	<link href="{{ mix('css/admin.css') }}" rel="stylesheet">  
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	@yield('css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

	@include('admin.navbar')
	@include('admin.sidebar')
	
	<div class="content-wrapper">
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">@yield('title')</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							@yield('breadcrumb')							
						</ol>
					</div>
				</div>
			</div>
		</div>
		
		<div class="content">
			<div class="container-fluid">
				@yield('content')				
			</div>
		</div>
	</div>

	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<div class="p-3">
			
		</div>
	</aside>

	@include('admin.footer')
</div>
<!-- ./wrapper -->

<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('js/adminlte.min.js') }}"></script>
@yield('js')
</body>
</html>
