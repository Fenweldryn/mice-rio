<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	@if (app()->environment() == 'production')
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149360743-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-149360743-1');
	</script>		
	@endif
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>MICE Rio - {{$cidade->nome . ' - ' . $cidade->estado_sigla}}</title>
	<meta name="description" content="{{substr($cidade->descricao,0,525)}}">

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Styles -->
	<link href="{{ mix('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/animate/animate.min.css')}}" rel="stylesheet">
	<link href="{{ asset('css/style.css')}}" rel="stylesheet">
	<style>
		h2 {
			font-weight: 500;
			font-size: 2em;
		}
	</style>
</head>
<body>	
	<header id="header" class='keep-header header-scrolled'>
		@include('index_navbar')
		@section('social-bar',"")
	</header> 
	<main id="main" class='clearfix'>   	
		<section id="cidade" class="section-bg">			
			<div class="container-fluid p-0" style="overflow-x:hidden">									
				<div class="row cover-img" id="cover-img-row">	
					<div class='nome-cidade'>
						<h2 class='text-uppercase'>{{$cidade->nome}}</h2>
					</div>
					<div id='carousel' class="carousel slide" data-ride="carousel" data-interval=4000>
						
						<ol class="carousel-indicators">						
							@if($cidade->getFirstMediaUrl() != null)
								<?php $i=0; ?>
								@foreach ($cidade->getMedia() as $foto)
									<li data-target="#carousel" data-slide-to="<?php echo $i; ?>" class="<?php if($foto->id == $cidade->foto_principal){echo 'active';} ?>"></li>
									<?php $i++; ?>							
								@endforeach
							@endif
						</ol>
						
						<div class="carousel-inner">						
						@php
							$active = "active";
						@endphp
						@if($cidade->getFirstMediaUrl() == null)
						
							<div class="carousel-item active">
							<img class="cover-img w-100" src="{{"/img/rio.jpg"}}" alt="{{$cidade->nome}}" >	
							</div>

						@else
						@foreach ($cidade->getMedia() as $foto)
						
							<div class="carousel-item <?php
								if ($cidade->foto_principal) {
									if ($foto->id == $cidade->foto_principal) {
										echo 'active';
									}
								} else {
									echo $active;
									$active = "";
								}
							?>">
								<img class="cover-img" style="width:100vw" src="{{$foto->getFullUrl()}}" alt="{{$cidade->nome}}">					
							</div>
						
						@endforeach
						@endif

						<a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
						</div>
					</div>
				</div>
				<hr>
				<main role="main">		
					<div class="row">
						<h1>{{$cidade->nome}}</h1>						
					</div>			
					<div class="row">
						<div class="col p-0">
							<p class="text-justify">
								@if (strlen($cidade->descricao) > 525)
								{!! nl2br(substr($cidade->descricao,0,525)) !!}<span id="dots">...</span><span id="more">{!! nl2br(substr($cidade->descricao,525,strlen($cidade->descricao))) !!} </span><button onclick="more()" class="btn-main btn-read" id="myBtn" style="">ler mais</button>
								
								@else
								{!! nl2br($cidade->descricao) !!}
								@endif
							</p>
						</div>						
					</div>
					
					<div class="info-container row">

						<div class="info-block col-xs-12 col-sm-12 col-md-3 wow fadeInUp">
							<i class="fas fa-chair fa-2x"></i>
							<span class='quanto'>{{$cidade->entidade->regiao_cidade_principal_id ? $cidade->entidade->cidadePrincipalRegiao->capacidade_maior_auditorio :  $cidade->capacidade_maior_auditorio}} pessoas</span>
							<span class="oque">maior auditorio</span>
						</div>
	
						<div class="info-block col-xs-12 col-sm-12 col-md-3 wow fadeInUp">
							<i class="far fa-square fa-2x"></i>
							<span class='quanto'>{{$cidade->entidade->regiao_cidade_principal_id ? $cidade->entidade->cidadePrincipalRegiao->capacidade_maior_espaco : $cidade->capacidade_maior_espaco}} m²</span>
							<span class="oque">maior espaço</span>
						</div>

						<div class="info-block col-xs-12 col-sm-12 col-md-3 wow fadeInUp">
							<i class="fas fa-utensils fa-2x"></i>
							<span class='quanto'>{{$cidade->entidade->regiao_cidade_principal_id ? $cidade->entidade->cidadePrincipalRegiao->capacidade_maior_banquete : $cidade->capacidade_maior_banquete}} pessoas</span>
							<span class="oque">maior banquete</span>
						</div>

						<div class="info-block col-xs-12 col-sm-12 col-md-3 wow fadeInUp">
							<i class="fas fa-door-open fa-2x"></i>
							<span class='quanto'>{{$cidade->entidade->regiao_cidade_principal_id ? $cidade->entidade->cidadePrincipalRegiao->capacidade_total_quartos : $cidade->capacidade_total_quartos}}</span>
							<span class="oque">total de quartos</span>
						</div>
					</div>

					<div class="row">
						
						<div class="col wow fadeInUp p-0">
							<ul id="list" class="">
								<li>
									<a data-toggle="collapse" class="collapsed" href="#maisinfo">MAIS INFORMAÇÕES <i class="fas fa-minus"></i></a>
									<div id="maisinfo" class="collapse" data-parent="#list" style="padding:15px">
										<table class="table">										
											<tbody>
												<tr>
													<td class="text-uppercase">Distância da Capital</td>
													<td class="text-right">{{$cidade->entidade->regiao_cidade_principal_id ? $cidade->entidade->cidadePrincipalRegiao->distancia_capital : $cidade->distancia_capital}} KM</td>
												</tr>
												<tr>
													<td class="text-uppercase">Total de leitos na cidade</td>
													<td class="text-right">{{$cidade->entidade->regiao_cidade_principal_id ? $cidade->entidade->cidadePrincipalRegiao->capacidade_total_leitos : $cidade->capacidade_total_leitos}} leitos</td>
												</tr>
												<tr>
													<td class="text-uppercase">Capacidade do maior hotel</td>
													<td class="text-right">{{$cidade->entidade->regiao_cidade_principal_id ? $cidade->entidade->cidadePrincipalRegiao->capacidade_quartos_maior_hotel : $cidade->capacidade_quartos_maior_hotel}} leitos</td>
												</tr>
												<tr>
													<td class="text-uppercase">Modais de Acesso</td>
													<td class="text-right">													
														@foreach ($cidade->modalTipos as $tipo)
															{{$tipo->nome}}<br>
														@endforeach												
													</td>
												</tr>
												<tr>
													<td class="text-uppercase">Locais de Evento</td>
													<td class="text-right">													
														@foreach ($cidade->localEventoTipos as $tipo)
															{{$tipo->nome}}<br>
														@endforeach										
													</td>
												</tr>
												<tr>
													<td class="text-uppercase">Instituições de Ensino Superior</td>
													<td class="text-right">{{$cidade->entidade->regiao_cidade_principal_id ? $cidade->entidade->cidadePrincipalRegiao->numero_instituicoes_ensino_superior : $cidade->numero_instituicoes_ensino_superior}}</td>
												</tr>											
												<tr>
													<td class="text-uppercase">Áreas de Expertise Referência</td>
													<td class="text-right">{!! nl2br($cidade->areas_expertise_referencia) !!}</td>
												</tr>											
												<tr>
													<td class="text-uppercase">Principais Eventos</td>
													<td class="text-right">{!! nl2br($cidade->principais_eventos) !!}</td>
												</tr>											
											</tbody>									
										</table>
									</div>
								</li>
							</ul>		
							@if ($cidade->entidade()->first())
							@if ($cidade->entidade()->first()->getFirstMediaUrl())
							<div class='text-center'>
								<a href="{{ $cidade->entidade->site }}" target="_blank">
									<img class="img-fluid p-5" style="max-height: 580px" src="{{$cidade->entidade()->first()->getFirstMediaUrl()}}" alt="">
								</a>
							</div>
							@endif
							@endif
							
						</div>
						<div class="col-md-6">
							<div class="google-maps wow fadeInUp w-100">
							<iframe id='mapa' width="100%" height="100%" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?output=embed&z=10&q={{str_replace(",", "", str_replace(" ", "+", $cidade->nome." - RJ"))}}"></iframe>
							</div>	
						</div>
					</div>
					
					<div class="row mt-5 wow fadeInUp">
						<a class="btn-main mt-5" href="{{url('/cidades')."/$cidade->slug"}}/solicitarproposta" role="button"> SOLICITAR PROPOSTA</a>
					</div>
				</main>
				
			</div>
			
		</section><!-- #services -->
	</main>
	
	<div style="background: #f5f8fd url({{$cidade->getFirstMediaUrl() ? $cidade->getFirstMediaUrl() : "/img/rio.jpg"}}) center top no-repeat fixed; background-size: cover; height:200px"></div>

	<footer id="footer" class="section-bg">
	@include('index_footer')
	</footer>
	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
	
	{{-- <div id="preloader"></div> --}}
 
	<!-- JavaScript Libraries -->
	<script src="{{ mix('js/app.js') }}"></script>
	<script src="{{asset('plugins/easing/easing.min.js')}}"></script>
	<script src="{{asset('plugins/mobile-nav/mobile-nav.js')}}"></script>
	<script src="{{asset('plugins/wow/wow.min.js')}}"></script>
	<script src="{{asset('js/main.js')}}"></script>
	<script>
		$('select').SumoSelect();
		
		function more() {
			var dots = document.getElementById("dots");
			var moreText = document.getElementById("more");
			var btnText = document.getElementById("myBtn");

			if (dots.style.display === "none") {
				dots.style.display = "inline";
				btnText.innerHTML = "ler mais";
				moreText.style.display = "none";
			} else {
				dots.style.display = "none";
				btnText.innerHTML = "ler menos";
				moreText.style.display = "inline";
			}
		}
	</script>
</body>
</html>
