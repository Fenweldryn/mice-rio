@yield('social-bar')

<div class="container-fluid">
    <div class="logo float-left"> 
    <h1 class="text-light"><a href="{{url('')}}" class="scrollto">
        <img class="img-fluid" src="{{url('/logot.png')}}" alt="">              
        <span>MICE Rio</span></a>
    </h1>        
    </div>

    <nav class="main-nav float-right d-none d-lg-block">
    <ul>
        <li><a href="{{url('')}}">Home</a></li>
        <li><a href="{{url('')}}#sobre">Sobre</a></li>                      
        <li><a href="{{url('')}}#cidades">Cidades</a></li>                      
        <li><a href="{{url('')}}#buscador">Buscador</a></li>                      
        <li><a href="#footer">Contato</a></li>
        @guest
        <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Entrar') }}</a>
        </li>        
        @else
        @if (auth()->user()->hasRole('admin'))
        <li class="nav-item">
            <a class="dropdown-item" href="{{ route('admin') }}"><i class="fas fa-lock"></i> Admin</a>             
        </li>  
        @endif
        @if (auth()->user()->hasRole('gestor'))
        <li class="nav-item">
            <a class="dropdown-item" href="{{ route('admin') }}"><i class="fas fa-lock"></i> Gestão</a>             
        </li>  
        @endif
        <li class="nav-item">
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">                   
                <i class="fas fa-sign-out-alt"></i> sair                   
                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                
            </a>
        </li>
        @endguest
        
    </ul>
    </nav><!-- .main-nav -->
    
</div>
