<div class="footer-top">
    <div class="container">

    <div class="row">

        <div class="col-lg-6">

        <div class="row">

            <div class="col-sm-6 text-center">
                <div class="footer-info">
                <h3><img class="img-fluid" src="{{url('/logot.png')}}" alt=""> MICE Rio</h3>
                <p>Meetings, Incentives, Conferences and Exhibitions Rio</p>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="footer-links">
                <h4><a href="https://www.fcvbrj.org.br/new/">Federação de Convention & Visitors <br>
                    Bureaux do Estado do Rio de Janeiro</a>  </h4>
                <p>
                    <b>SEDE</b><br>           
                    Rua São José, 40, 4º andar <br>
                    Centro - Rio de Janeiro<br>
                    CEP 20010-20 <br>
                    +55 21 3231-9032<br>
                    projetos@fcvbrj.org.br<br>
                </p>
                
                <p>
                    <b>ESCRITÓRIO REGIONAL MACAÉ</b><br>           
                    Avenida Rui Barbosa, 1829 sobreloja <br>
                    Imbetiba - Macaé/RJ<br>
                    CEP 27915-012 <br>
                    +55 22 2020-7601<br>                    
                </p>

                   <p>
                    <b>ESCRITÓRIO REGIONAL NOVA FRIBURGO</b><br>           
                    Avenida Alberto Braune, 4 - sobreloja 3<br>
                    Centro - Nova Friburgo<br>
                    CEP 28613-000 <br>
                    +55 22 2580-5209<br>                    
                </p>
                </div>				  

            </div>

        </div>

        </div>

        <div class="col-lg-6">

        <div class="form">
            
            <h4>Contato</h4>
            <form id='email' role="form" class="contactForm">
            <input type="hidden" name="mordor" value="">
            <div class="form-group">
                <input type="text" name="nome" class="form-control" id="name" placeholder="nome" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
            </div>
            <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="assunto" id="subject" placeholder="assunto" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validation"></div>
            </div>
            <div class="form-group">
                <textarea class="form-control" name="mensagem" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="mensagem"></textarea>
                <div class="validation"></div>
            </div>

            <div id="sendmessage">Sua mensagem foi enviada com sucesso.</div>
            <div id="errormessage">Ocorreu um erro e sua mensagem não pôde ser enviada.<br>Tente novamente mais tarde.</div>
            <div id="errorform" class="alert alert-danger d-none"><ul></ul></div>

            </form>
            <button onclick="email(this)" title="Send Message" class='w-100 btn-main'>Enviar Mensagem</button>
        </div>
        </div>
    </div>
    </div>
</div>

<div class="container">
    <div class="copyright">
    &copy; Copyright <strong>MICE Rio</strong>. Todos os Direitos Reservados. <br>Desenvolvido por <a href="http://krotec.com.br">Krotec Desenvolvimento de Sistemas</a>
    </div>         
</div>

<script>
function email(btn) {
    event.preventDefault();
    $("#sendmessage").hide();
    $("#errormessage").hide();
    $("#errorform").addClass('d-none');
    $(btn).LoadingOverlay("show");
    $.ajax({
        type: "POST",
        url: "{{url("")}}/enviaremail",
        data: $("#email").serializeArray(),                        
    }).fail(function(response){
        $("#errormessage").show();
    }).done(function(response){    
        if(response === true) {
            $("#sendmessage").show();
            $('#email').trigger("reset");
        } else if(response === false) {
            $("#errormessage").show();
        } else if('error' in response) {
            $("#errorform ul").html("");
            $.each(response.error, function (i, v) { 
                $("#errorform ul").append("<li>"+v+"</li>");
            });
            $("#errorform").removeClass('d-none');
        }  
    }).always(function(){
        $(btn).LoadingOverlay("hide");
    });   
}
</script>