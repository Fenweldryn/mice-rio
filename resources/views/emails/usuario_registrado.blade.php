Mensagem do site MICE-RIO.COM.BR.
<br>
Um novo usuário se registrou no site em {{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y H:i:s') }}.
<br>
<br>Nome: <b>{{ $user->name }}</b>
<br>Email: <b>{{ $user->email }}</b>