/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/*!
 * jquery.sumoselect - v3.0.3
 * http://hemantnegi.github.io/jquery.sumoselect
 * 2016-12-12
 *
 * Copyright 2015 Hemant Negi
 * Email : hemant.frnz@gmail.com
 * Compressor http://refresh-sf.com/
 */

(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

})(function ($) {

    'namespace sumo';
    $.fn.SumoSelect = function (options) {

        // This is the easiest way to have default options.
        var settings = $.extend({
            placeholder: 'Select Here',   // Dont change it here.
            csvDispCount: 3,              // display no. of items in multiselect. 0 to display all.
            captionFormat: '{0} Selected', // format of caption text. you can set your locale.
            captionFormatAllSelected: '{0} all selected!', // format of caption text when all elements are selected. set null to use captionFormat. It will not work if there are disabled elements in select.
            floatWidth: 400,              // Screen width of device at which the list is rendered in floating popup fashion.
            forceCustomRendering: false,  // force the custom modal on all devices below floatWidth resolution.
            nativeOnDevice: ['Android', 'BlackBerry', 'iPhone', 'iPad', 'iPod', 'Opera Mini', 'IEMobile', 'Silk'], //
            outputAsCSV: false,           // true to POST data as csv ( false for Html control array ie. default select )
            csvSepChar: ',',              // separation char in csv mode
            okCancelInMulti: false,       // display ok cancel buttons in desktop mode multiselect also.
            isClickAwayOk: false,         // for okCancelInMulti=true. sets whether click outside will trigger Ok or Cancel (default is cancel).
            triggerChangeCombined: true,  // im multi select mode whether to trigger change event on individual selection or combined selection.
            selectAll: false,             // to display select all button in multiselect mode.|| also select all will not be available on mobile devices.

            search: false,                // to display input for filtering content. selectAlltext will be input text placeholder
            searchText: 'Search...',      // placeholder for search input
            searchFn: function (haystack, needle) { // search function
                return haystack.toLowerCase().indexOf(needle.toLowerCase()) < 0;
            },
            noMatch: 'No matches for "{0}"',
            prefix: '',                   // some prefix usually the field name. eg. '<b>Hello</b>'
            locale: ['OK', 'Cancel', 'Select All'],  // all text that is used. don't change the index.
            up: false,                    // set true to open upside.
            showTitle: true               // set to false to prevent title (tooltip) from appearing
        }, options);

        var ret = this.each(function () {
            var selObj = this; // the original select object.
            if (this.sumo || !$(this).is('select')) return; //already initialized

            this.sumo = {
                E: $(selObj),   //the jquery object of original select element.
                is_multi: $(selObj).attr('multiple'),  //if its a multiple select
                select: '',
                caption: '',
                placeholder: '',
                optDiv: '',
                CaptionCont: '',
                ul: '',
                is_floating: false,
                is_opened: false,
                //backdrop: '',
                mob: false, // if to open device default select
                Pstate: [],
                lastUnselected: null,

                createElems: function () {
                    var O = this;
                    O.E.wrap('<div class="SumoSelect" tabindex="0" role="button" aria-expanded="false">');
                    O.select = O.E.parent();
                    O.caption = $('<span>');
                    O.CaptionCont = $('<p class="CaptionCont SelectBox" ><label><i></i></label></p>')
                        .attr('style', O.E.attr('style'))
                        .prepend(O.caption);
                    O.select.append(O.CaptionCont);

                    // default turn off if no multiselect
                    if (!O.is_multi) settings.okCancelInMulti = false

                    if (O.E.attr('disabled'))
                        O.select.addClass('disabled').removeAttr('tabindex');

                    //if output as csv and is a multiselect.
                    if (settings.outputAsCSV && O.is_multi && O.E.attr('name')) {
                        //create a hidden field to store csv value.
                        O.select.append($('<input class="HEMANT123" type="hidden" />').attr('name', O.E.attr('name')).val(O.getSelStr()));

                        // so it can not post the original select.
                        O.E.removeAttr('name');
                    }

                    //break for mobile rendring.. if forceCustomRendering is false
                    if (O.isMobile() && !settings.forceCustomRendering) {
                        O.setNativeMobile();
                        return;
                    }

                    // if there is a name attr in select add a class to container div
                    if (O.E.attr('name')) O.select.addClass('sumo_' + O.E.attr('name').replace(/\[\]/, ''))

                    //hide original select
                    O.E.addClass('SumoUnder').attr('tabindex', '-1');

                    //## Creating the list...
                    O.optDiv = $('<div class="optWrapper ' + (settings.up ? 'up' : '') + '">');

                    //branch for floating list in low res devices.
                    O.floatingList();

                    //Creating the markup for the available options
                    O.ul = $('<ul class="options">');
                    O.optDiv.append(O.ul);

                    // Select all functionality
                    if (settings.selectAll && O.is_multi) O.SelAll();

                    // search functionality
                    if (settings.search) O.Search();

                    O.ul.append(O.prepItems(O.E.children()));

                    //if multiple then add the class multiple and add OK / CANCEL button
                    if (O.is_multi) O.multiSelelect();

                    O.select.append(O.optDiv);
                    O.basicEvents();
                    O.selAllState();
                },

                prepItems: function (opts, d) {
                    var lis = [], O = this;
                    $(opts).each(function (i, opt) {       // parsing options to li
                        opt = $(opt);
                        lis.push(opt.is('optgroup') ?
                            $('<li class="group ' + (opt[0].disabled ? 'disabled' : '') + '"><label>' + opt.attr('label') + '</label><ul></ul></li>')
                                .find('ul')
                                .append(O.prepItems(opt.children(), opt[0].disabled))
                                .end()
                            :
                            O.createLi(opt, d)
                        );
                    });
                    return lis;
                },

                //## Creates a LI element from a given option and binds events to it
                //## returns the jquery instance of li (not inserted in dom)
                createLi: function (opt, d) {
                    var O = this;

                    if (!opt.attr('value')) opt.attr('value', opt.val());
                    var li = $('<li class="opt"><label>' + opt.text() + '</label></li>');

                    li.data('opt', opt);    // store a direct reference to option.
                    opt.data('li', li);    // store a direct reference to list item.
                    if (O.is_multi) li.prepend('<span><i></i></span>');

                    if (opt[0].disabled || d)
                        li = li.addClass('disabled');

                    O.onOptClick(li);

                    if (opt[0].selected)
                        li.addClass('selected');

                    if (opt.attr('class'))
                        li.addClass(opt.attr('class'));

                    if (opt.attr('title'))
                        li.attr('title', opt.attr('title'));

                    return li;
                },

                //## Returns the selected items as string in a Multiselect.
                getSelStr: function () {
                    // get the pre selected items.
                    var sopt = [];
                    this.E.find('option:selected').each(function () { sopt.push($(this).val()); });
                    return sopt.join(settings.csvSepChar);
                },

                //## THOSE OK/CANCEL BUTTONS ON MULTIPLE SELECT.
                multiSelelect: function () {
                    var O = this;
                    O.optDiv.addClass('multiple');
                    O.okbtn = $('<p tabindex="0" class="btnOk">' + settings.locale[0] + '</p>').click(function () {
                        //if combined change event is set.
                        O._okbtn();
                        O.hideOpts();
                    });
                    O.cancelBtn = $('<p tabindex="0" class="btnCancel">' + settings.locale[1] + '</p>').click(function () {
                        O._cnbtn();
                        O.hideOpts();
                    });
                    var btns = O.okbtn.add(O.cancelBtn);
                    O.optDiv.append($('<div class="MultiControls">').append(btns));

                    // handling keyboard navigation on ok cancel buttons.
                    btns.on('keydown.sumo', function (e) {
                        var el = $(this);
                        switch (e.which) {
                            case 32: // space
                            case 13: // enter
                                el.trigger('click');
                                break;

                            case 9:  //tab
                                if (el.hasClass('btnOk')) return;
                            case 27: // esc
                                O._cnbtn();
                                O.hideOpts();
                                return;
                        }
                        e.stopPropagation();
                        e.preventDefault();
                    });
                },

                _okbtn: function () {
                    var O = this, cg = 0;
                    //if combined change event is set.
                    if (settings.triggerChangeCombined) {
                        //check for a change in the selection.
                        if (O.E.find('option:selected').length !== O.Pstate.length) {
                            cg = 1;
                        }
                        else {
                            O.E.find('option').each(function (i, e) {
                                if (e.selected && O.Pstate.indexOf(i) < 0) cg = 1;
                            });
                        }

                        if (cg) {
                            O.callChange();
                            O.setText();
                        }
                    }
                },
                _cnbtn: function () {
                    var O = this;
                    //remove all selections
                    O.E.find('option:selected').each(function () { this.selected = false; });
                    O.optDiv.find('li.selected').removeClass('selected')

                    //restore selections from saved state.
                    for (var i = 0; i < O.Pstate.length; i++) {
                        O.E.find('option')[O.Pstate[i]].selected = true;
                        O.ul.find('li.opt').eq(O.Pstate[i]).addClass('selected');
                    }
                    O.selAllState();
                },

                SelAll: function () {
                    var O = this;
                    if (!O.is_multi) return;
                    O.selAll = $('<p class="select-all"><span><i></i></span><label>' + settings.locale[2] + '</label></p>');
                    O.optDiv.addClass('selall');
                    O.selAll.on('click', function () {
                        O.selAll.toggleClass('selected');
                        O.toggSelAll(O.selAll.hasClass('selected'), 1);
                        //O.selAllState();
                    });

                    O.optDiv.prepend(O.selAll);
                },

                // search module (can be removed if not required.)
                Search: function () {
                    var O = this,
                        cc = O.CaptionCont.addClass('search'),
                        P = $('<p class="no-match">'),
                        fn = (options.searchFn && typeof options.searchFn == 'function') ? options.searchFn : settings.searchFn;

                    O.ftxt = $('<input type="text" class="search-txt" value="" placeholder="' + settings.searchText + '">')
                        .on('click', function (e) {
                            e.stopPropagation();
                        });
                    cc.append(O.ftxt);
                    O.optDiv.children('ul').after(P);

                    O.ftxt.on('keyup.sumo', function () {
                        var hid = O.optDiv.find('ul.options li.opt').each(function (ix, e) {
                            var e = $(e),
                                opt = e.data('opt')[0];
                            opt.hidden = fn(e.text(), O.ftxt.val());
                            e.toggleClass('hidden', opt.hidden);
                        }).not('.hidden');

                        P.html(settings.noMatch.replace(/\{0\}/g, '<em></em>')).toggle(!hid.length);
                        P.find('em').text(O.ftxt.val());
                        O.selAllState();
                    });
                },

                selAllState: function () {
                    var O = this;
                    if (settings.selectAll && O.is_multi) {
                        var sc = 0, vc = 0;
                        O.optDiv.find('li.opt').not('.hidden').each(function (ix, e) {
                            if ($(e).hasClass('selected')) sc++;
                            if (!$(e).hasClass('disabled')) vc++;
                        });
                        //select all checkbox state change.
                        if (sc === vc) O.selAll.removeClass('partial').addClass('selected');
                        else if (sc === 0) O.selAll.removeClass('selected partial');
                        else O.selAll.addClass('partial')//.removeClass('selected');
                    }
                },

                showOpts: function () {
                    var O = this;
                    if (O.E.attr('disabled')) return; // if select is disabled then retrun
                    O.E.trigger('sumo:opening', O);
                    O.is_opened = true;
                    O.select.addClass('open').attr('aria-expanded', 'true');
                    O.E.trigger('sumo:opened', O);

                    if (O.ftxt) O.ftxt.focus();
                    else O.select.focus();

                    // hide options on click outside.
                    $(document).on('click.sumo', function (e) {
                        if (!O.select.is(e.target)                  // if the target of the click isn't the container...
                            && O.select.has(e.target).length === 0) { // ... nor a descendant of the container
                            if (!O.is_opened) return;
                            O.hideOpts();
                            if (settings.okCancelInMulti) {
                                if (settings.isClickAwayOk)
                                    O._okbtn();
                                else
                                    O._cnbtn();
                            }
                        }
                    });

                    if (O.is_floating) {
                        var H = O.optDiv.children('ul').outerHeight() + 2;  // +2 is clear fix
                        if (O.is_multi) H = H + parseInt(O.optDiv.css('padding-bottom'));
                        O.optDiv.css('height', H);
                        $('body').addClass('sumoStopScroll');
                    }

                    O.setPstate();
                },

                //maintain state when ok/cancel buttons are available storing the indexes.
                setPstate: function () {
                    var O = this;
                    if (O.is_multi && (O.is_floating || settings.okCancelInMulti)) {
                        O.Pstate = [];
                        // assuming that find returns elements in tree order
                        O.E.find('option').each(function (i, e) { if (e.selected) O.Pstate.push(i); });
                    }
                },

                callChange: function () {
                    this.E.trigger('change').trigger('click');
                },

                hideOpts: function () {
                    var O = this;
                    if (O.is_opened) {
                        O.E.trigger('sumo:closing', O);
                        O.is_opened = false;
                        O.select.removeClass('open').attr('aria-expanded', 'true').find('ul li.sel').removeClass('sel');
                        O.E.trigger('sumo:closed', O);
                        $(document).off('click.sumo');
                        O.select.focus();
                        $('body').removeClass('sumoStopScroll');

                        // clear the search
                        if (settings.search) {
                            O.ftxt.val('');
                            O.ftxt.trigger('keyup.sumo');
                        }
                    }
                },
                setOnOpen: function () {
                    var O = this,
                        li = O.optDiv.find('li.opt:not(.hidden)').eq(settings.search ? 0 : O.E[0].selectedIndex);
                    if (li.hasClass('disabled')) {
                        li = li.next(':not(disabled)')
                        if (!li.length) return;
                    }
                    O.optDiv.find('li.sel').removeClass('sel');
                    li.addClass('sel');
                    O.showOpts();
                },
                nav: function (up) {
                    var O = this, c,
                        s = O.ul.find('li.opt:not(.disabled, .hidden)'),
                        sel = O.ul.find('li.opt.sel:not(.hidden)'),
                        idx = s.index(sel);
                    if (O.is_opened && sel.length) {

                        if (up && idx > 0)
                            c = s.eq(idx - 1);
                        else if (!up && idx < s.length - 1 && idx > -1)
                            c = s.eq(idx + 1);
                        else return; // if no items before or after

                        sel.removeClass('sel');
                        sel = c.addClass('sel');

                        // setting sel item to visible view.
                        var ul = O.ul,
                            st = ul.scrollTop(),
                            t = sel.position().top + st;
                        if (t >= st + ul.height() - sel.outerHeight())
                            ul.scrollTop(t - ul.height() + sel.outerHeight());
                        if (t < st)
                            ul.scrollTop(t);

                    }
                    else
                        O.setOnOpen();
                },

                basicEvents: function () {
                    var O = this;
                    O.CaptionCont.click(function (evt) {
                        O.E.trigger('click');
                        if (O.is_opened) O.hideOpts(); else O.showOpts();
                        evt.stopPropagation();
                    });

                    O.select.on('keydown.sumo', function (e) {
                        switch (e.which) {
                            case 38: // up
                                O.nav(true);
                                break;

                            case 40: // down
                                O.nav(false);
                                break;

                            case 65: // shortcut ctrl + a to select all and ctrl + shift + a to unselect all.
                                if (O.is_multi && e.ctrlKey) {
                                    O.toggSelAll(!e.shiftKey, 1);
                                    break;
                                }
                                else
                                    return;

                            case 32: // space
                                if (settings.search && O.ftxt.is(e.target)) return;
                            case 13: // enter
                                if (O.is_opened)
                                    O.optDiv.find('ul li.sel').trigger('click');
                                else
                                    O.setOnOpen();
                                break;
                            case 9:	 //tab
                                if (!settings.okCancelInMulti)
                                    O.hideOpts();
                                return;
                            case 27: // esc
                                if (settings.okCancelInMulti) O._cnbtn();
                                O.hideOpts();
                                return;

                            default:
                                return; // exit this handler for other keys
                        }
                        e.preventDefault(); // prevent the default action (scroll / move caret)
                    });

                    $(window).on('resize.sumo', function () {
                        O.floatingList();
                    });
                },

                onOptClick: function (li) {
                    var O = this;
                    li.click(function () {
                        var li = $(this);
                        if (li.hasClass('disabled')) return;
                        var txt = "";
                        if (O.is_multi) {
                            li.toggleClass('selected');
                            li.data('opt')[0].selected = li.hasClass('selected');
                            if (li.data('opt')[0].selected === false) {
                                O.lastUnselected = li.data('opt')[0].textContent;
                            }
                            O.selAllState();
                        }
                        else {
                            li.parent().find('li.selected').removeClass('selected'); //if not multiselect then remove all selections from this list
                            li.toggleClass('selected');
                            li.data('opt')[0].selected = true;
                        }

                        //branch for combined change event.
                        if (!(O.is_multi && settings.triggerChangeCombined && (O.is_floating || settings.okCancelInMulti))) {
                            O.setText();
                            O.callChange();
                        }

                        if (!O.is_multi) O.hideOpts(); //if its not a multiselect then hide on single select.
                    });
                },

                // fixed some variables that were not explicitly typed (michc)
                setText: function () {
                    var O = this;
                    O.placeholder = "";
                    if (O.is_multi) {
                        var sels = O.E.find(':selected').not(':disabled'); //selected options.

                        for (var i = 0; i < sels.length; i++) {
                            if (i + 1 >= settings.csvDispCount && settings.csvDispCount) {
                                if (sels.length === O.E.find('option').length && settings.captionFormatAllSelected) {
                                    O.placeholder = settings.captionFormatAllSelected.replace(/\{0\}/g, sels.length) + ',';
                                } else {
                                    O.placeholder = settings.captionFormat.replace(/\{0\}/g, sels.length) + ',';
                                }

                                break;
                            }
                            else O.placeholder += $(sels[i]).text() + ", ";
                        }
                        O.placeholder = O.placeholder.replace(/,([^,]*)$/, '$1'); //remove unexpected "," from last.
                    }
                    else {
                        O.placeholder = O.E.find(':selected').not(':disabled').text();
                    }

                    var is_placeholder = false;

                    if (!O.placeholder) {

                        is_placeholder = true;

                        O.placeholder = O.E.attr('placeholder');
                        if (!O.placeholder)                  //if placeholder is there then set it
                            O.placeholder = O.E.find('option:disabled:selected').text();
                    }

                    O.placeholder = O.placeholder ? (settings.prefix + ' ' + O.placeholder) : settings.placeholder

                    //set display text
                    O.caption.html(O.placeholder);
                    if (settings.showTitle) O.CaptionCont.attr('title', O.placeholder);

                    //set the hidden field if post as csv is true.
                    var csvField = O.select.find('input.HEMANT123');
                    if (csvField.length) csvField.val(O.getSelStr());

                    //add class placeholder if its a placeholder text.
                    if (is_placeholder) O.caption.addClass('placeholder'); else O.caption.removeClass('placeholder');
                    return O.placeholder;
                },

                isMobile: function () {

                    // Adapted from http://www.detectmobilebrowsers.com
                    var ua = navigator.userAgent || navigator.vendor || window.opera;

                    // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
                    for (var i = 0; i < settings.nativeOnDevice.length; i++) if (ua.toString().toLowerCase().indexOf(settings.nativeOnDevice[i].toLowerCase()) > 0) return settings.nativeOnDevice[i];
                    return false;
                },

                setNativeMobile: function () {
                    var O = this;
                    O.E.addClass('SelectClass')//.css('height', O.select.outerHeight());
                    O.mob = true;
                    O.E.change(function () {
                        O.setText();
                    });
                },

                floatingList: function () {
                    var O = this;
                    //called on init and also on resize.
                    //O.is_floating = true if window width is < specified float width
                    O.is_floating = $(window).width() <= settings.floatWidth;

                    //set class isFloating
                    O.optDiv.toggleClass('isFloating', O.is_floating);

                    //remove height if not floating
                    if (!O.is_floating) O.optDiv.css('height', '');

                    //toggle class according to okCancelInMulti flag only when it is not floating
                    O.optDiv.toggleClass('okCancelInMulti', settings.okCancelInMulti && !O.is_floating);
                },

                //HELPERS FOR OUTSIDERS
                // validates range of given item operations
                vRange: function (i) {
                    var O = this;
                    var opts = O.E.find('option');
                    if (opts.length <= i || i < 0) throw "index out of bounds"
                    return O;
                },

                //toggles selection on c as boolean.
                toggSel: function (c, i) {
                    var O = this;
                    var opt;
                    if (typeof (i) === "number") {
                        O.vRange(i);
                        opt = O.E.find('option')[i];
                    }
                    else {
                        opt = O.E.find('option[value="' + i + '"]')[0] || 0;
                    }
                    if (!opt || opt.disabled)
                        return;

                    if (opt.selected !== c) {
                        opt.selected = c;
                        if (!O.mob) $(opt).data('li').toggleClass('selected', c);

                        O.callChange();
                        O.setPstate();
                        O.setText();
                        O.selAllState();
                    }
                },

                //toggles disabled on c as boolean.
                toggDis: function (c, i) {
                    var O = this.vRange(i);
                    O.E.find('option')[i].disabled = c;
                    if (c) O.E.find('option')[i].selected = false;
                    if (!O.mob) O.optDiv.find('ul.options li').eq(i).toggleClass('disabled', c).removeClass('selected');
                    O.setText();
                },

                // toggle disable/enable on complete select control
                toggSumo: function (val) {
                    var O = this;
                    O.enabled = val;
                    O.select.toggleClass('disabled', val);

                    if (val) {
                        O.E.attr('disabled', 'disabled');
                        O.select.removeAttr('tabindex');
                    }
                    else {
                        O.E.removeAttr('disabled');
                        O.select.attr('tabindex', '0');
                    }

                    return O;
                },

                // toggles all option on c as boolean.
                // set direct=false/0 bypasses okCancelInMulti behaviour.
                toggSelAll: function (c, direct) {
                    var O = this;
                    O.E.find('option:not(:disabled,:hidden)')
                        .each(function (ix, e) {
                            var is_selected = e.selected,
                                e = $(e).data('li');
                            if (e.hasClass('hidden')) return;
                            if (!!c) {
                                if (!is_selected) e.trigger('click');
                            }
                            else {
                                if (is_selected) e.trigger('click');
                            }
                        });

                    if (!direct) {
                        if (!O.mob && O.selAll) O.selAll.removeClass('partial').toggleClass('selected', !!c);
                        O.callChange();
                        O.setText();
                        O.setPstate();
                    }
                },

                /* outside accessibility options
                 which can be accessed from the element instance.
                 */
                reload: function () {
                    var elm = this.unload();
                    return $(elm).SumoSelect(settings);
                },

                unload: function () {
                    var O = this;
                    O.select.before(O.E);
                    O.E.show();

                    if (settings.outputAsCSV && O.is_multi && O.select.find('input.HEMANT123').length) {
                        O.E.attr('name', O.select.find('input.HEMANT123').attr('name')); // restore the name;
                    }
                    O.select.remove();
                    delete selObj.sumo;
                    return selObj;
                },

                //## add a new option to select at a given index.
                add: function (val, txt, i) {
                    if (typeof val === "undefined") throw "No value to add"

                    var O = this;
                    var opts = O.E.find('option')
                    if (typeof txt === "number") { i = txt; txt = val; }
                    if (typeof txt === "undefined") { txt = val; }

                    var opt = $("<option></option>").val(val).html(txt);

                    if (opts.length < i) throw "index out of bounds"

                    if (typeof i === "undefined" || opts.length === i) { // add it to the last if given index is last no or no index provides.
                        O.E.append(opt);
                        if (!O.mob) O.ul.append(O.createLi(opt));
                    }
                    else {
                        opts.eq(i).before(opt);
                        if (!O.mob) O.ul.find('li.opt').eq(i).before(O.createLi(opt));
                    }

                    return selObj;
                },

                //## removes an item at a given index.
                remove: function (i) {
                    var O = this.vRange(i);
                    O.E.find('option').eq(i).remove();
                    if (!O.mob) O.optDiv.find('ul.options li').eq(i).remove();
                    O.setText();
                },

                // removes all but the selected one
                removeAll: function () {
                    var O = this;
                    var options = O.E.find('option');

                    for (var x = (options.length - 1); x >= 0; x--) {
                        if (options[x].selected !== true) {
                            O.remove(x);
                        }
                    }

                },


                find: function (val) {
                    var O = this;
                    var options = O.E.find('option');
                    for (var x in options) {
                        if (options[x].value === val) {
                            return parseInt(x);
                        }
                    }

                    return -1;

                },

                //## Select an item at a given index.
                selectItem: function (i) { this.toggSel(true, i); },

                //## UnSelect an iten at a given index.
                unSelectItem: function (i) { this.toggSel(false, i); },

                //## Select all items  of the select.
                selectAll: function () { this.toggSelAll(true); },

                //## UnSelect all items of the select.
                unSelectAll: function () { this.toggSelAll(false); },

                //## Disable an iten at a given index.
                disableItem: function (i) { this.toggDis(true, i) },

                //## Removes disabled an iten at a given index.
                enableItem: function (i) { this.toggDis(false, i) },

                //## New simple methods as getter and setter are not working fine in ie8-
                //## variable to check state of control if enabled or disabled.
                enabled: true,
                //## Enables the control
                enable: function () { return this.toggSumo(false) },

                //## Disables the control
                disable: function () { return this.toggSumo(true) },


                init: function () {
                    var O = this;
                    O.createElems();
                    O.setText();
                    return O
                }

            };

            selObj.sumo.init();
        });

        return ret.length === 1 ? ret[0] : ret;
    };


});

/***************************************************************************************************
LoadingOverlay - A flexible loading overlay jQuery plugin
    Author          : Gaspare Sganga
    Version         : 2.1.6
    License         : MIT
    Documentation   : https://gasparesganga.com/labs/jquery-loading-overlay/
***************************************************************************************************/
; (function (factory) {
    if (typeof define === "function" && define.amd) {
        // AMD. Register as an anonymous module
        define(["jquery"], factory);
    } else if (typeof module === "object" && module.exports) {
        // Node/CommonJS
        factory(require("jquery"));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($, undefined) {
    "use strict";

    // Default Settings
    var _defaults = {
        // Background
        background: "rgba(255, 255, 255, 0.8)",
        backgroundClass: "",
        // Image
        image: "<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1000 1000'><circle r='80' cx='500' cy='90'/><circle r='80' cx='500' cy='910'/><circle r='80' cx='90' cy='500'/><circle r='80' cx='910' cy='500'/><circle r='80' cx='212' cy='212'/><circle r='80' cx='788' cy='212'/><circle r='80' cx='212' cy='788'/><circle r='80' cx='788' cy='788'/></svg>",
        imageAnimation: "2000ms rotate_right",
        imageAutoResize: true,
        imageResizeFactor: 1,
        imageColor: "#202020",
        imageClass: "",
        imageOrder: 1,
        // Font Awesome
        fontawesome: "",
        fontawesomeAnimation: "",
        fontawesomeAutoResize: true,
        fontawesomeResizeFactor: 1,
        fontawesomeColor: "#202020",
        fontawesomeOrder: 2,
        // Custom
        custom: "",
        customAnimation: "",
        customAutoResize: true,
        customResizeFactor: 1,
        customOrder: 3,
        // Text
        text: "",
        textAnimation: "",
        textAutoResize: true,
        textResizeFactor: 0.5,
        textColor: "#202020",
        textClass: "",
        textOrder: 4,
        // Progress
        progress: false,
        progressAutoResize: true,
        progressResizeFactor: 0.25,
        progressColor: "#a0a0a0",
        progressClass: "",
        progressOrder: 5,
        progressFixedPosition: "",
        progressSpeed: 200,
        progressMin: 0,
        progressMax: 100,
        // Sizing
        size: 50,
        maxSize: 120,
        minSize: 20,
        // Misc
        direction: "column",
        fade: true,
        resizeInterval: 50,
        zIndex: 2147483647
    };

    // Required CSS
    var _css = {
        overlay: {
            "box-sizing": "border-box",
            "position": "relative",
            "display": "flex",
            "flex-wrap": "nowrap",
            "align-items": "center",
            "justify-content": "space-around"
        },
        element: {
            "box-sizing": "border-box",
            "overflow": "visible",
            "flex": "0 0 auto",
            "display": "flex",
            "justify-content": "center",
            "align-items": "center"
        },
        element_svg: {
            "width": "100%",
            "height": "100%"
        },
        progress_fixed: {
            "position": "absolute",
            "left": "0",
            "width": "100%"
        },
        progress_wrapper: {
            "position": "absolute",
            "top": "0",
            "left": "0",
            "width": "100%",
            "height": "100%"
        },
        progress_bar: {
            "position": "absolute",
            "left": "0"
        }
    };

    // Data Template
    var _dataTemplate = {
        "count": 0,
        "container": undefined,
        "settings": undefined,
        "wholePage": undefined,
        "resizeIntervalId": undefined,
        "text": undefined,
        "progress": undefined
    };

    // Whitelists
    var _whitelists = {
        animations: [
            "rotate_right",
            "rotate_left",
            "fadein",
            "pulse"
        ],
        progressPosition: [
            "top",
            "bottom"
        ]
    };

    // Default Values
    var _defaultValues = {
        animations: {
            name: "rotate_right",
            time: "2000ms"
        },
        fade: [400, 200]
    };


    $.LoadingOverlaySetup = function (settings) {
        $.extend(true, _defaults, settings);
    };

    $.LoadingOverlay = function (action, options) {
        switch (action.toLowerCase()) {
            case "show":
                var settings = $.extend(true, {}, _defaults, options);
                Show("body", settings);
                break;

            case "hide":
                Hide("body", options);
                break;

            case "resize":
                Resize("body", options);
                break;

            case "text":
                Text("body", options);
                break;

            case "progress":
                Progress("body", options);
                break;
        }
    };

    $.fn.LoadingOverlay = function (action, options) {
        switch (action.toLowerCase()) {
            case "show":
                var settings = $.extend(true, {}, _defaults, options);
                return this.each(function () {
                    Show(this, settings);
                });

            case "hide":
                return this.each(function () {
                    Hide(this, options);
                });

            case "resize":
                return this.each(function () {
                    Resize(this, options);
                });

            case "text":
                return this.each(function () {
                    Text(this, options);
                });

            case "progress":
                return this.each(function () {
                    Progress(this, options);
                });
        }
    };


    function Show(container, settings) {
        container = $(container);
        settings.size = _ParseSize(settings.size);
        settings.maxSize = parseInt(settings.maxSize, 10) || 0;
        settings.minSize = parseInt(settings.minSize, 10) || 0;
        settings.resizeInterval = parseInt(settings.resizeInterval, 10) || 0;

        var overlay = _GetOverlay(container);
        var data = _GetData(container);
        if (data === false) {
            // Init data
            data = $.extend({}, _dataTemplate);
            data.container = container;
            data.wholePage = container.is("body");

            // Overlay
            overlay = $("<div>", {
                "class": "loadingoverlay"
            })
                .css(_css.overlay)
                .css("flex-direction", settings.direction.toLowerCase() === "row" ? "row" : "column");
            if (settings.backgroundClass) {
                overlay.addClass(settings.backgroundClass);
            } else {
                overlay.css("background", settings.background);
            }
            if (data.wholePage) {
                overlay.css({
                    "position": "fixed",
                    "top": 0,
                    "left": 0,
                    "width": "100%",
                    "height": "100%"
                });
            }
            if (typeof settings.zIndex !== "undefined") overlay.css("z-index", settings.zIndex);

            // Image
            if (settings.image) {
                if ($.isArray(settings.imageColor)) {
                    if (settings.imageColor.length === 0) {
                        settings.imageColor = false;
                    } else if (settings.imageColor.length === 1) {
                        settings.imageColor = {
                            "fill": settings.imageColor[0]
                        };
                    } else {
                        settings.imageColor = {
                            "fill": settings.imageColor[0],
                            "stroke": settings.imageColor[1]
                        };
                    }
                } else if (settings.imageColor) {
                    settings.imageColor = {
                        "fill": settings.imageColor
                    };
                }
                var element = _CreateElement(overlay, settings.imageOrder, settings.imageAutoResize, settings.imageResizeFactor, settings.imageAnimation);
                if (settings.image.slice(0, 4).toLowerCase() === "<svg" && settings.image.slice(-6).toLowerCase() === "</svg>") {
                    // Inline SVG
                    element.append(settings.image);
                    element.children().css(_css.element_svg);
                    if (!settings.imageClass && settings.imageColor) element.find("*").css(settings.imageColor);
                } else if (settings.image.slice(-4).toLowerCase() === ".svg" || settings.image.slice(0, 14).toLowerCase() === "data:image/svg") {
                    // SVG file or base64-encoded SVG
                    $.ajax({
                        url: settings.image,
                        type: "GET",
                        dataType: "html",
                        global: false
                    }).done(function (data) {
                        element.html(data);
                        element.children().css(_css.element_svg);
                        if (!settings.imageClass && settings.imageColor) element.find("*").css(settings.imageColor);
                    });
                } else {
                    // Raster
                    element.css({
                        "background-image": "url(" + settings.image + ")",
                        "background-position": "center",
                        "background-repeat": "no-repeat",
                        "background-size": "cover"
                    });
                }
                if (settings.imageClass) element.addClass(settings.imageClass);
            }

            // Font Awesome
            if (settings.fontawesome) {
                var element = _CreateElement(overlay, settings.fontawesomeOrder, settings.fontawesomeAutoResize, settings.fontawesomeResizeFactor, settings.fontawesomeAnimation)
                    .addClass("loadingoverlay_fa");
                $("<div>", {
                    "class": settings.fontawesome
                }).appendTo(element);
                if (settings.fontawesomeColor) element.css("color", settings.fontawesomeColor);
            }

            // Custom
            if (settings.custom) {
                var element = _CreateElement(overlay, settings.customOrder, settings.customAutoResize, settings.customResizeFactor, settings.customAnimation)
                    .append(settings.custom);
            }

            // Text
            if (settings.text) {
                data.text = _CreateElement(overlay, settings.textOrder, settings.textAutoResize, settings.textResizeFactor, settings.textAnimation)
                    .addClass("loadingoverlay_text")
                    .text(settings.text);
                if (settings.textClass) {
                    data.text.addClass(settings.textClass);
                } else if (settings.textColor) {
                    data.text.css("color", settings.textColor);
                }
            }

            // Progress
            if (settings.progress) {
                var element = _CreateElement(overlay, settings.progressOrder, settings.progressAutoResize, settings.progressResizeFactor, false)
                    .addClass("loadingoverlay_progress");
                var wrapper = $("<div>")
                    .css(_css.progress_wrapper)
                    .appendTo(element);
                data.progress = {
                    bar: $("<div>").css(_css.progress_bar).appendTo(wrapper),
                    fixed: false,
                    margin: 0,
                    min: parseFloat(settings.progressMin),
                    max: parseFloat(settings.progressMax),
                    speed: parseInt(settings.progressSpeed, 10)
                };
                var progressPositionParts = (settings.progressFixedPosition + "").replace(/\s\s+/g, " ").toLowerCase().split(" ");
                if (progressPositionParts.length === 2 && _ValidateProgressPosition(progressPositionParts[0])) {
                    data.progress.fixed = progressPositionParts[0];
                    data.progress.margin = _ParseSize(progressPositionParts[1]);
                } else if (progressPositionParts.length === 2 && _ValidateProgressPosition(progressPositionParts[1])) {
                    data.progress.fixed = progressPositionParts[1];
                    data.progress.margin = _ParseSize(progressPositionParts[0]);
                } else if (progressPositionParts.length === 1 && _ValidateProgressPosition(progressPositionParts[0])) {
                    data.progress.fixed = progressPositionParts[0];
                    data.progress.margin = 0;
                }
                if (data.progress.fixed === "top") {
                    element
                        .css(_css.progress_fixed)
                        .css("top", data.progress.margin ? data.progress.margin.value + (data.progress.margin.fixed ? data.progress.margin.units : "%") : 0);
                } else if (data.progress.fixed === "bottom") {
                    element
                        .css(_css.progress_fixed)
                        .css("top", "auto");
                }
                if (settings.progressClass) {
                    data.progress.bar.addClass(settings.progressClass);
                } else if (settings.progressColor) {
                    data.progress.bar.css("background", settings.progressColor);
                }
            }

            // Fade
            if (!settings.fade) {
                settings.fade = [0, 0];
            } else if (settings.fade === true) {
                settings.fade = _defaultValues.fade;
            } else if (typeof settings.fade === "string" || typeof settings.fade === "number") {
                settings.fade = [settings.fade, settings.fade];
            } else if ($.isArray(settings.fade) && settings.fade.length < 2) {
                settings.fade = [settings.fade[0], settings.fade[0]];
            }
            settings.fade = [parseInt(settings.fade[0], 10), parseInt(settings.fade[1], 10)]


            // Save settings
            data.settings = settings;
            // Save data
            overlay.data("loadingoverlay_data", data);
            // Save reference to overlay
            container.data("loadingoverlay", overlay);


            // Resize
            overlay
                .fadeTo(0, 0.01)
                .appendTo("body");
            _IntervalResize(container, true);
            if (settings.resizeInterval > 0) {
                data.resizeIntervalId = setInterval(function () {
                    _IntervalResize(container, false);
                }, settings.resizeInterval);
            }

            // Show LoadingOverlay
            overlay.fadeTo(settings.fade[0], 1);
        }
        data.count++;
    }

    function Hide(container, force) {
        container = $(container);
        var overlay = _GetOverlay(container);
        var data = _GetData(container);
        if (data === false) return;

        data.count--;
        if (force || data.count <= 0) {
            overlay.animate({
                "opacity": 0
            }, data.settings.fade[1], function () {
                if (data.resizeIntervalId) clearInterval(data.resizeIntervalId);
                $(this).remove();
                container.removeData("loadingoverlay");
            });
        }
    }

    function Resize(container) {
        _IntervalResize($(container), true);
    }

    function Text(container, value) {
        container = $(container);
        var data = _GetData(container);
        if (data === false || !data.text) return;

        if (value === false) {
            data.text.hide();
        } else {
            data.text
                .show()
                .text(value);
        }
    }

    function Progress(container, value) {
        container = $(container);
        var data = _GetData(container);
        if (data === false || !data.progress) return;

        if (value === false) {
            data.progress.bar.hide();
        } else {
            var v = ((parseFloat(value) || 0) - data.progress.min) * 100 / (data.progress.max - data.progress.min);
            if (v < 0) v = 0;
            if (v > 100) v = 100;
            data.progress.bar
                .show()
                .animate({
                    "width": v + "%"
                }, data.progress.speed);
        }
    }


    function _IntervalResize(container, force) {
        var overlay = _GetOverlay(container);
        var data = _GetData(container);
        if (data === false) return;

        // Overlay
        if (!data.wholePage) {
            var isFixed = container.css("position") === "fixed";
            var pos = isFixed ? container[0].getBoundingClientRect() : container.offset();
            overlay.css({
                "position": isFixed ? "fixed" : "absolute",
                "top": pos.top + parseInt(container.css("border-top-width"), 10),
                "left": pos.left + parseInt(container.css("border-left-width"), 10),
                "width": container.innerWidth(),
                "height": container.innerHeight()
            });
        }

        // Elements
        if (data.settings.size) {
            var c = data.wholePage ? $(window) : container;
            var size = data.settings.size.value;
            if (!data.settings.size.fixed) {
                size = Math.min(c.innerWidth(), c.innerHeight()) * size / 100;
                if (data.settings.maxSize && size > data.settings.maxSize) size = data.settings.maxSize;
                if (data.settings.minSize && size < data.settings.minSize) size = data.settings.minSize;
            }
            overlay.children(".loadingoverlay_element").each(function () {
                var $this = $(this);
                if (force || $this.data("loadingoverlay_autoresize")) {
                    var resizeFactor = $this.data("loadingoverlay_resizefactor");
                    if ($this.hasClass("loadingoverlay_fa") || $this.hasClass("loadingoverlay_text")) {
                        $this.css("font-size", (size * resizeFactor) + data.settings.size.units);
                    } else if ($this.hasClass("loadingoverlay_progress")) {
                        data.progress.bar.css("height", (size * resizeFactor) + data.settings.size.units);
                        if (!data.progress.fixed) {
                            data.progress.bar
                                .css("top", $this.position().top)
                                .css("top", "-=" + (size * resizeFactor * 0.5) + data.settings.size.units);
                        } else if (data.progress.fixed === "bottom") {
                            $this
                                .css("bottom", data.progress.margin ? data.progress.margin.value + (data.progress.margin.fixed ? data.progress.margin.units : "%") : 0)
                                .css("bottom", "+=" + (size * resizeFactor) + data.settings.size.units);
                        }
                    } else {
                        $this.css({
                            "width": (size * resizeFactor) + data.settings.size.units,
                            "height": (size * resizeFactor) + data.settings.size.units
                        });
                    }
                }
            });
        }
    }


    function _GetOverlay(container) {
        return container.data("loadingoverlay");
    }

    function _GetData(container) {
        var overlay = _GetOverlay(container);
        var data = (typeof overlay === "undefined") ? undefined : overlay.data("loadingoverlay_data");
        if (typeof data === "undefined") {
            // Clean DOM
            $(".loadingoverlay").each(function () {
                var $this = $(this);
                var data = $this.data("loadingoverlay_data");
                if (!document.body.contains(data.container[0])) {
                    if (data.resizeIntervalId) clearInterval(data.resizeIntervalId);
                    $this.remove();
                }
            });
            return false;
        } else {
            overlay.toggle(container.is(":visible"));
            return data;
        }
    }


    function _CreateElement(overlay, order, autoResize, resizeFactor, animation) {
        var element = $("<div>", {
            "class": "loadingoverlay_element",
            "css": {
                "order": order
            }
        })
            .css(_css.element)
            .data({
                "loadingoverlay_autoresize": autoResize,
                "loadingoverlay_resizefactor": resizeFactor
            })
            .appendTo(overlay);

        // Parse animation
        if (animation === true) animation = _defaultValues.animations.time + " " + _defaultValues.animations.name;
        if (typeof animation === "string") {
            var animationName;
            var animationTime;
            var parts = animation.replace(/\s\s+/g, " ").toLowerCase().split(" ");
            if (parts.length === 2 && _ValidateCssTime(parts[0]) && _ValidateAnimation(parts[1])) {
                animationName = parts[1];
                animationTime = parts[0];
            } else if (parts.length === 2 && _ValidateCssTime(parts[1]) && _ValidateAnimation(parts[0])) {
                animationName = parts[0];
                animationTime = parts[1];
            } else if (parts.length === 1 && _ValidateCssTime(parts[0])) {
                animationName = _defaultValues.animations.name;
                animationTime = parts[0];
            } else if (parts.length === 1 && _ValidateAnimation(parts[0])) {
                animationName = parts[0];
                animationTime = _defaultValues.animations.time;
            }
            element.css({
                "animation-name": "loadingoverlay_animation__" + animationName,
                "animation-duration": animationTime,
                "animation-timing-function": "linear",
                "animation-iteration-count": "infinite"
            });
        }

        return element;
    }

    function _ValidateCssTime(value) {
        return !isNaN(parseFloat(value)) && (value.slice(-1) === "s" || value.slice(-2) === "ms");
    }

    function _ValidateAnimation(value) {
        return _whitelists.animations.indexOf(value) > -1;
    }

    function _ValidateProgressPosition(value) {
        return _whitelists.progressPosition.indexOf(value) > -1;
    }


    function _ParseSize(value) {
        if (!value || value < 0) {
            return false;
        } else if (typeof value === "string" && ["vmin", "vmax"].indexOf(value.slice(-4)) > -1) {
            return {
                fixed: true,
                units: value.slice(-4),
                value: value.slice(0, -4)
            };
        } else if (typeof value === "string" && ["rem"].indexOf(value.slice(-3)) > -1) {
            return {
                fixed: true,
                units: value.slice(-3),
                value: value.slice(0, -3)
            };
        } else if (typeof value === "string" && ["px", "em", "cm", "mm", "in", "pt", "pc", "vh", "vw"].indexOf(value.slice(-2)) > -1) {
            return {
                fixed: true,
                units: value.slice(-2),
                value: value.slice(0, -2)
            };
        } else {
            return {
                fixed: false,
                units: "px",
                value: parseFloat(value)
            };
        }
    }


    $(function () {
        $("head").append([
            "<style>",
            "@-webkit-keyframes loadingoverlay_animation__rotate_right {",
            "to {",
            "-webkit-transform : rotate(360deg);",
            "transform : rotate(360deg);",
            "}",
            "}",
            "@keyframes loadingoverlay_animation__rotate_right {",
            "to {",
            "-webkit-transform : rotate(360deg);",
            "transform : rotate(360deg);",
            "}",
            "}",

            "@-webkit-keyframes loadingoverlay_animation__rotate_left {",
            "to {",
            "-webkit-transform : rotate(-360deg);",
            "transform : rotate(-360deg);",
            "}",
            "}",
            "@keyframes loadingoverlay_animation__rotate_left {",
            "to {",
            "-webkit-transform : rotate(-360deg);",
            "transform : rotate(-360deg);",
            "}",
            "}",

            "@-webkit-keyframes loadingoverlay_animation__fadein {",
            "0% {",
            "opacity   : 0;",
            "-webkit-transform : scale(0.1, 0.1);",
            "transform : scale(0.1, 0.1);",
            "}",
            "50% {",
            "opacity   : 1;",
            "}",
            "100% {",
            "opacity   : 0;",
            "-webkit-transform : scale(1, 1);",
            "transform : scale(1, 1);",
            "}",
            "}",
            "@keyframes loadingoverlay_animation__fadein {",
            "0% {",
            "opacity   : 0;",
            "-webkit-transform : scale(0.1, 0.1);",
            "transform : scale(0.1, 0.1);",
            "}",
            "50% {",
            "opacity   : 1;",
            "}",
            "100% {",
            "opacity   : 0;",
            "-webkit-transform : scale(1, 1);",
            "transform : scale(1, 1);",
            "}",
            "}",

            "@-webkit-keyframes loadingoverlay_animation__pulse {",
            "0% {",
            "-webkit-transform : scale(0, 0);",
            "transform : scale(0, 0);",
            "}",
            "50% {",
            "-webkit-transform : scale(1, 1);",
            "transform : scale(1, 1);",
            "}",
            "100% {",
            "-webkit-transform : scale(0, 0);",
            "transform : scale(0, 0);",
            "}",
            "}",
            "@keyframes loadingoverlay_animation__pulse {",
            "0% {",
            "-webkit-transform : scale(0, 0);",
            "transform : scale(0, 0);",
            "}",
            "50% {",
            "-webkit-transform : scale(1, 1);",
            "transform : scale(1, 1);",
            "}",
            "100% {",
            "-webkit-transform : scale(0, 0);",
            "transform : scale(0, 0);",
            "}",
            "}",
            "</style>"
        ].join(" "));
    });

}));